package org.opencds.opencds_js_client

class MainController {

	def index() {

		def interactionId_scopingEntityId = "edu.utah.bmi"
		def interactionId_interactionId = "123456"
		def interactionId_submissionTime = new Date().format("yyyy-MM-dd'T'H:mm:ss")

		def specifiedTime = new Date().format("yyyy-MM-dd'T'H:mm:ss") //

		def evaluationRequest_clientLanguage = ""
		def evaluationRequest_clientTimeZoneOffset = ""

		def kmEvaluationRequest_kmId_scopingEntityId = "org.opencds"
		def kmEvaluationRequest_kmId_businessId = "qm.template"
		def kmEvaluationRequest_kmId_version = "1.0.0-SNAPSHOT"

		def dataRequirementItemData_driId = "testPayload"

		def containingEntityId_scopingEntityId = "edu.utah.bmi"
		def containingEntityId_businessId = "bounceData"
		def containingEntityId_version = "1.0.0"

		def informationModelSSId_scopingEntityId = "org.opencds.fhir"
		def informationModelSSId_businessId = "FHIR"
		def informationModelSSId_version = "1.0"

		String outputFhirMsg = grailsApplication.mainContext.getResource("resources/samples/qm_template/sample_input_patient.json").file.text

		[
			interactionId_scopingEntityId:interactionId_scopingEntityId, 
			interactionId_interactionId:interactionId_interactionId, 
			interactionId_submissionTime:interactionId_submissionTime, 
			specifiedTime:specifiedTime,
			evaluationRequest_clientLanguage:evaluationRequest_clientLanguage,
			evaluationRequest_clientTimeZoneOffset:evaluationRequest_clientTimeZoneOffset,
			kmEvaluationRequest_kmId_scopingEntityId:kmEvaluationRequest_kmId_scopingEntityId,
			kmEvaluationRequest_kmId_businessId:kmEvaluationRequest_kmId_businessId,
			kmEvaluationRequest_kmId_version:kmEvaluationRequest_kmId_version,
			dataRequirementItemData_driId:dataRequirementItemData_driId,
			containingEntityId_scopingEntityId:containingEntityId_scopingEntityId,
			containingEntityId_businessId:containingEntityId_businessId,
			containingEntityId_version:containingEntityId_version,
			informationModelSSId_scopingEntityId:informationModelSSId_scopingEntityId,
			informationModelSSId_businessId:informationModelSSId_businessId,
			informationModelSSId_version:informationModelSSId_version,
			outputFhirMsg:outputFhirMsg
		]
	}
}
