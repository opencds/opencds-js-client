
<%-- interactionId --%>
<div class="fieldcontain">
	<label for="interactionId_scopingEntityId">
		interactionId_scopingEntityId
	</label>
	<g:textField name="interactionId_scopingEntityId" value="${interactionId_scopingEntityId}"/>
</div>

<div class="fieldcontain">
	<label for="interactionId_interactionId">
		interactionId_interactionId
	</label>
	<g:textField name="interactionId_interactionId" value="${interactionId_interactionId}"/>
</div>

<div class="fieldcontain">
	<label for="interactionId_submissionTime">
		interactionId_submissionTime
	</label>
	<g:textField name="interactionId_submissionTime" value="${interactionId_submissionTime}"/>
</div>



<%-- specifed time --%>
<div class="fieldcontain">
	<label for="specifiedTime">
		specifiedTime
	</label>
	<g:textField name="specifiedTime" value="${specifiedTime}"/>
</div>



<%-- evaluation request --%>
<div class="fieldcontain">
	<label for="evaluationRequest_clientLanguage">
		evaluationRequest_clientLanguage
	</label>
	<g:textField name="evaluationRequest_clientLanguage" value="${evaluationRequest_clientLanguage}"/>
</div>

<div class="fieldcontain">
	<label for="evaluationRequest_clientTimeZoneOffset">
		evaluationRequest_clientTimeZoneOffset
	</label>
	<g:textField name="evaluationRequest_clientTimeZoneOffset" value="${evaluationRequest_clientTimeZoneOffset}"/>
</div>



<%-- kmEvaluationRequest --%>
<div class="fieldcontain">
	<label for="kmEvaluationRequest_kmId_scopingEntityId">
		* kmEvaluationRequest_kmId_scopingEntityId 
	</label>
	<g:textField name="kmEvaluationRequest_kmId_scopingEntityId" value="${kmEvaluationRequest_kmId_scopingEntityId}"/>
</div>

<div class="fieldcontain">
	<label for="kmEvaluationRequest_kmId_businessId">
		* kmEvaluationRequest_kmId_businessId 
	</label>
	<g:textField name="kmEvaluationRequest_kmId_businessId" value="${kmEvaluationRequest_kmId_businessId}"/>
</div>

<div class="fieldcontain">
	<label for="kmEvaluationRequest_kmId_version">
		* kmEvaluationRequest_kmId_version 
	</label>
	<g:textField name="kmEvaluationRequest_kmId_version" value="${kmEvaluationRequest_kmId_version}"/>
</div>




<%-- dataRequirementItemData --%>
<div class="fieldcontain">
	<label for="dataRequirementItemData_driId">
		dataRequirementItemData_driId 
	</label>
	<g:textField name="dataRequirementItemData_driId" value="${dataRequirementItemData_driId}"/>
</div>

<div class="fieldcontain">
	<label for="containingEntityId_scopingEntityId">
		containingEntityId_scopingEntityId 
	</label>
	<g:textField name="containingEntityId_scopingEntityId" value="${containingEntityId_scopingEntityId}"/>
</div>

<div class="fieldcontain">
	<label for="containingEntityId_businessId">
		containingEntityId_businessId 
	</label>
	<g:textField name="containingEntityId_businessId" value="${containingEntityId_businessId}"/>
</div>

<div class="fieldcontain">
	<label for="containingEntityId_version">
		containingEntityId_version 
	</label>
	<g:textField name="containingEntityId_version" value="${containingEntityId_version}"/>
</div>



<%-- data --%>
<div class="fieldcontain">
	<label for="informationModelSSId_scopingEntityId">
		informationModelSSId_scopingEntityId 
	</label>
	<g:textField name="informationModelSSId_scopingEntityId" value="${informationModelSSId_scopingEntityId}"/>
</div>

<div class="fieldcontain">
	<label for="informationModelSSId_businessId">
		informationModelSSId_businessId 
	</label>
	<g:textField name="informationModelSSId_businessId" value="${informationModelSSId_businessId}"/>
</div>

<div class="fieldcontain">
	<label for="informationModelSSId_version">
		informationModelSSId_version 
	</label>
	<g:textField name="informationModelSSId_version" value="${informationModelSSId_version}"/>
</div>



<%--  --%>
<div class="fieldcontain">
	<label for="outputFhirMsg">
		fhir msg
	</label>
	<g:textArea name="outputFhirMsg" style="width:900px; height: 200px;" value="${outputFhirMsg}"/>
</div>
<div class="fieldcontain">
	<label for="inputFhirMsg">
	response 
	</label>
	<g:textArea name="inputFhirMsg" id="response" style="width:900px; height: 200px;" value="${inputFhirMsg}"/>
</div>
