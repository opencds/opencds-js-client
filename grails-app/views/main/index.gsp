<!doctype html>
<html>
	<head>
        <script type="text/javascript" src="js/cxf-utils.js"></script>
        <script type="text/javascript" src="js/DecisionSupportService.js"></script>
        <script type="text/javascript" src="js/EvaluationServiceImp.js"></script>	

		<meta name="layout" content="main">
		<g:set var="entityName" value="OpenCDS Form" />
		
		<title>OpenCDS Client</title>
	</head>
	<body>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				
			</ul>
		</div>
		<div id="create-feedbackForm" class="content scaffold-create" role="main">
			<h1><g:message code="OpenCDS Client" /></h1>
			<g:form action="index" name="opencdsclientform" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<input type="button" class="save" value="Send Request" name="send" onClick="invokeEvaluationService()"/> 	
					<input type="button" class="delete" value="Clean" name="clear" onClick="clearResponseTextArea()"/>
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
