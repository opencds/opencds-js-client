//
// Definitions for schema: http://www.omg.org/spec/CDSS/201105/dss
//  http://localhost:8081/opencds-decision-support-service/evaluate?xsd=OmgDssSchema.xsd
//
//
// Simple type (enumeration) {http://www.omg.org/spec/CDSS/201105/dss}KMStatus
//
// - APPROVED
// - DEFINED
// - DRAFT
// - PROMOTED
// - REJECTED
// - RETIRED
//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedScopingEntityException
//
function DSS_UnrecognizedScopingEntityException () {
    this.typeMarker = 'DSS_UnrecognizedScopingEntityException';
    this._errorMessage = [];
    this._scopingEntityId = '';
}

//
// accessor is DSS_UnrecognizedScopingEntityException.prototype.getErrorMessage
// element get for errorMessage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ErrorMessage
// - required element
// - array
//
// element set for errorMessage
// setter function is is DSS_UnrecognizedScopingEntityException.prototype.setErrorMessage
//
function DSS_UnrecognizedScopingEntityException_getErrorMessage() { return this._errorMessage;}

DSS_UnrecognizedScopingEntityException.prototype.getErrorMessage = DSS_UnrecognizedScopingEntityException_getErrorMessage;

function DSS_UnrecognizedScopingEntityException_setErrorMessage(value) { this._errorMessage = value;}

DSS_UnrecognizedScopingEntityException.prototype.setErrorMessage = DSS_UnrecognizedScopingEntityException_setErrorMessage;
//
// accessor is DSS_UnrecognizedScopingEntityException.prototype.getScopingEntityId
// element get for scopingEntityId
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for scopingEntityId
// setter function is is DSS_UnrecognizedScopingEntityException.prototype.setScopingEntityId
//
function DSS_UnrecognizedScopingEntityException_getScopingEntityId() { return this._scopingEntityId;}

DSS_UnrecognizedScopingEntityException.prototype.getScopingEntityId = DSS_UnrecognizedScopingEntityException_getScopingEntityId;

function DSS_UnrecognizedScopingEntityException_setScopingEntityId(value) { this._scopingEntityId = value;}

DSS_UnrecognizedScopingEntityException.prototype.setScopingEntityId = DSS_UnrecognizedScopingEntityException_setScopingEntityId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedScopingEntityException
//
function DSS_UnrecognizedScopingEntityException_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._errorMessage != null) {
      for (var ax = 0;ax < this._errorMessage.length;ax ++) {
       if (this._errorMessage[ax] == null) {
        xml = xml + '<errorMessage/>';
       } else {
        xml = xml + '<errorMessage>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._errorMessage[ax]);
        xml = xml + '</errorMessage>';
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_UnrecognizedScopingEntityException.prototype.serialize = DSS_UnrecognizedScopingEntityException_serialize;

function DSS_UnrecognizedScopingEntityException_deserialize (cxfjsutils, element) {
    var newobject = new DSS_UnrecognizedScopingEntityException();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing errorMessage');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage'));
     newobject.setErrorMessage(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMDescriptionBase
//
function DSS_KMDescriptionBase () {
    this.typeMarker = 'DSS_KMDescriptionBase';
    this._kmId = null;
    this._status = '';
    this._traitValue = [];
}

//
// accessor is DSS_KMDescriptionBase.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_KMDescriptionBase.prototype.setKmId
//
function DSS_KMDescriptionBase_getKmId() { return this._kmId;}

DSS_KMDescriptionBase.prototype.getKmId = DSS_KMDescriptionBase_getKmId;

function DSS_KMDescriptionBase_setKmId(value) { this._kmId = value;}

DSS_KMDescriptionBase.prototype.setKmId = DSS_KMDescriptionBase_setKmId;
//
// accessor is DSS_KMDescriptionBase.prototype.getStatus
// element get for status
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMStatus
// - required element
//
// element set for status
// setter function is is DSS_KMDescriptionBase.prototype.setStatus
//
function DSS_KMDescriptionBase_getStatus() { return this._status;}

DSS_KMDescriptionBase.prototype.getStatus = DSS_KMDescriptionBase_getStatus;

function DSS_KMDescriptionBase_setStatus(value) { this._status = value;}

DSS_KMDescriptionBase.prototype.setStatus = DSS_KMDescriptionBase_setStatus;
//
// accessor is DSS_KMDescriptionBase.prototype.getTraitValue
// element get for traitValue
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMTraitValue
// - required element
// - array
//
// element set for traitValue
// setter function is is DSS_KMDescriptionBase.prototype.setTraitValue
//
function DSS_KMDescriptionBase_getTraitValue() { return this._traitValue;}

DSS_KMDescriptionBase.prototype.getTraitValue = DSS_KMDescriptionBase_getTraitValue;

function DSS_KMDescriptionBase_setTraitValue(value) { this._traitValue = value;}

DSS_KMDescriptionBase.prototype.setTraitValue = DSS_KMDescriptionBase_setTraitValue;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMDescriptionBase
//
function DSS_KMDescriptionBase_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    // block for local variables
    {
     xml = xml + '<status>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._status);
     xml = xml + '</status>';
    }
    // block for local variables
    {
     if (this._traitValue != null) {
      for (var ax = 0;ax < this._traitValue.length;ax ++) {
       if (this._traitValue[ax] == null) {
        xml = xml + '<traitValue/>';
       } else {
        xml = xml + this._traitValue[ax].serialize(cxfjsutils, 'traitValue', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMDescriptionBase.prototype.serialize = DSS_KMDescriptionBase_serialize;

function DSS_KMDescriptionBase_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMDescriptionBase();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing status');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setStatus(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing traitValue');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'traitValue')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_KMTraitValue_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'traitValue'));
     newobject.setTraitValue(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}UnsupportedLanguageException
//
function DSS_UnsupportedLanguageException () {
    this.typeMarker = 'DSS_UnsupportedLanguageException';
    this._errorMessage = [];
    this._unsupportedLanguage = '';
}

//
// accessor is DSS_UnsupportedLanguageException.prototype.getErrorMessage
// element get for errorMessage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ErrorMessage
// - required element
// - array
//
// element set for errorMessage
// setter function is is DSS_UnsupportedLanguageException.prototype.setErrorMessage
//
function DSS_UnsupportedLanguageException_getErrorMessage() { return this._errorMessage;}

DSS_UnsupportedLanguageException.prototype.getErrorMessage = DSS_UnsupportedLanguageException_getErrorMessage;

function DSS_UnsupportedLanguageException_setErrorMessage(value) { this._errorMessage = value;}

DSS_UnsupportedLanguageException.prototype.setErrorMessage = DSS_UnsupportedLanguageException_setErrorMessage;
//
// accessor is DSS_UnsupportedLanguageException.prototype.getUnsupportedLanguage
// element get for unsupportedLanguage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Language
// - required element
//
// element set for unsupportedLanguage
// setter function is is DSS_UnsupportedLanguageException.prototype.setUnsupportedLanguage
//
function DSS_UnsupportedLanguageException_getUnsupportedLanguage() { return this._unsupportedLanguage;}

DSS_UnsupportedLanguageException.prototype.getUnsupportedLanguage = DSS_UnsupportedLanguageException_getUnsupportedLanguage;

function DSS_UnsupportedLanguageException_setUnsupportedLanguage(value) { this._unsupportedLanguage = value;}

DSS_UnsupportedLanguageException.prototype.setUnsupportedLanguage = DSS_UnsupportedLanguageException_setUnsupportedLanguage;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}UnsupportedLanguageException
//
function DSS_UnsupportedLanguageException_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._errorMessage != null) {
      for (var ax = 0;ax < this._errorMessage.length;ax ++) {
       if (this._errorMessage[ax] == null) {
        xml = xml + '<errorMessage/>';
       } else {
        xml = xml + '<errorMessage>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._errorMessage[ax]);
        xml = xml + '</errorMessage>';
       }
      }
     }
    }
    // block for local variables
    {
     xml = xml + '<unsupportedLanguage>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._unsupportedLanguage);
     xml = xml + '</unsupportedLanguage>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_UnsupportedLanguageException.prototype.serialize = DSS_UnsupportedLanguageException_serialize;

function DSS_UnsupportedLanguageException_deserialize (cxfjsutils, element) {
    var newobject = new DSS_UnsupportedLanguageException();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing errorMessage');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage'));
     newobject.setErrorMessage(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing unsupportedLanguage');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setUnsupportedLanguage(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}SemanticRequirement
//
function DSS_SemanticRequirement () {
    this.typeMarker = 'DSS_SemanticRequirement';
    this._name = '';
    this._description = '';
    this._entityId = null;
    this._type = '';
}

//
// accessor is DSS_SemanticRequirement.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_SemanticRequirement.prototype.setName
//
function DSS_SemanticRequirement_getName() { return this._name;}

DSS_SemanticRequirement.prototype.getName = DSS_SemanticRequirement_getName;

function DSS_SemanticRequirement_setName(value) { this._name = value;}

DSS_SemanticRequirement.prototype.setName = DSS_SemanticRequirement_setName;
//
// accessor is DSS_SemanticRequirement.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_SemanticRequirement.prototype.setDescription
//
function DSS_SemanticRequirement_getDescription() { return this._description;}

DSS_SemanticRequirement.prototype.getDescription = DSS_SemanticRequirement_getDescription;

function DSS_SemanticRequirement_setDescription(value) { this._description = value;}

DSS_SemanticRequirement.prototype.setDescription = DSS_SemanticRequirement_setDescription;
//
// accessor is DSS_SemanticRequirement.prototype.getEntityId
// element get for entityId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for entityId
// setter function is is DSS_SemanticRequirement.prototype.setEntityId
//
function DSS_SemanticRequirement_getEntityId() { return this._entityId;}

DSS_SemanticRequirement.prototype.getEntityId = DSS_SemanticRequirement_getEntityId;

function DSS_SemanticRequirement_setEntityId(value) { this._entityId = value;}

DSS_SemanticRequirement.prototype.setEntityId = DSS_SemanticRequirement_setEntityId;
//
// accessor is DSS_SemanticRequirement.prototype.getType
// element get for type
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticRequirementType
// - required element
//
// element set for type
// setter function is is DSS_SemanticRequirement.prototype.setType
//
function DSS_SemanticRequirement_getType() { return this._type;}

DSS_SemanticRequirement.prototype.getType = DSS_SemanticRequirement_getType;

function DSS_SemanticRequirement_setType(value) { this._type = value;}

DSS_SemanticRequirement.prototype.setType = DSS_SemanticRequirement_setType;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}SemanticRequirement
//
function DSS_SemanticRequirement_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._entityId.serialize(cxfjsutils, 'entityId', null);
    }
    // block for local variables
    {
     xml = xml + '<type>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._type);
     xml = xml + '</type>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_SemanticRequirement.prototype.serialize = DSS_SemanticRequirement_serialize;

function DSS_SemanticRequirement_deserialize (cxfjsutils, element) {
    var newobject = new DSS_SemanticRequirement();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing entityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing type');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setType(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationRequestBase
//
function DSS_KMEvaluationRequestBase () {
    this.typeMarker = 'DSS_KMEvaluationRequestBase';
    this._kmId = null;
}

//
// accessor is DSS_KMEvaluationRequestBase.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_KMEvaluationRequestBase.prototype.setKmId
//
function DSS_KMEvaluationRequestBase_getKmId() { return this._kmId;}

DSS_KMEvaluationRequestBase.prototype.getKmId = DSS_KMEvaluationRequestBase_getKmId;

function DSS_KMEvaluationRequestBase_setKmId(value) { this._kmId = value;}

DSS_KMEvaluationRequestBase.prototype.setKmId = DSS_KMEvaluationRequestBase_setKmId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationRequestBase
//
function DSS_KMEvaluationRequestBase_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMEvaluationRequestBase.prototype.serialize = DSS_KMEvaluationRequestBase_serialize;

function DSS_KMEvaluationRequestBase_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMEvaluationRequestBase();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsResponse
//
function DSS_getKMDataRequirementsResponse () {
    this.typeMarker = 'DSS_getKMDataRequirementsResponse';
    this._kmDataRequirements = null;
}

//
// accessor is DSS_getKMDataRequirementsResponse.prototype.getKmDataRequirements
// element get for kmDataRequirements
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirements
// - required element
//
// element set for kmDataRequirements
// setter function is is DSS_getKMDataRequirementsResponse.prototype.setKmDataRequirements
//
function DSS_getKMDataRequirementsResponse_getKmDataRequirements() { return this._kmDataRequirements;}

DSS_getKMDataRequirementsResponse.prototype.getKmDataRequirements = DSS_getKMDataRequirementsResponse_getKmDataRequirements;

function DSS_getKMDataRequirementsResponse_setKmDataRequirements(value) { this._kmDataRequirements = value;}

DSS_getKMDataRequirementsResponse.prototype.setKmDataRequirements = DSS_getKMDataRequirementsResponse_setKmDataRequirements;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsResponse
//
function DSS_getKMDataRequirementsResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmDataRequirements.serialize(cxfjsutils, 'kmDataRequirements', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_getKMDataRequirementsResponse.prototype.serialize = DSS_getKMDataRequirementsResponse_serialize;

function DSS_getKMDataRequirementsResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_getKMDataRequirementsResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmDataRequirements');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_KMDataRequirements_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmDataRequirements(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Simple type (enumeration) {http://www.omg.org/spec/CDSS/201105/dss}ProfileType
//
// - CONFORMANCE_PROFILE
// - FUNCTIONAL_PROFILE
// - SEMANTIC_PROFILE
//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}CPQPInUse
//
function DSS_CPQPInUse () {
    this.typeMarker = 'DSS_CPQPInUse';
    this._specificationPath = '';
    this._cpqpItemId = null;
}

//
// accessor is DSS_CPQPInUse.prototype.getSpecificationPath
// element get for specificationPath
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for specificationPath
// setter function is is DSS_CPQPInUse.prototype.setSpecificationPath
//
function DSS_CPQPInUse_getSpecificationPath() { return this._specificationPath;}

DSS_CPQPInUse.prototype.getSpecificationPath = DSS_CPQPInUse_getSpecificationPath;

function DSS_CPQPInUse_setSpecificationPath(value) { this._specificationPath = value;}

DSS_CPQPInUse.prototype.setSpecificationPath = DSS_CPQPInUse_setSpecificationPath;
//
// accessor is DSS_CPQPInUse.prototype.getCpqpItemId
// element get for cpqpItemId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
//
// element set for cpqpItemId
// setter function is is DSS_CPQPInUse.prototype.setCpqpItemId
//
function DSS_CPQPInUse_getCpqpItemId() { return this._cpqpItemId;}

DSS_CPQPInUse.prototype.getCpqpItemId = DSS_CPQPInUse_getCpqpItemId;

function DSS_CPQPInUse_setCpqpItemId(value) { this._cpqpItemId = value;}

DSS_CPQPInUse.prototype.setCpqpItemId = DSS_CPQPInUse_setCpqpItemId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}CPQPInUse
//
function DSS_CPQPInUse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<specificationPath>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._specificationPath);
     xml = xml + '</specificationPath>';
    }
    // block for local variables
    {
     xml = xml + this._cpqpItemId.serialize(cxfjsutils, 'cpqpItemId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_CPQPInUse.prototype.serialize = DSS_CPQPInUse_serialize;

function DSS_CPQPInUse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_CPQPInUse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing specificationPath');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setSpecificationPath(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing cpqpItemId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setCpqpItemId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}listProfiles
//
function DSS_listProfiles () {
    this.typeMarker = 'DSS_listProfiles';
    this._interactionId = null;
}

//
// accessor is DSS_listProfiles.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_listProfiles.prototype.setInteractionId
//
function DSS_listProfiles_getInteractionId() { return this._interactionId;}

DSS_listProfiles.prototype.getInteractionId = DSS_listProfiles_getInteractionId;

function DSS_listProfiles_setInteractionId(value) { this._interactionId = value;}

DSS_listProfiles.prototype.setInteractionId = DSS_listProfiles_setInteractionId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}listProfiles
//
function DSS_listProfiles_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_listProfiles.prototype.serialize = DSS_listProfiles_serialize;

function DSS_listProfiles_deserialize (cxfjsutils, element) {
    var newobject = new DSS_listProfiles();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}ServiceRequestBase
//
function DSS_ServiceRequestBase () {
    this.typeMarker = 'DSS_ServiceRequestBase';
    this._interactionId = null;
}

//
// accessor is DSS_ServiceRequestBase.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_ServiceRequestBase.prototype.setInteractionId
//
function DSS_ServiceRequestBase_getInteractionId() { return this._interactionId;}

DSS_ServiceRequestBase.prototype.getInteractionId = DSS_ServiceRequestBase_getInteractionId;

function DSS_ServiceRequestBase_setInteractionId(value) { this._interactionId = value;}

DSS_ServiceRequestBase.prototype.setInteractionId = DSS_ServiceRequestBase_setInteractionId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}ServiceRequestBase
//
function DSS_ServiceRequestBase_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_ServiceRequestBase.prototype.serialize = DSS_ServiceRequestBase_serialize;

function DSS_ServiceRequestBase_deserialize (cxfjsutils, element) {
    var newobject = new DSS_ServiceRequestBase();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}IntermediateState
//
function DSS_IntermediateState () {
    this.typeMarker = 'DSS_IntermediateState';
    this._base64EncodedStateData = '';
}

//
// accessor is DSS_IntermediateState.prototype.getBase64EncodedStateData
// element get for base64EncodedStateData
// - element type is {http://www.w3.org/2001/XMLSchema}base64Binary
// - required element
//
// element set for base64EncodedStateData
// setter function is is DSS_IntermediateState.prototype.setBase64EncodedStateData
//
function DSS_IntermediateState_getBase64EncodedStateData() { return this._base64EncodedStateData;}

DSS_IntermediateState.prototype.getBase64EncodedStateData = DSS_IntermediateState_getBase64EncodedStateData;

function DSS_IntermediateState_setBase64EncodedStateData(value) { this._base64EncodedStateData = value;}

DSS_IntermediateState.prototype.setBase64EncodedStateData = DSS_IntermediateState_setBase64EncodedStateData;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}IntermediateState
//
function DSS_IntermediateState_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<base64EncodedStateData>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._base64EncodedStateData);
     xml = xml + '</base64EncodedStateData>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_IntermediateState.prototype.serialize = DSS_IntermediateState_serialize;

function DSS_IntermediateState_deserialize (cxfjsutils, element) {
    var newobject = new DSS_IntermediateState();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing base64EncodedStateData');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = cxfjsutils.deserializeBase64orMom(curElement);
    }
    newobject.setBase64EncodedStateData(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementGroup
//
function DSS_KMDataRequirementGroup () {
    this.typeMarker = 'DSS_KMDataRequirementGroup';
    this._name = '';
    this._description = '';
    this._id = null;
    this._dataRequirementItem = [];
}

//
// accessor is DSS_KMDataRequirementGroup.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_KMDataRequirementGroup.prototype.setName
//
function DSS_KMDataRequirementGroup_getName() { return this._name;}

DSS_KMDataRequirementGroup.prototype.getName = DSS_KMDataRequirementGroup_getName;

function DSS_KMDataRequirementGroup_setName(value) { this._name = value;}

DSS_KMDataRequirementGroup.prototype.setName = DSS_KMDataRequirementGroup_setName;
//
// accessor is DSS_KMDataRequirementGroup.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_KMDataRequirementGroup.prototype.setDescription
//
function DSS_KMDataRequirementGroup_getDescription() { return this._description;}

DSS_KMDataRequirementGroup.prototype.getDescription = DSS_KMDataRequirementGroup_getDescription;

function DSS_KMDataRequirementGroup_setDescription(value) { this._description = value;}

DSS_KMDataRequirementGroup.prototype.setDescription = DSS_KMDataRequirementGroup_setDescription;
//
// accessor is DSS_KMDataRequirementGroup.prototype.getId
// element get for id
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
//
// element set for id
// setter function is is DSS_KMDataRequirementGroup.prototype.setId
//
function DSS_KMDataRequirementGroup_getId() { return this._id;}

DSS_KMDataRequirementGroup.prototype.getId = DSS_KMDataRequirementGroup_getId;

function DSS_KMDataRequirementGroup_setId(value) { this._id = value;}

DSS_KMDataRequirementGroup.prototype.setId = DSS_KMDataRequirementGroup_setId;
//
// accessor is DSS_KMDataRequirementGroup.prototype.getDataRequirementItem
// element get for dataRequirementItem
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementItem
// - required element
// - array
//
// element set for dataRequirementItem
// setter function is is DSS_KMDataRequirementGroup.prototype.setDataRequirementItem
//
function DSS_KMDataRequirementGroup_getDataRequirementItem() { return this._dataRequirementItem;}

DSS_KMDataRequirementGroup.prototype.getDataRequirementItem = DSS_KMDataRequirementGroup_getDataRequirementItem;

function DSS_KMDataRequirementGroup_setDataRequirementItem(value) { this._dataRequirementItem = value;}

DSS_KMDataRequirementGroup.prototype.setDataRequirementItem = DSS_KMDataRequirementGroup_setDataRequirementItem;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementGroup
//
function DSS_KMDataRequirementGroup_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._id.serialize(cxfjsutils, 'id', null);
    }
    // block for local variables
    {
     if (this._dataRequirementItem != null) {
      for (var ax = 0;ax < this._dataRequirementItem.length;ax ++) {
       if (this._dataRequirementItem[ax] == null) {
        xml = xml + '<dataRequirementItem/>';
       } else {
        xml = xml + this._dataRequirementItem[ax].serialize(cxfjsutils, 'dataRequirementItem', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMDataRequirementGroup.prototype.serialize = DSS_KMDataRequirementGroup_serialize;

function DSS_KMDataRequirementGroup_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMDataRequirementGroup();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing id');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing dataRequirementItem');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'dataRequirementItem')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_KMDataRequirementItem_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'dataRequirementItem'));
     newobject.setDataRequirementItem(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}listKMsResponse
//
function DSS_listKMsResponse () {
    this.typeMarker = 'DSS_listKMsResponse';
    this._kmList = null;
}

//
// accessor is DSS_listKMsResponse.prototype.getKmList
// element get for kmList
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMList
// - required element
//
// element set for kmList
// setter function is is DSS_listKMsResponse.prototype.setKmList
//
function DSS_listKMsResponse_getKmList() { return this._kmList;}

DSS_listKMsResponse.prototype.getKmList = DSS_listKMsResponse_getKmList;

function DSS_listKMsResponse_setKmList(value) { this._kmList = value;}

DSS_listKMsResponse.prototype.setKmList = DSS_listKMsResponse_setKmList;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}listKMsResponse
//
function DSS_listKMsResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmList.serialize(cxfjsutils, 'kmList', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_listKMsResponse.prototype.serialize = DSS_listKMsResponse_serialize;

function DSS_listKMsResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_listKMsResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmList');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_KMList_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmList(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMTraitCriterion
//
function DSS_KMTraitCriterion () {
    this.typeMarker = 'DSS_KMTraitCriterion';
    this._kmTraitCriterionValue = null;
}

//
// accessor is DSS_KMTraitCriterion.prototype.getKmTraitCriterionValue
// element get for kmTraitCriterionValue
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMTraitCriterionValue
// - required element
//
// element set for kmTraitCriterionValue
// setter function is is DSS_KMTraitCriterion.prototype.setKmTraitCriterionValue
//
function DSS_KMTraitCriterion_getKmTraitCriterionValue() { return this._kmTraitCriterionValue;}

DSS_KMTraitCriterion.prototype.getKmTraitCriterionValue = DSS_KMTraitCriterion_getKmTraitCriterionValue;

function DSS_KMTraitCriterion_setKmTraitCriterionValue(value) { this._kmTraitCriterionValue = value;}

DSS_KMTraitCriterion.prototype.setKmTraitCriterionValue = DSS_KMTraitCriterion_setKmTraitCriterionValue;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMTraitCriterion
//
function DSS_KMTraitCriterion_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmTraitCriterionValue.serialize(cxfjsutils, 'kmTraitCriterionValue', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMTraitCriterion.prototype.serialize = DSS_KMTraitCriterion_serialize;

function DSS_KMTraitCriterion_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMTraitCriterion();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmTraitCriterionValue');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_KMTraitCriterionValue_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmTraitCriterionValue(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}RequiredDataRequirement
//
function DSS_RequiredDataRequirement () {
    this.typeMarker = 'DSS_RequiredDataRequirement';
    this._informationModelSSId = null;
    this._requiredQueryModelSSId = null;
}

//
// accessor is DSS_RequiredDataRequirement.prototype.getInformationModelSSId
// element get for informationModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for informationModelSSId
// setter function is is DSS_RequiredDataRequirement.prototype.setInformationModelSSId
//
function DSS_RequiredDataRequirement_getInformationModelSSId() { return this._informationModelSSId;}

DSS_RequiredDataRequirement.prototype.getInformationModelSSId = DSS_RequiredDataRequirement_getInformationModelSSId;

function DSS_RequiredDataRequirement_setInformationModelSSId(value) { this._informationModelSSId = value;}

DSS_RequiredDataRequirement.prototype.setInformationModelSSId = DSS_RequiredDataRequirement_setInformationModelSSId;
//
// accessor is DSS_RequiredDataRequirement.prototype.getRequiredQueryModelSSId
// element get for requiredQueryModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - optional element
//
// element set for requiredQueryModelSSId
// setter function is is DSS_RequiredDataRequirement.prototype.setRequiredQueryModelSSId
//
function DSS_RequiredDataRequirement_getRequiredQueryModelSSId() { return this._requiredQueryModelSSId;}

DSS_RequiredDataRequirement.prototype.getRequiredQueryModelSSId = DSS_RequiredDataRequirement_getRequiredQueryModelSSId;

function DSS_RequiredDataRequirement_setRequiredQueryModelSSId(value) { this._requiredQueryModelSSId = value;}

DSS_RequiredDataRequirement.prototype.setRequiredQueryModelSSId = DSS_RequiredDataRequirement_setRequiredQueryModelSSId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}RequiredDataRequirement
//
function DSS_RequiredDataRequirement_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._informationModelSSId.serialize(cxfjsutils, 'informationModelSSId', null);
    }
    // block for local variables
    {
     if (this._requiredQueryModelSSId != null) {
      xml = xml + this._requiredQueryModelSSId.serialize(cxfjsutils, 'requiredQueryModelSSId', null);
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_RequiredDataRequirement.prototype.serialize = DSS_RequiredDataRequirement_serialize;

function DSS_RequiredDataRequirement_deserialize (cxfjsutils, element) {
    var newobject = new DSS_RequiredDataRequirement();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing informationModelSSId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInformationModelSSId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing requiredQueryModelSSId');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'requiredQueryModelSSId')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
     }
     newobject.setRequiredQueryModelSSId(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}IterativeKMEvaluationResponse
//
function DSS_IterativeKMEvaluationResponse () {
    this.typeMarker = 'DSS_IterativeKMEvaluationResponse';
    this._kmId = null;
    this._warning = [];
    this._requiredDRGId = [];
    this._intermediateState = null;
}

//
// accessor is DSS_IterativeKMEvaluationResponse.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_IterativeKMEvaluationResponse.prototype.setKmId
//
function DSS_IterativeKMEvaluationResponse_getKmId() { return this._kmId;}

DSS_IterativeKMEvaluationResponse.prototype.getKmId = DSS_IterativeKMEvaluationResponse_getKmId;

function DSS_IterativeKMEvaluationResponse_setKmId(value) { this._kmId = value;}

DSS_IterativeKMEvaluationResponse.prototype.setKmId = DSS_IterativeKMEvaluationResponse_setKmId;
//
// accessor is DSS_IterativeKMEvaluationResponse.prototype.getWarning
// element get for warning
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Warning
// - required element
// - array
//
// element set for warning
// setter function is is DSS_IterativeKMEvaluationResponse.prototype.setWarning
//
function DSS_IterativeKMEvaluationResponse_getWarning() { return this._warning;}

DSS_IterativeKMEvaluationResponse.prototype.getWarning = DSS_IterativeKMEvaluationResponse_getWarning;

function DSS_IterativeKMEvaluationResponse_setWarning(value) { this._warning = value;}

DSS_IterativeKMEvaluationResponse.prototype.setWarning = DSS_IterativeKMEvaluationResponse_setWarning;
//
// accessor is DSS_IterativeKMEvaluationResponse.prototype.getRequiredDRGId
// element get for requiredDRGId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
// - array
//
// element set for requiredDRGId
// setter function is is DSS_IterativeKMEvaluationResponse.prototype.setRequiredDRGId
//
function DSS_IterativeKMEvaluationResponse_getRequiredDRGId() { return this._requiredDRGId;}

DSS_IterativeKMEvaluationResponse.prototype.getRequiredDRGId = DSS_IterativeKMEvaluationResponse_getRequiredDRGId;

function DSS_IterativeKMEvaluationResponse_setRequiredDRGId(value) { this._requiredDRGId = value;}

DSS_IterativeKMEvaluationResponse.prototype.setRequiredDRGId = DSS_IterativeKMEvaluationResponse_setRequiredDRGId;
//
// accessor is DSS_IterativeKMEvaluationResponse.prototype.getIntermediateState
// element get for intermediateState
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}IntermediateState
// - required element
//
// element set for intermediateState
// setter function is is DSS_IterativeKMEvaluationResponse.prototype.setIntermediateState
//
function DSS_IterativeKMEvaluationResponse_getIntermediateState() { return this._intermediateState;}

DSS_IterativeKMEvaluationResponse.prototype.getIntermediateState = DSS_IterativeKMEvaluationResponse_getIntermediateState;

function DSS_IterativeKMEvaluationResponse_setIntermediateState(value) { this._intermediateState = value;}

DSS_IterativeKMEvaluationResponse.prototype.setIntermediateState = DSS_IterativeKMEvaluationResponse_setIntermediateState;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}IterativeKMEvaluationResponse
//
function DSS_IterativeKMEvaluationResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    // block for local variables
    {
     if (this._warning != null) {
      for (var ax = 0;ax < this._warning.length;ax ++) {
       if (this._warning[ax] == null) {
        xml = xml + '<warning/>';
       } else {
        xml = xml + this._warning[ax].serialize(cxfjsutils, 'warning', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._requiredDRGId != null) {
      for (var ax = 0;ax < this._requiredDRGId.length;ax ++) {
       if (this._requiredDRGId[ax] == null) {
        xml = xml + '<requiredDRGId/>';
       } else {
        xml = xml + this._requiredDRGId[ax].serialize(cxfjsutils, 'requiredDRGId', null);
       }
      }
     }
    }
    // block for local variables
    {
     xml = xml + this._intermediateState.serialize(cxfjsutils, 'intermediateState', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_IterativeKMEvaluationResponse.prototype.serialize = DSS_IterativeKMEvaluationResponse_serialize;

function DSS_IterativeKMEvaluationResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_IterativeKMEvaluationResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing warning');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'warning')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_Warning_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'warning'));
     newobject.setWarning(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing requiredDRGId');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'requiredDRGId')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'requiredDRGId'));
     newobject.setRequiredDRGId(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing intermediateState');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_IntermediateState_deserialize(cxfjsutils, curElement);
    }
    newobject.setIntermediateState(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationResponse
//
function DSS_IterativeEvaluationResponse () {
    this.typeMarker = 'DSS_IterativeEvaluationResponse';
    this._finalKMEvaluationResponse = [];
    this._iterativeKMEvaluationResponse = [];
}

//
// accessor is DSS_IterativeEvaluationResponse.prototype.getFinalKMEvaluationResponse
// element get for finalKMEvaluationResponse
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}FinalKMEvaluationResponse
// - required element
// - array
//
// element set for finalKMEvaluationResponse
// setter function is is DSS_IterativeEvaluationResponse.prototype.setFinalKMEvaluationResponse
//
function DSS_IterativeEvaluationResponse_getFinalKMEvaluationResponse() { return this._finalKMEvaluationResponse;}

DSS_IterativeEvaluationResponse.prototype.getFinalKMEvaluationResponse = DSS_IterativeEvaluationResponse_getFinalKMEvaluationResponse;

function DSS_IterativeEvaluationResponse_setFinalKMEvaluationResponse(value) { this._finalKMEvaluationResponse = value;}

DSS_IterativeEvaluationResponse.prototype.setFinalKMEvaluationResponse = DSS_IterativeEvaluationResponse_setFinalKMEvaluationResponse;
//
// accessor is DSS_IterativeEvaluationResponse.prototype.getIterativeKMEvaluationResponse
// element get for iterativeKMEvaluationResponse
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}IterativeKMEvaluationResponse
// - required element
// - array
//
// element set for iterativeKMEvaluationResponse
// setter function is is DSS_IterativeEvaluationResponse.prototype.setIterativeKMEvaluationResponse
//
function DSS_IterativeEvaluationResponse_getIterativeKMEvaluationResponse() { return this._iterativeKMEvaluationResponse;}

DSS_IterativeEvaluationResponse.prototype.getIterativeKMEvaluationResponse = DSS_IterativeEvaluationResponse_getIterativeKMEvaluationResponse;

function DSS_IterativeEvaluationResponse_setIterativeKMEvaluationResponse(value) { this._iterativeKMEvaluationResponse = value;}

DSS_IterativeEvaluationResponse.prototype.setIterativeKMEvaluationResponse = DSS_IterativeEvaluationResponse_setIterativeKMEvaluationResponse;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationResponse
//
function DSS_IterativeEvaluationResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._finalKMEvaluationResponse != null) {
      for (var ax = 0;ax < this._finalKMEvaluationResponse.length;ax ++) {
       if (this._finalKMEvaluationResponse[ax] == null) {
        xml = xml + '<finalKMEvaluationResponse/>';
       } else {
        xml = xml + this._finalKMEvaluationResponse[ax].serialize(cxfjsutils, 'finalKMEvaluationResponse', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._iterativeKMEvaluationResponse != null) {
      for (var ax = 0;ax < this._iterativeKMEvaluationResponse.length;ax ++) {
       if (this._iterativeKMEvaluationResponse[ax] == null) {
        xml = xml + '<iterativeKMEvaluationResponse/>';
       } else {
        xml = xml + this._iterativeKMEvaluationResponse[ax].serialize(cxfjsutils, 'iterativeKMEvaluationResponse', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_IterativeEvaluationResponse.prototype.serialize = DSS_IterativeEvaluationResponse_serialize;

function DSS_IterativeEvaluationResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_IterativeEvaluationResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing finalKMEvaluationResponse');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'finalKMEvaluationResponse')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_FinalKMEvaluationResponse_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'finalKMEvaluationResponse'));
     newobject.setFinalKMEvaluationResponse(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing iterativeKMEvaluationResponse');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'iterativeKMEvaluationResponse')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_IterativeKMEvaluationResponse_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'iterativeKMEvaluationResponse'));
     newobject.setIterativeKMEvaluationResponse(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}ProfilesByType
//
function DSS_ProfilesByType () {
    this.typeMarker = 'DSS_ProfilesByType';
    this._profilesOfType = [];
}

//
// accessor is DSS_ProfilesByType.prototype.getProfilesOfType
// element get for profilesOfType
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ProfilesOfType
// - required element
// - array
//
// element set for profilesOfType
// setter function is is DSS_ProfilesByType.prototype.setProfilesOfType
//
function DSS_ProfilesByType_getProfilesOfType() { return this._profilesOfType;}

DSS_ProfilesByType.prototype.getProfilesOfType = DSS_ProfilesByType_getProfilesOfType;

function DSS_ProfilesByType_setProfilesOfType(value) { this._profilesOfType = value;}

DSS_ProfilesByType.prototype.setProfilesOfType = DSS_ProfilesByType_setProfilesOfType;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}ProfilesByType
//
function DSS_ProfilesByType_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._profilesOfType != null) {
      for (var ax = 0;ax < this._profilesOfType.length;ax ++) {
       if (this._profilesOfType[ax] == null) {
        xml = xml + '<profilesOfType/>';
       } else {
        xml = xml + this._profilesOfType[ax].serialize(cxfjsutils, 'profilesOfType', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_ProfilesByType.prototype.serialize = DSS_ProfilesByType_serialize;

function DSS_ProfilesByType_deserialize (cxfjsutils, element) {
    var newobject = new DSS_ProfilesByType();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing profilesOfType');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'profilesOfType')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_ProfilesOfType_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'profilesOfType'));
     newobject.setProfilesOfType(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}SemanticPayload
//
function DSS_SemanticPayload () {
    this.typeMarker = 'DSS_SemanticPayload';
    this._informationModelSSId = null;
    this._base64EncodedPayload = '';
}

//
// accessor is DSS_SemanticPayload.prototype.getInformationModelSSId
// element get for informationModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for informationModelSSId
// setter function is is DSS_SemanticPayload.prototype.setInformationModelSSId
//
function DSS_SemanticPayload_getInformationModelSSId() { return this._informationModelSSId;}

DSS_SemanticPayload.prototype.getInformationModelSSId = DSS_SemanticPayload_getInformationModelSSId;

function DSS_SemanticPayload_setInformationModelSSId(value) { this._informationModelSSId = value;}

DSS_SemanticPayload.prototype.setInformationModelSSId = DSS_SemanticPayload_setInformationModelSSId;
//
// accessor is DSS_SemanticPayload.prototype.getBase64EncodedPayload
// element get for base64EncodedPayload
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}base64EncodedContent
// - required element
//
// element set for base64EncodedPayload
// setter function is is DSS_SemanticPayload.prototype.setBase64EncodedPayload
//
function DSS_SemanticPayload_getBase64EncodedPayload() { return this._base64EncodedPayload;}

DSS_SemanticPayload.prototype.getBase64EncodedPayload = DSS_SemanticPayload_getBase64EncodedPayload;

function DSS_SemanticPayload_setBase64EncodedPayload(value) { this._base64EncodedPayload = value;}

DSS_SemanticPayload.prototype.setBase64EncodedPayload = DSS_SemanticPayload_setBase64EncodedPayload;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}SemanticPayload
//
function DSS_SemanticPayload_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._informationModelSSId.serialize(cxfjsutils, 'informationModelSSId', null);
    }
    // block for local variables
    {
     xml = xml + '<base64EncodedPayload>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._base64EncodedPayload);
     xml = xml + '</base64EncodedPayload>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_SemanticPayload.prototype.serialize = DSS_SemanticPayload_serialize;

function DSS_SemanticPayload_deserialize (cxfjsutils, element) {
    var newobject = new DSS_SemanticPayload();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing informationModelSSId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInformationModelSSId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing base64EncodedPayload');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setBase64EncodedPayload(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}EvaluationRequest
//
function DSS_EvaluationRequest () {
    this.typeMarker = 'DSS_EvaluationRequest';
    this._kmEvaluationRequest = [];
    this._dataRequirementItemData = [];
    this._clientLanguage = '';
    this._clientTimeZoneOffset = '';
}

//
// accessor is DSS_EvaluationRequest.prototype.getKmEvaluationRequest
// element get for kmEvaluationRequest
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationRequest
// - required element
// - array
//
// element set for kmEvaluationRequest
// setter function is is DSS_EvaluationRequest.prototype.setKmEvaluationRequest
//
function DSS_EvaluationRequest_getKmEvaluationRequest() { return this._kmEvaluationRequest;}

DSS_EvaluationRequest.prototype.getKmEvaluationRequest = DSS_EvaluationRequest_getKmEvaluationRequest;

function DSS_EvaluationRequest_setKmEvaluationRequest(value) { this._kmEvaluationRequest = value;}

DSS_EvaluationRequest.prototype.setKmEvaluationRequest = DSS_EvaluationRequest_setKmEvaluationRequest;
//
// accessor is DSS_EvaluationRequest.prototype.getDataRequirementItemData
// element get for dataRequirementItemData
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}DataRequirementItemData
// - required element
// - array
//
// element set for dataRequirementItemData
// setter function is is DSS_EvaluationRequest.prototype.setDataRequirementItemData
//
function DSS_EvaluationRequest_getDataRequirementItemData() { return this._dataRequirementItemData;}

DSS_EvaluationRequest.prototype.getDataRequirementItemData = DSS_EvaluationRequest_getDataRequirementItemData;

function DSS_EvaluationRequest_setDataRequirementItemData(value) { this._dataRequirementItemData = value;}

DSS_EvaluationRequest.prototype.setDataRequirementItemData = DSS_EvaluationRequest_setDataRequirementItemData;
//
// accessor is DSS_EvaluationRequest.prototype.getClientLanguage
// element get for clientLanguage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Language
// - required element
//
// element set for clientLanguage
// setter function is is DSS_EvaluationRequest.prototype.setClientLanguage
//
function DSS_EvaluationRequest_getClientLanguage() { return this._clientLanguage;}

DSS_EvaluationRequest.prototype.getClientLanguage = DSS_EvaluationRequest_getClientLanguage;

function DSS_EvaluationRequest_setClientLanguage(value) { this._clientLanguage = value;}

DSS_EvaluationRequest.prototype.setClientLanguage = DSS_EvaluationRequest_setClientLanguage;
//
// accessor is DSS_EvaluationRequest.prototype.getClientTimeZoneOffset
// element get for clientTimeZoneOffset
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for clientTimeZoneOffset
// setter function is is DSS_EvaluationRequest.prototype.setClientTimeZoneOffset
//
function DSS_EvaluationRequest_getClientTimeZoneOffset() { return this._clientTimeZoneOffset;}

DSS_EvaluationRequest.prototype.getClientTimeZoneOffset = DSS_EvaluationRequest_getClientTimeZoneOffset;

function DSS_EvaluationRequest_setClientTimeZoneOffset(value) { this._clientTimeZoneOffset = value;}

DSS_EvaluationRequest.prototype.setClientTimeZoneOffset = DSS_EvaluationRequest_setClientTimeZoneOffset;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}EvaluationRequest
//
function DSS_EvaluationRequest_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName + ' clientLanguage0=' + '"' + this.getClientLanguage() + '"' + ' clientTimeZoneOffset=' + '"' + this.getClientTimeZoneOffset() + '"';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._kmEvaluationRequest != null) {
      for (var ax = 0;ax < this._kmEvaluationRequest.length;ax ++) {
       if (this._kmEvaluationRequest[ax] == null) {
        xml = xml + '<kmEvaluationRequest/>';
       } else {
        xml = xml + this._kmEvaluationRequest[ax].serialize(cxfjsutils, 'kmEvaluationRequest', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._dataRequirementItemData != null) {
      for (var ax = 0;ax < this._dataRequirementItemData.length;ax ++) {
       if (this._dataRequirementItemData[ax] == null) {
        xml = xml + '<dataRequirementItemData/>';
       } else {
        xml = xml + this._dataRequirementItemData[ax].serialize(cxfjsutils, 'dataRequirementItemData', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_EvaluationRequest.prototype.serialize = DSS_EvaluationRequest_serialize;

function DSS_EvaluationRequest_deserialize (cxfjsutils, element) {
    var newobject = new DSS_EvaluationRequest();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmEvaluationRequest');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'kmEvaluationRequest')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_KMEvaluationRequest_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'kmEvaluationRequest'));
     newobject.setKmEvaluationRequest(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing dataRequirementItemData');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'dataRequirementItemData')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_DataRequirementItemData_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'dataRequirementItemData'));
     newobject.setDataRequirementItemData(item);
     var item = null;
    }
    return newobject;
}

//
// Simple type (enumeration) {http://www.omg.org/spec/CDSS/201105/dss}EntityType
//
// - SEMANTIC_SIGNIFIER
// - TRAIT
// - PROFILE
// - SEMANTIC_REQUIREMENT
// - KNOWLEDGE_MODULE
//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}evaluateResponse
//
function DSS_evaluateResponse () {
    this.typeMarker = 'DSS_evaluateResponse';
    this._evaluationResponse = null;
}

//
// accessor is DSS_evaluateResponse.prototype.getEvaluationResponse
// element get for evaluationResponse
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EvaluationResponse
// - required element
//
// element set for evaluationResponse
// setter function is is DSS_evaluateResponse.prototype.setEvaluationResponse
//
function DSS_evaluateResponse_getEvaluationResponse() { return this._evaluationResponse;}

DSS_evaluateResponse.prototype.getEvaluationResponse = DSS_evaluateResponse_getEvaluationResponse;

function DSS_evaluateResponse_setEvaluationResponse(value) { this._evaluationResponse = value;}

DSS_evaluateResponse.prototype.setEvaluationResponse = DSS_evaluateResponse_setEvaluationResponse;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}evaluateResponse
//
function DSS_evaluateResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._evaluationResponse.serialize(cxfjsutils, 'evaluationResponse', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_evaluateResponse.prototype.serialize = DSS_evaluateResponse_serialize;

function DSS_evaluateResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_evaluateResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing evaluationResponse');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EvaluationResponse_deserialize(cxfjsutils, curElement);
    }
    newobject.setEvaluationResponse(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}ConformanceProfile
//
function DSS_ConformanceProfile () {
    this.typeMarker = 'DSS_ConformanceProfile';
    this._name = '';
    this._description = '';
    this._entityId = null;
    this._supportedFunctionalProfile = [];
    this._supportedSemanticProfile = [];
}

//
// accessor is DSS_ConformanceProfile.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_ConformanceProfile.prototype.setName
//
function DSS_ConformanceProfile_getName() { return this._name;}

DSS_ConformanceProfile.prototype.getName = DSS_ConformanceProfile_getName;

function DSS_ConformanceProfile_setName(value) { this._name = value;}

DSS_ConformanceProfile.prototype.setName = DSS_ConformanceProfile_setName;
//
// accessor is DSS_ConformanceProfile.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_ConformanceProfile.prototype.setDescription
//
function DSS_ConformanceProfile_getDescription() { return this._description;}

DSS_ConformanceProfile.prototype.getDescription = DSS_ConformanceProfile_getDescription;

function DSS_ConformanceProfile_setDescription(value) { this._description = value;}

DSS_ConformanceProfile.prototype.setDescription = DSS_ConformanceProfile_setDescription;
//
// accessor is DSS_ConformanceProfile.prototype.getEntityId
// element get for entityId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for entityId
// setter function is is DSS_ConformanceProfile.prototype.setEntityId
//
function DSS_ConformanceProfile_getEntityId() { return this._entityId;}

DSS_ConformanceProfile.prototype.getEntityId = DSS_ConformanceProfile_getEntityId;

function DSS_ConformanceProfile_setEntityId(value) { this._entityId = value;}

DSS_ConformanceProfile.prototype.setEntityId = DSS_ConformanceProfile_setEntityId;
//
// accessor is DSS_ConformanceProfile.prototype.getSupportedFunctionalProfile
// element get for supportedFunctionalProfile
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}FunctionalProfile
// - required element
// - array
//
// element set for supportedFunctionalProfile
// setter function is is DSS_ConformanceProfile.prototype.setSupportedFunctionalProfile
//
function DSS_ConformanceProfile_getSupportedFunctionalProfile() { return this._supportedFunctionalProfile;}

DSS_ConformanceProfile.prototype.getSupportedFunctionalProfile = DSS_ConformanceProfile_getSupportedFunctionalProfile;

function DSS_ConformanceProfile_setSupportedFunctionalProfile(value) { this._supportedFunctionalProfile = value;}

DSS_ConformanceProfile.prototype.setSupportedFunctionalProfile = DSS_ConformanceProfile_setSupportedFunctionalProfile;
//
// accessor is DSS_ConformanceProfile.prototype.getSupportedSemanticProfile
// element get for supportedSemanticProfile
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticProfile
// - required element
// - array
//
// element set for supportedSemanticProfile
// setter function is is DSS_ConformanceProfile.prototype.setSupportedSemanticProfile
//
function DSS_ConformanceProfile_getSupportedSemanticProfile() { return this._supportedSemanticProfile;}

DSS_ConformanceProfile.prototype.getSupportedSemanticProfile = DSS_ConformanceProfile_getSupportedSemanticProfile;

function DSS_ConformanceProfile_setSupportedSemanticProfile(value) { this._supportedSemanticProfile = value;}

DSS_ConformanceProfile.prototype.setSupportedSemanticProfile = DSS_ConformanceProfile_setSupportedSemanticProfile;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}ConformanceProfile
//
function DSS_ConformanceProfile_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._entityId.serialize(cxfjsutils, 'entityId', null);
    }
    // block for local variables
    {
     if (this._supportedFunctionalProfile != null) {
      for (var ax = 0;ax < this._supportedFunctionalProfile.length;ax ++) {
       if (this._supportedFunctionalProfile[ax] == null) {
        xml = xml + '<supportedFunctionalProfile/>';
       } else {
        xml = xml + this._supportedFunctionalProfile[ax].serialize(cxfjsutils, 'supportedFunctionalProfile', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._supportedSemanticProfile != null) {
      for (var ax = 0;ax < this._supportedSemanticProfile.length;ax ++) {
       if (this._supportedSemanticProfile[ax] == null) {
        xml = xml + '<supportedSemanticProfile/>';
       } else {
        xml = xml + this._supportedSemanticProfile[ax].serialize(cxfjsutils, 'supportedSemanticProfile', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_ConformanceProfile.prototype.serialize = DSS_ConformanceProfile_serialize;

function DSS_ConformanceProfile_deserialize (cxfjsutils, element) {
    var newobject = new DSS_ConformanceProfile();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing entityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing supportedFunctionalProfile');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'supportedFunctionalProfile')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_FunctionalProfile_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'supportedFunctionalProfile'));
     newobject.setSupportedFunctionalProfile(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing supportedSemanticProfile');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'supportedSemanticProfile')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_SemanticProfile_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'supportedSemanticProfile'));
     newobject.setSupportedSemanticProfile(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}InvalidTraitCriterionDataFormatException
//
function DSS_InvalidTraitCriterionDataFormatException () {
    this.typeMarker = 'DSS_InvalidTraitCriterionDataFormatException';
    this._errorMessage = [];
    this._informationModelSSId = null;
    this._traitCriterionId = null;
}

//
// accessor is DSS_InvalidTraitCriterionDataFormatException.prototype.getErrorMessage
// element get for errorMessage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ErrorMessage
// - required element
// - array
//
// element set for errorMessage
// setter function is is DSS_InvalidTraitCriterionDataFormatException.prototype.setErrorMessage
//
function DSS_InvalidTraitCriterionDataFormatException_getErrorMessage() { return this._errorMessage;}

DSS_InvalidTraitCriterionDataFormatException.prototype.getErrorMessage = DSS_InvalidTraitCriterionDataFormatException_getErrorMessage;

function DSS_InvalidTraitCriterionDataFormatException_setErrorMessage(value) { this._errorMessage = value;}

DSS_InvalidTraitCriterionDataFormatException.prototype.setErrorMessage = DSS_InvalidTraitCriterionDataFormatException_setErrorMessage;
//
// accessor is DSS_InvalidTraitCriterionDataFormatException.prototype.getInformationModelSSId
// element get for informationModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for informationModelSSId
// setter function is is DSS_InvalidTraitCriterionDataFormatException.prototype.setInformationModelSSId
//
function DSS_InvalidTraitCriterionDataFormatException_getInformationModelSSId() { return this._informationModelSSId;}

DSS_InvalidTraitCriterionDataFormatException.prototype.getInformationModelSSId = DSS_InvalidTraitCriterionDataFormatException_getInformationModelSSId;

function DSS_InvalidTraitCriterionDataFormatException_setInformationModelSSId(value) { this._informationModelSSId = value;}

DSS_InvalidTraitCriterionDataFormatException.prototype.setInformationModelSSId = DSS_InvalidTraitCriterionDataFormatException_setInformationModelSSId;
//
// accessor is DSS_InvalidTraitCriterionDataFormatException.prototype.getTraitCriterionId
// element get for traitCriterionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
//
// element set for traitCriterionId
// setter function is is DSS_InvalidTraitCriterionDataFormatException.prototype.setTraitCriterionId
//
function DSS_InvalidTraitCriterionDataFormatException_getTraitCriterionId() { return this._traitCriterionId;}

DSS_InvalidTraitCriterionDataFormatException.prototype.getTraitCriterionId = DSS_InvalidTraitCriterionDataFormatException_getTraitCriterionId;

function DSS_InvalidTraitCriterionDataFormatException_setTraitCriterionId(value) { this._traitCriterionId = value;}

DSS_InvalidTraitCriterionDataFormatException.prototype.setTraitCriterionId = DSS_InvalidTraitCriterionDataFormatException_setTraitCriterionId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}InvalidTraitCriterionDataFormatException
//
function DSS_InvalidTraitCriterionDataFormatException_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._errorMessage != null) {
      for (var ax = 0;ax < this._errorMessage.length;ax ++) {
       if (this._errorMessage[ax] == null) {
        xml = xml + '<errorMessage/>';
       } else {
        xml = xml + '<errorMessage>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._errorMessage[ax]);
        xml = xml + '</errorMessage>';
       }
      }
     }
    }
    // block for local variables
    {
     xml = xml + this._informationModelSSId.serialize(cxfjsutils, 'informationModelSSId', null);
    }
    // block for local variables
    {
     xml = xml + this._traitCriterionId.serialize(cxfjsutils, 'traitCriterionId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_InvalidTraitCriterionDataFormatException.prototype.serialize = DSS_InvalidTraitCriterionDataFormatException_serialize;

function DSS_InvalidTraitCriterionDataFormatException_deserialize (cxfjsutils, element) {
    var newobject = new DSS_InvalidTraitCriterionDataFormatException();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing errorMessage');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage'));
     newobject.setErrorMessage(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing informationModelSSId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInformationModelSSId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing traitCriterionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setTraitCriterionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}EvaluationResponse
//
function DSS_EvaluationResponse () {
    this.typeMarker = 'DSS_EvaluationResponse';
    this._finalKMEvaluationResponse = [];
    this._intermediateKMEvaluationResponse = [];
}

//
// accessor is DSS_EvaluationResponse.prototype.getFinalKMEvaluationResponse
// element get for finalKMEvaluationResponse
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}FinalKMEvaluationResponse
// - required element
// - array
//
// element set for finalKMEvaluationResponse
// setter function is is DSS_EvaluationResponse.prototype.setFinalKMEvaluationResponse
//
function DSS_EvaluationResponse_getFinalKMEvaluationResponse() { return this._finalKMEvaluationResponse;}

DSS_EvaluationResponse.prototype.getFinalKMEvaluationResponse = DSS_EvaluationResponse_getFinalKMEvaluationResponse;

function DSS_EvaluationResponse_setFinalKMEvaluationResponse(value) { this._finalKMEvaluationResponse = value;}

DSS_EvaluationResponse.prototype.setFinalKMEvaluationResponse = DSS_EvaluationResponse_setFinalKMEvaluationResponse;
//
// accessor is DSS_EvaluationResponse.prototype.getIntermediateKMEvaluationResponse
// element get for intermediateKMEvaluationResponse
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}IntermediateKMEvaluationResponse
// - required element
// - array
//
// element set for intermediateKMEvaluationResponse
// setter function is is DSS_EvaluationResponse.prototype.setIntermediateKMEvaluationResponse
//
function DSS_EvaluationResponse_getIntermediateKMEvaluationResponse() { return this._intermediateKMEvaluationResponse;}

DSS_EvaluationResponse.prototype.getIntermediateKMEvaluationResponse = DSS_EvaluationResponse_getIntermediateKMEvaluationResponse;

function DSS_EvaluationResponse_setIntermediateKMEvaluationResponse(value) { this._intermediateKMEvaluationResponse = value;}

DSS_EvaluationResponse.prototype.setIntermediateKMEvaluationResponse = DSS_EvaluationResponse_setIntermediateKMEvaluationResponse;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}EvaluationResponse
//
function DSS_EvaluationResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._finalKMEvaluationResponse != null) {
      for (var ax = 0;ax < this._finalKMEvaluationResponse.length;ax ++) {
       if (this._finalKMEvaluationResponse[ax] == null) {
        xml = xml + '<finalKMEvaluationResponse/>';
       } else {
        xml = xml + this._finalKMEvaluationResponse[ax].serialize(cxfjsutils, 'finalKMEvaluationResponse', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._intermediateKMEvaluationResponse != null) {
      for (var ax = 0;ax < this._intermediateKMEvaluationResponse.length;ax ++) {
       if (this._intermediateKMEvaluationResponse[ax] == null) {
        xml = xml + '<intermediateKMEvaluationResponse/>';
       } else {
        xml = xml + this._intermediateKMEvaluationResponse[ax].serialize(cxfjsutils, 'intermediateKMEvaluationResponse', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_EvaluationResponse.prototype.serialize = DSS_EvaluationResponse_serialize;

function DSS_EvaluationResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_EvaluationResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing finalKMEvaluationResponse');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'finalKMEvaluationResponse')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_FinalKMEvaluationResponse_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'finalKMEvaluationResponse'));
     newobject.setFinalKMEvaluationResponse(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing intermediateKMEvaluationResponse');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'intermediateKMEvaluationResponse')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_IntermediateKMEvaluationResponse_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'intermediateKMEvaluationResponse'));
     newobject.setIntermediateKMEvaluationResponse(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultSemanticsList
//
function DSS_KMEvaluationResultSemanticsList () {
    this.typeMarker = 'DSS_KMEvaluationResultSemanticsList';
    this._kmEvaluationResultSemantics = [];
}

//
// accessor is DSS_KMEvaluationResultSemanticsList.prototype.getKmEvaluationResultSemantics
// element get for kmEvaluationResultSemantics
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultSemantics
// - required element
// - array
//
// element set for kmEvaluationResultSemantics
// setter function is is DSS_KMEvaluationResultSemanticsList.prototype.setKmEvaluationResultSemantics
//
function DSS_KMEvaluationResultSemanticsList_getKmEvaluationResultSemantics() { return this._kmEvaluationResultSemantics;}

DSS_KMEvaluationResultSemanticsList.prototype.getKmEvaluationResultSemantics = DSS_KMEvaluationResultSemanticsList_getKmEvaluationResultSemantics;

function DSS_KMEvaluationResultSemanticsList_setKmEvaluationResultSemantics(value) { this._kmEvaluationResultSemantics = value;}

DSS_KMEvaluationResultSemanticsList.prototype.setKmEvaluationResultSemantics = DSS_KMEvaluationResultSemanticsList_setKmEvaluationResultSemantics;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultSemanticsList
//
function DSS_KMEvaluationResultSemanticsList_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._kmEvaluationResultSemantics != null) {
      for (var ax = 0;ax < this._kmEvaluationResultSemantics.length;ax ++) {
       if (this._kmEvaluationResultSemantics[ax] == null) {
        xml = xml + '<kmEvaluationResultSemantics/>';
       } else {
        xml = xml + this._kmEvaluationResultSemantics[ax].serialize(cxfjsutils, 'kmEvaluationResultSemantics', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMEvaluationResultSemanticsList.prototype.serialize = DSS_KMEvaluationResultSemanticsList_serialize;

function DSS_KMEvaluationResultSemanticsList_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMEvaluationResultSemanticsList();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmEvaluationResultSemantics');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'kmEvaluationResultSemantics')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_KMEvaluationResultSemantics_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'kmEvaluationResultSemantics'));
     newobject.setKmEvaluationResultSemantics(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyResponse
//
function DSS_evaluateIterativelyResponse () {
    this.typeMarker = 'DSS_evaluateIterativelyResponse';
    this._iterativeEvaluationResponse = null;
}

//
// accessor is DSS_evaluateIterativelyResponse.prototype.getIterativeEvaluationResponse
// element get for iterativeEvaluationResponse
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationResponse
// - required element
//
// element set for iterativeEvaluationResponse
// setter function is is DSS_evaluateIterativelyResponse.prototype.setIterativeEvaluationResponse
//
function DSS_evaluateIterativelyResponse_getIterativeEvaluationResponse() { return this._iterativeEvaluationResponse;}

DSS_evaluateIterativelyResponse.prototype.getIterativeEvaluationResponse = DSS_evaluateIterativelyResponse_getIterativeEvaluationResponse;

function DSS_evaluateIterativelyResponse_setIterativeEvaluationResponse(value) { this._iterativeEvaluationResponse = value;}

DSS_evaluateIterativelyResponse.prototype.setIterativeEvaluationResponse = DSS_evaluateIterativelyResponse_setIterativeEvaluationResponse;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyResponse
//
function DSS_evaluateIterativelyResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._iterativeEvaluationResponse.serialize(cxfjsutils, 'iterativeEvaluationResponse', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_evaluateIterativelyResponse.prototype.serialize = DSS_evaluateIterativelyResponse_serialize;

function DSS_evaluateIterativelyResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_evaluateIterativelyResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing iterativeEvaluationResponse');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_IterativeEvaluationResponse_deserialize(cxfjsutils, curElement);
    }
    newobject.setIterativeEvaluationResponse(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}TraitRequirement
//
function DSS_TraitRequirement () {
    this.typeMarker = 'DSS_TraitRequirement';
    this._isMandatory = '';
    this._traitId = null;
}

//
// accessor is DSS_TraitRequirement.prototype.getIsMandatory
// element get for isMandatory
// - element type is {http://www.w3.org/2001/XMLSchema}boolean
// - required element
//
// element set for isMandatory
// setter function is is DSS_TraitRequirement.prototype.setIsMandatory
//
function DSS_TraitRequirement_getIsMandatory() { return this._isMandatory;}

DSS_TraitRequirement.prototype.getIsMandatory = DSS_TraitRequirement_getIsMandatory;

function DSS_TraitRequirement_setIsMandatory(value) { this._isMandatory = value;}

DSS_TraitRequirement.prototype.setIsMandatory = DSS_TraitRequirement_setIsMandatory;
//
// accessor is DSS_TraitRequirement.prototype.getTraitId
// element get for traitId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for traitId
// setter function is is DSS_TraitRequirement.prototype.setTraitId
//
function DSS_TraitRequirement_getTraitId() { return this._traitId;}

DSS_TraitRequirement.prototype.getTraitId = DSS_TraitRequirement_getTraitId;

function DSS_TraitRequirement_setTraitId(value) { this._traitId = value;}

DSS_TraitRequirement.prototype.setTraitId = DSS_TraitRequirement_setTraitId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}TraitRequirement
//
function DSS_TraitRequirement_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<isMandatory>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._isMandatory);
     xml = xml + '</isMandatory>';
    }
    // block for local variables
    {
     xml = xml + this._traitId.serialize(cxfjsutils, 'traitId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_TraitRequirement.prototype.serialize = DSS_TraitRequirement_serialize;

function DSS_TraitRequirement_deserialize (cxfjsutils, element) {
    var newobject = new DSS_TraitRequirement();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing isMandatory');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = (value == 'true');
    }
    newobject.setIsMandatory(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing traitId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setTraitId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyAtSpecifiedTime
//
function DSS_evaluateIterativelyAtSpecifiedTime () {
    this.typeMarker = 'DSS_evaluateIterativelyAtSpecifiedTime';
    this._interactionId = null;
    this._specifiedTime = '';
    this._iterativeEvaluationRequest = null;
}

//
// accessor is DSS_evaluateIterativelyAtSpecifiedTime.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_evaluateIterativelyAtSpecifiedTime.prototype.setInteractionId
//
function DSS_evaluateIterativelyAtSpecifiedTime_getInteractionId() { return this._interactionId;}

DSS_evaluateIterativelyAtSpecifiedTime.prototype.getInteractionId = DSS_evaluateIterativelyAtSpecifiedTime_getInteractionId;

function DSS_evaluateIterativelyAtSpecifiedTime_setInteractionId(value) { this._interactionId = value;}

DSS_evaluateIterativelyAtSpecifiedTime.prototype.setInteractionId = DSS_evaluateIterativelyAtSpecifiedTime_setInteractionId;
//
// accessor is DSS_evaluateIterativelyAtSpecifiedTime.prototype.getSpecifiedTime
// element get for specifiedTime
// - element type is {http://www.w3.org/2001/XMLSchema}dateTime
// - required element
//
// element set for specifiedTime
// setter function is is DSS_evaluateIterativelyAtSpecifiedTime.prototype.setSpecifiedTime
//
function DSS_evaluateIterativelyAtSpecifiedTime_getSpecifiedTime() { return this._specifiedTime;}

DSS_evaluateIterativelyAtSpecifiedTime.prototype.getSpecifiedTime = DSS_evaluateIterativelyAtSpecifiedTime_getSpecifiedTime;

function DSS_evaluateIterativelyAtSpecifiedTime_setSpecifiedTime(value) { this._specifiedTime = value;}

DSS_evaluateIterativelyAtSpecifiedTime.prototype.setSpecifiedTime = DSS_evaluateIterativelyAtSpecifiedTime_setSpecifiedTime;
//
// accessor is DSS_evaluateIterativelyAtSpecifiedTime.prototype.getIterativeEvaluationRequest
// element get for iterativeEvaluationRequest
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationRequest
// - required element
//
// element set for iterativeEvaluationRequest
// setter function is is DSS_evaluateIterativelyAtSpecifiedTime.prototype.setIterativeEvaluationRequest
//
function DSS_evaluateIterativelyAtSpecifiedTime_getIterativeEvaluationRequest() { return this._iterativeEvaluationRequest;}

DSS_evaluateIterativelyAtSpecifiedTime.prototype.getIterativeEvaluationRequest = DSS_evaluateIterativelyAtSpecifiedTime_getIterativeEvaluationRequest;

function DSS_evaluateIterativelyAtSpecifiedTime_setIterativeEvaluationRequest(value) { this._iterativeEvaluationRequest = value;}

DSS_evaluateIterativelyAtSpecifiedTime.prototype.setIterativeEvaluationRequest = DSS_evaluateIterativelyAtSpecifiedTime_setIterativeEvaluationRequest;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyAtSpecifiedTime
//
function DSS_evaluateIterativelyAtSpecifiedTime_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + '<specifiedTime>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._specifiedTime);
     xml = xml + '</specifiedTime>';
    }
    // block for local variables
    {
     xml = xml + this._iterativeEvaluationRequest.serialize(cxfjsutils, 'iterativeEvaluationRequest', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_evaluateIterativelyAtSpecifiedTime.prototype.serialize = DSS_evaluateIterativelyAtSpecifiedTime_serialize;

function DSS_evaluateIterativelyAtSpecifiedTime_deserialize (cxfjsutils, element) {
    var newobject = new DSS_evaluateIterativelyAtSpecifiedTime();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing specifiedTime');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setSpecifiedTime(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing iterativeEvaluationRequest');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_IterativeEvaluationRequest_deserialize(cxfjsutils, curElement);
    }
    newobject.setIterativeEvaluationRequest(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityHierarchy
//
function DSS_describeScopingEntityHierarchy () {
    this.typeMarker = 'DSS_describeScopingEntityHierarchy';
    this._interactionId = null;
    this._scopingEntityId = '';
    this._maximumDescendantDepth = '';
}

//
// accessor is DSS_describeScopingEntityHierarchy.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_describeScopingEntityHierarchy.prototype.setInteractionId
//
function DSS_describeScopingEntityHierarchy_getInteractionId() { return this._interactionId;}

DSS_describeScopingEntityHierarchy.prototype.getInteractionId = DSS_describeScopingEntityHierarchy_getInteractionId;

function DSS_describeScopingEntityHierarchy_setInteractionId(value) { this._interactionId = value;}

DSS_describeScopingEntityHierarchy.prototype.setInteractionId = DSS_describeScopingEntityHierarchy_setInteractionId;
//
// accessor is DSS_describeScopingEntityHierarchy.prototype.getScopingEntityId
// element get for scopingEntityId
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for scopingEntityId
// setter function is is DSS_describeScopingEntityHierarchy.prototype.setScopingEntityId
//
function DSS_describeScopingEntityHierarchy_getScopingEntityId() { return this._scopingEntityId;}

DSS_describeScopingEntityHierarchy.prototype.getScopingEntityId = DSS_describeScopingEntityHierarchy_getScopingEntityId;

function DSS_describeScopingEntityHierarchy_setScopingEntityId(value) { this._scopingEntityId = value;}

DSS_describeScopingEntityHierarchy.prototype.setScopingEntityId = DSS_describeScopingEntityHierarchy_setScopingEntityId;
//
// accessor is DSS_describeScopingEntityHierarchy.prototype.getMaximumDescendantDepth
// element get for maximumDescendantDepth
// - element type is {http://www.w3.org/2001/XMLSchema}positiveInteger
// - required element
//
// element set for maximumDescendantDepth
// setter function is is DSS_describeScopingEntityHierarchy.prototype.setMaximumDescendantDepth
//
function DSS_describeScopingEntityHierarchy_getMaximumDescendantDepth() { return this._maximumDescendantDepth;}

DSS_describeScopingEntityHierarchy.prototype.getMaximumDescendantDepth = DSS_describeScopingEntityHierarchy_getMaximumDescendantDepth;

function DSS_describeScopingEntityHierarchy_setMaximumDescendantDepth(value) { this._maximumDescendantDepth = value;}

DSS_describeScopingEntityHierarchy.prototype.setMaximumDescendantDepth = DSS_describeScopingEntityHierarchy_setMaximumDescendantDepth;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityHierarchy
//
function DSS_describeScopingEntityHierarchy_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + '<scopingEntityId>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._scopingEntityId);
     xml = xml + '</scopingEntityId>';
    }
    // block for local variables
    {
     xml = xml + '<maximumDescendantDepth>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._maximumDescendantDepth);
     xml = xml + '</maximumDescendantDepth>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_describeScopingEntityHierarchy.prototype.serialize = DSS_describeScopingEntityHierarchy_serialize;

function DSS_describeScopingEntityHierarchy_deserialize (cxfjsutils, element) {
    var newobject = new DSS_describeScopingEntityHierarchy();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing scopingEntityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setScopingEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing maximumDescendantDepth');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setMaximumDescendantDepth(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}EvaluationResponseBase
//
function DSS_EvaluationResponseBase () {
    this.typeMarker = 'DSS_EvaluationResponseBase';
    this._finalKMEvaluationResponse = [];
}

//
// accessor is DSS_EvaluationResponseBase.prototype.getFinalKMEvaluationResponse
// element get for finalKMEvaluationResponse
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}FinalKMEvaluationResponse
// - required element
// - array
//
// element set for finalKMEvaluationResponse
// setter function is is DSS_EvaluationResponseBase.prototype.setFinalKMEvaluationResponse
//
function DSS_EvaluationResponseBase_getFinalKMEvaluationResponse() { return this._finalKMEvaluationResponse;}

DSS_EvaluationResponseBase.prototype.getFinalKMEvaluationResponse = DSS_EvaluationResponseBase_getFinalKMEvaluationResponse;

function DSS_EvaluationResponseBase_setFinalKMEvaluationResponse(value) { this._finalKMEvaluationResponse = value;}

DSS_EvaluationResponseBase.prototype.setFinalKMEvaluationResponse = DSS_EvaluationResponseBase_setFinalKMEvaluationResponse;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}EvaluationResponseBase
//
function DSS_EvaluationResponseBase_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._finalKMEvaluationResponse != null) {
      for (var ax = 0;ax < this._finalKMEvaluationResponse.length;ax ++) {
       if (this._finalKMEvaluationResponse[ax] == null) {
        xml = xml + '<finalKMEvaluationResponse/>';
       } else {
        xml = xml + this._finalKMEvaluationResponse[ax].serialize(cxfjsutils, 'finalKMEvaluationResponse', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_EvaluationResponseBase.prototype.serialize = DSS_EvaluationResponseBase_serialize;

function DSS_EvaluationResponseBase_deserialize (cxfjsutils, element) {
    var newobject = new DSS_EvaluationResponseBase();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing finalKMEvaluationResponse');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'finalKMEvaluationResponse')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_FinalKMEvaluationResponse_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'finalKMEvaluationResponse'));
     newobject.setFinalKMEvaluationResponse(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}getKMDescription
//
function DSS_getKMDescription () {
    this.typeMarker = 'DSS_getKMDescription';
    this._interactionId = null;
    this._kmId = null;
    this._clientLanguage = '';
}

//
// accessor is DSS_getKMDescription.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_getKMDescription.prototype.setInteractionId
//
function DSS_getKMDescription_getInteractionId() { return this._interactionId;}

DSS_getKMDescription.prototype.getInteractionId = DSS_getKMDescription_getInteractionId;

function DSS_getKMDescription_setInteractionId(value) { this._interactionId = value;}

DSS_getKMDescription.prototype.setInteractionId = DSS_getKMDescription_setInteractionId;
//
// accessor is DSS_getKMDescription.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_getKMDescription.prototype.setKmId
//
function DSS_getKMDescription_getKmId() { return this._kmId;}

DSS_getKMDescription.prototype.getKmId = DSS_getKMDescription_getKmId;

function DSS_getKMDescription_setKmId(value) { this._kmId = value;}

DSS_getKMDescription.prototype.setKmId = DSS_getKMDescription_setKmId;
//
// accessor is DSS_getKMDescription.prototype.getClientLanguage
// element get for clientLanguage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Language
// - required element
//
// element set for clientLanguage
// setter function is is DSS_getKMDescription.prototype.setClientLanguage
//
function DSS_getKMDescription_getClientLanguage() { return this._clientLanguage;}

DSS_getKMDescription.prototype.getClientLanguage = DSS_getKMDescription_getClientLanguage;

function DSS_getKMDescription_setClientLanguage(value) { this._clientLanguage = value;}

DSS_getKMDescription.prototype.setClientLanguage = DSS_getKMDescription_setClientLanguage;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}getKMDescription
//
function DSS_getKMDescription_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    // block for local variables
    {
     xml = xml + '<clientLanguage>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._clientLanguage);
     xml = xml + '</clientLanguage>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_getKMDescription.prototype.serialize = DSS_getKMDescription_serialize;

function DSS_getKMDescription_deserialize (cxfjsutils, element) {
    var newobject = new DSS_getKMDescription();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing clientLanguage');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setClientLanguage(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Simple type (enumeration) {http://www.omg.org/spec/CDSS/201105/dss}KMRelationshipType
//
// - PROVIDES_EVALUATION_RESULT_FOR_USE_BY
// - PROVIDES_EVALUATION_RESULT_FOR_PASS_THROUGH_BY
// - USES_EVALUATION_RESULT_FROM
// - PASSES_THROUGH_EVALUATION_RESULT_FROM
// - SUPERCEDED_BY
// - SUPERCEDES
//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}DescribedDO
//
function DSS_DescribedDO () {
    this.typeMarker = 'DSS_DescribedDO';
    this._name = '';
    this._description = '';
}

//
// accessor is DSS_DescribedDO.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_DescribedDO.prototype.setName
//
function DSS_DescribedDO_getName() { return this._name;}

DSS_DescribedDO.prototype.getName = DSS_DescribedDO_getName;

function DSS_DescribedDO_setName(value) { this._name = value;}

DSS_DescribedDO.prototype.setName = DSS_DescribedDO_setName;
//
// accessor is DSS_DescribedDO.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_DescribedDO.prototype.setDescription
//
function DSS_DescribedDO_getDescription() { return this._description;}

DSS_DescribedDO.prototype.getDescription = DSS_DescribedDO_getDescription;

function DSS_DescribedDO_setDescription(value) { this._description = value;}

DSS_DescribedDO.prototype.setDescription = DSS_DescribedDO_setDescription;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}DescribedDO
//
function DSS_DescribedDO_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_DescribedDO.prototype.serialize = DSS_DescribedDO_serialize;

function DSS_DescribedDO_deserialize (cxfjsutils, element) {
    var newobject = new DSS_DescribedDO();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementItem
//
function DSS_KMDataRequirementItem () {
    this.typeMarker = 'DSS_KMDataRequirementItem';
    this._name = '';
    this._description = '';
    this._id = null;
    this._informationModelAlternative = [];
}

//
// accessor is DSS_KMDataRequirementItem.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_KMDataRequirementItem.prototype.setName
//
function DSS_KMDataRequirementItem_getName() { return this._name;}

DSS_KMDataRequirementItem.prototype.getName = DSS_KMDataRequirementItem_getName;

function DSS_KMDataRequirementItem_setName(value) { this._name = value;}

DSS_KMDataRequirementItem.prototype.setName = DSS_KMDataRequirementItem_setName;
//
// accessor is DSS_KMDataRequirementItem.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_KMDataRequirementItem.prototype.setDescription
//
function DSS_KMDataRequirementItem_getDescription() { return this._description;}

DSS_KMDataRequirementItem.prototype.getDescription = DSS_KMDataRequirementItem_getDescription;

function DSS_KMDataRequirementItem_setDescription(value) { this._description = value;}

DSS_KMDataRequirementItem.prototype.setDescription = DSS_KMDataRequirementItem_setDescription;
//
// accessor is DSS_KMDataRequirementItem.prototype.getId
// element get for id
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
//
// element set for id
// setter function is is DSS_KMDataRequirementItem.prototype.setId
//
function DSS_KMDataRequirementItem_getId() { return this._id;}

DSS_KMDataRequirementItem.prototype.getId = DSS_KMDataRequirementItem_getId;

function DSS_KMDataRequirementItem_setId(value) { this._id = value;}

DSS_KMDataRequirementItem.prototype.setId = DSS_KMDataRequirementItem_setId;
//
// accessor is DSS_KMDataRequirementItem.prototype.getInformationModelAlternative
// element get for informationModelAlternative
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InformationModelAlternative
// - required element
// - array
//
// element set for informationModelAlternative
// setter function is is DSS_KMDataRequirementItem.prototype.setInformationModelAlternative
//
function DSS_KMDataRequirementItem_getInformationModelAlternative() { return this._informationModelAlternative;}

DSS_KMDataRequirementItem.prototype.getInformationModelAlternative = DSS_KMDataRequirementItem_getInformationModelAlternative;

function DSS_KMDataRequirementItem_setInformationModelAlternative(value) { this._informationModelAlternative = value;}

DSS_KMDataRequirementItem.prototype.setInformationModelAlternative = DSS_KMDataRequirementItem_setInformationModelAlternative;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementItem
//
function DSS_KMDataRequirementItem_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._id.serialize(cxfjsutils, 'id', null);
    }
    // block for local variables
    {
     if (this._informationModelAlternative != null) {
      for (var ax = 0;ax < this._informationModelAlternative.length;ax ++) {
       if (this._informationModelAlternative[ax] == null) {
        xml = xml + '<informationModelAlternative/>';
       } else {
        xml = xml + this._informationModelAlternative[ax].serialize(cxfjsutils, 'informationModelAlternative', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMDataRequirementItem.prototype.serialize = DSS_KMDataRequirementItem_serialize;

function DSS_KMDataRequirementItem_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMDataRequirementItem();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing id');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing informationModelAlternative');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'informationModelAlternative')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_InformationModelAlternative_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'informationModelAlternative'));
     newobject.setInformationModelAlternative(item);
     var item = null;
    }
    return newobject;
}

//
// Simple type (enumeration) {http://www.omg.org/spec/CDSS/201105/dss}SemanticRequirementType
//
// - INFORMATION_MODEL_REQUIREMENT
// - LANGUAGE_SUPPORT_REQUIREMENT
// - TRAIT_SET_REQUIREMENT
// - OTHER_SEMANTIC_REQUIREMENT
//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}listKMs
//
function DSS_listKMs () {
    this.typeMarker = 'DSS_listKMs';
    this._interactionId = null;
    this._clientLanguage = '';
    this._kmTraitInclusionSpecification = null;
}

//
// accessor is DSS_listKMs.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_listKMs.prototype.setInteractionId
//
function DSS_listKMs_getInteractionId() { return this._interactionId;}

DSS_listKMs.prototype.getInteractionId = DSS_listKMs_getInteractionId;

function DSS_listKMs_setInteractionId(value) { this._interactionId = value;}

DSS_listKMs.prototype.setInteractionId = DSS_listKMs_setInteractionId;
//
// accessor is DSS_listKMs.prototype.getClientLanguage
// element get for clientLanguage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Language
// - required element
//
// element set for clientLanguage
// setter function is is DSS_listKMs.prototype.setClientLanguage
//
function DSS_listKMs_getClientLanguage() { return this._clientLanguage;}

DSS_listKMs.prototype.getClientLanguage = DSS_listKMs_getClientLanguage;

function DSS_listKMs_setClientLanguage(value) { this._clientLanguage = value;}

DSS_listKMs.prototype.setClientLanguage = DSS_listKMs_setClientLanguage;
//
// accessor is DSS_listKMs.prototype.getKmTraitInclusionSpecification
// element get for kmTraitInclusionSpecification
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMTraitInclusionSpecification
// - required element
//
// element set for kmTraitInclusionSpecification
// setter function is is DSS_listKMs.prototype.setKmTraitInclusionSpecification
//
function DSS_listKMs_getKmTraitInclusionSpecification() { return this._kmTraitInclusionSpecification;}

DSS_listKMs.prototype.getKmTraitInclusionSpecification = DSS_listKMs_getKmTraitInclusionSpecification;

function DSS_listKMs_setKmTraitInclusionSpecification(value) { this._kmTraitInclusionSpecification = value;}

DSS_listKMs.prototype.setKmTraitInclusionSpecification = DSS_listKMs_setKmTraitInclusionSpecification;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}listKMs
//
function DSS_listKMs_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + '<clientLanguage>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._clientLanguage);
     xml = xml + '</clientLanguage>';
    }
    // block for local variables
    {
     xml = xml + this._kmTraitInclusionSpecification.serialize(cxfjsutils, 'kmTraitInclusionSpecification', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_listKMs.prototype.serialize = DSS_listKMs_serialize;

function DSS_listKMs_deserialize (cxfjsutils, element) {
    var newobject = new DSS_listKMs();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing clientLanguage');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setClientLanguage(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmTraitInclusionSpecification');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_KMTraitInclusionSpecification_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmTraitInclusionSpecification(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}evaluateIteratively
//
function DSS_evaluateIteratively () {
    this.typeMarker = 'DSS_evaluateIteratively';
    this._interactionId = null;
    this._iterativeEvaluationRequest = null;
}

//
// accessor is DSS_evaluateIteratively.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_evaluateIteratively.prototype.setInteractionId
//
function DSS_evaluateIteratively_getInteractionId() { return this._interactionId;}

DSS_evaluateIteratively.prototype.getInteractionId = DSS_evaluateIteratively_getInteractionId;

function DSS_evaluateIteratively_setInteractionId(value) { this._interactionId = value;}

DSS_evaluateIteratively.prototype.setInteractionId = DSS_evaluateIteratively_setInteractionId;
//
// accessor is DSS_evaluateIteratively.prototype.getIterativeEvaluationRequest
// element get for iterativeEvaluationRequest
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationRequest
// - required element
//
// element set for iterativeEvaluationRequest
// setter function is is DSS_evaluateIteratively.prototype.setIterativeEvaluationRequest
//
function DSS_evaluateIteratively_getIterativeEvaluationRequest() { return this._iterativeEvaluationRequest;}

DSS_evaluateIteratively.prototype.getIterativeEvaluationRequest = DSS_evaluateIteratively_getIterativeEvaluationRequest;

function DSS_evaluateIteratively_setIterativeEvaluationRequest(value) { this._iterativeEvaluationRequest = value;}

DSS_evaluateIteratively.prototype.setIterativeEvaluationRequest = DSS_evaluateIteratively_setIterativeEvaluationRequest;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}evaluateIteratively
//
function DSS_evaluateIteratively_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + this._iterativeEvaluationRequest.serialize(cxfjsutils, 'iterativeEvaluationRequest', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_evaluateIteratively.prototype.serialize = DSS_evaluateIteratively_serialize;

function DSS_evaluateIteratively_deserialize (cxfjsutils, element) {
    var newobject = new DSS_evaluateIteratively();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing iterativeEvaluationRequest');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_IterativeEvaluationRequest_deserialize(cxfjsutils, curElement);
    }
    newobject.setIterativeEvaluationRequest(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}describeSemanticSignifierResponse
//
function DSS_describeSemanticSignifierResponse () {
    this.typeMarker = 'DSS_describeSemanticSignifierResponse';
    this._semanticSignifier = null;
}

//
// accessor is DSS_describeSemanticSignifierResponse.prototype.getSemanticSignifier
// element get for semanticSignifier
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticSignifier
// - required element
//
// element set for semanticSignifier
// setter function is is DSS_describeSemanticSignifierResponse.prototype.setSemanticSignifier
//
function DSS_describeSemanticSignifierResponse_getSemanticSignifier() { return this._semanticSignifier;}

DSS_describeSemanticSignifierResponse.prototype.getSemanticSignifier = DSS_describeSemanticSignifierResponse_getSemanticSignifier;

function DSS_describeSemanticSignifierResponse_setSemanticSignifier(value) { this._semanticSignifier = value;}

DSS_describeSemanticSignifierResponse.prototype.setSemanticSignifier = DSS_describeSemanticSignifierResponse_setSemanticSignifier;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}describeSemanticSignifierResponse
//
function DSS_describeSemanticSignifierResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._semanticSignifier.serialize(cxfjsutils, 'semanticSignifier', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_describeSemanticSignifierResponse.prototype.serialize = DSS_describeSemanticSignifierResponse_serialize;

function DSS_describeSemanticSignifierResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_describeSemanticSignifierResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing semanticSignifier');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_SemanticSignifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setSemanticSignifier(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}RelatedKM
//
function DSS_RelatedKM () {
    this.typeMarker = 'DSS_RelatedKM';
    this._relationshipType = '';
    this._relatedKmId = null;
}

//
// accessor is DSS_RelatedKM.prototype.getRelationshipType
// element get for relationshipType
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMRelationshipType
// - required element
//
// element set for relationshipType
// setter function is is DSS_RelatedKM.prototype.setRelationshipType
//
function DSS_RelatedKM_getRelationshipType() { return this._relationshipType;}

DSS_RelatedKM.prototype.getRelationshipType = DSS_RelatedKM_getRelationshipType;

function DSS_RelatedKM_setRelationshipType(value) { this._relationshipType = value;}

DSS_RelatedKM.prototype.setRelationshipType = DSS_RelatedKM_setRelationshipType;
//
// accessor is DSS_RelatedKM.prototype.getRelatedKmId
// element get for relatedKmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for relatedKmId
// setter function is is DSS_RelatedKM.prototype.setRelatedKmId
//
function DSS_RelatedKM_getRelatedKmId() { return this._relatedKmId;}

DSS_RelatedKM.prototype.getRelatedKmId = DSS_RelatedKM_getRelatedKmId;

function DSS_RelatedKM_setRelatedKmId(value) { this._relatedKmId = value;}

DSS_RelatedKM.prototype.setRelatedKmId = DSS_RelatedKM_setRelatedKmId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}RelatedKM
//
function DSS_RelatedKM_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<relationshipType>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._relationshipType);
     xml = xml + '</relationshipType>';
    }
    // block for local variables
    {
     xml = xml + this._relatedKmId.serialize(cxfjsutils, 'relatedKmId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_RelatedKM.prototype.serialize = DSS_RelatedKM_serialize;

function DSS_RelatedKM_deserialize (cxfjsutils, element) {
    var newobject = new DSS_RelatedKM();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing relationshipType');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setRelationshipType(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing relatedKmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setRelatedKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}describeTrait
//
function DSS_describeTrait () {
    this.typeMarker = 'DSS_describeTrait';
    this._interactionId = null;
    this._traitId = null;
}

//
// accessor is DSS_describeTrait.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_describeTrait.prototype.setInteractionId
//
function DSS_describeTrait_getInteractionId() { return this._interactionId;}

DSS_describeTrait.prototype.getInteractionId = DSS_describeTrait_getInteractionId;

function DSS_describeTrait_setInteractionId(value) { this._interactionId = value;}

DSS_describeTrait.prototype.setInteractionId = DSS_describeTrait_setInteractionId;
//
// accessor is DSS_describeTrait.prototype.getTraitId
// element get for traitId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for traitId
// setter function is is DSS_describeTrait.prototype.setTraitId
//
function DSS_describeTrait_getTraitId() { return this._traitId;}

DSS_describeTrait.prototype.getTraitId = DSS_describeTrait_getTraitId;

function DSS_describeTrait_setTraitId(value) { this._traitId = value;}

DSS_describeTrait.prototype.setTraitId = DSS_describeTrait_setTraitId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}describeTrait
//
function DSS_describeTrait_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + this._traitId.serialize(cxfjsutils, 'traitId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_describeTrait.prototype.serialize = DSS_describeTrait_serialize;

function DSS_describeTrait_deserialize (cxfjsutils, element) {
    var newobject = new DSS_describeTrait();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing traitId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setTraitId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirements
//
function DSS_KMDataRequirements () {
    this.typeMarker = 'DSS_KMDataRequirements';
    this._initialDataRequirementGroup = [];
    this._additionalDataRequirementGroup = [];
    this._consumerProvidedQueryParameter = [];
}

//
// accessor is DSS_KMDataRequirements.prototype.getInitialDataRequirementGroup
// element get for initialDataRequirementGroup
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementGroup
// - required element
// - array
//
// element set for initialDataRequirementGroup
// setter function is is DSS_KMDataRequirements.prototype.setInitialDataRequirementGroup
//
function DSS_KMDataRequirements_getInitialDataRequirementGroup() { return this._initialDataRequirementGroup;}

DSS_KMDataRequirements.prototype.getInitialDataRequirementGroup = DSS_KMDataRequirements_getInitialDataRequirementGroup;

function DSS_KMDataRequirements_setInitialDataRequirementGroup(value) { this._initialDataRequirementGroup = value;}

DSS_KMDataRequirements.prototype.setInitialDataRequirementGroup = DSS_KMDataRequirements_setInitialDataRequirementGroup;
//
// accessor is DSS_KMDataRequirements.prototype.getAdditionalDataRequirementGroup
// element get for additionalDataRequirementGroup
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementGroup
// - required element
// - array
//
// element set for additionalDataRequirementGroup
// setter function is is DSS_KMDataRequirements.prototype.setAdditionalDataRequirementGroup
//
function DSS_KMDataRequirements_getAdditionalDataRequirementGroup() { return this._additionalDataRequirementGroup;}

DSS_KMDataRequirements.prototype.getAdditionalDataRequirementGroup = DSS_KMDataRequirements_getAdditionalDataRequirementGroup;

function DSS_KMDataRequirements_setAdditionalDataRequirementGroup(value) { this._additionalDataRequirementGroup = value;}

DSS_KMDataRequirements.prototype.setAdditionalDataRequirementGroup = DSS_KMDataRequirements_setAdditionalDataRequirementGroup;
//
// accessor is DSS_KMDataRequirements.prototype.getConsumerProvidedQueryParameter
// element get for consumerProvidedQueryParameter
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMConsumerProvidedQueryParameter
// - required element
// - array
//
// element set for consumerProvidedQueryParameter
// setter function is is DSS_KMDataRequirements.prototype.setConsumerProvidedQueryParameter
//
function DSS_KMDataRequirements_getConsumerProvidedQueryParameter() { return this._consumerProvidedQueryParameter;}

DSS_KMDataRequirements.prototype.getConsumerProvidedQueryParameter = DSS_KMDataRequirements_getConsumerProvidedQueryParameter;

function DSS_KMDataRequirements_setConsumerProvidedQueryParameter(value) { this._consumerProvidedQueryParameter = value;}

DSS_KMDataRequirements.prototype.setConsumerProvidedQueryParameter = DSS_KMDataRequirements_setConsumerProvidedQueryParameter;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirements
//
function DSS_KMDataRequirements_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._initialDataRequirementGroup != null) {
      for (var ax = 0;ax < this._initialDataRequirementGroup.length;ax ++) {
       if (this._initialDataRequirementGroup[ax] == null) {
        xml = xml + '<initialDataRequirementGroup/>';
       } else {
        xml = xml + this._initialDataRequirementGroup[ax].serialize(cxfjsutils, 'initialDataRequirementGroup', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._additionalDataRequirementGroup != null) {
      for (var ax = 0;ax < this._additionalDataRequirementGroup.length;ax ++) {
       if (this._additionalDataRequirementGroup[ax] == null) {
        xml = xml + '<additionalDataRequirementGroup/>';
       } else {
        xml = xml + this._additionalDataRequirementGroup[ax].serialize(cxfjsutils, 'additionalDataRequirementGroup', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._consumerProvidedQueryParameter != null) {
      for (var ax = 0;ax < this._consumerProvidedQueryParameter.length;ax ++) {
       if (this._consumerProvidedQueryParameter[ax] == null) {
        xml = xml + '<consumerProvidedQueryParameter/>';
       } else {
        xml = xml + this._consumerProvidedQueryParameter[ax].serialize(cxfjsutils, 'consumerProvidedQueryParameter', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMDataRequirements.prototype.serialize = DSS_KMDataRequirements_serialize;

function DSS_KMDataRequirements_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMDataRequirements();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing initialDataRequirementGroup');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'initialDataRequirementGroup')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_KMDataRequirementGroup_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'initialDataRequirementGroup'));
     newobject.setInitialDataRequirementGroup(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing additionalDataRequirementGroup');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'additionalDataRequirementGroup')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_KMDataRequirementGroup_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'additionalDataRequirementGroup'));
     newobject.setAdditionalDataRequirementGroup(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing consumerProvidedQueryParameter');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'consumerProvidedQueryParameter')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_KMConsumerProvidedQueryParameter_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'consumerProvidedQueryParameter'));
     newobject.setConsumerProvidedQueryParameter(item);
     var item = null;
    }
    return newobject;
}

//
// Simple type (enumeration) {http://www.omg.org/spec/CDSS/201105/dss}ErrorMessage
//
//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMItem
//
function DSS_KMItem () {
    this.typeMarker = 'DSS_KMItem';
    this._name = '';
    this._description = '';
    this._id = null;
}

//
// accessor is DSS_KMItem.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_KMItem.prototype.setName
//
function DSS_KMItem_getName() { return this._name;}

DSS_KMItem.prototype.getName = DSS_KMItem_getName;

function DSS_KMItem_setName(value) { this._name = value;}

DSS_KMItem.prototype.setName = DSS_KMItem_setName;
//
// accessor is DSS_KMItem.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_KMItem.prototype.setDescription
//
function DSS_KMItem_getDescription() { return this._description;}

DSS_KMItem.prototype.getDescription = DSS_KMItem_getDescription;

function DSS_KMItem_setDescription(value) { this._description = value;}

DSS_KMItem.prototype.setDescription = DSS_KMItem_setDescription;
//
// accessor is DSS_KMItem.prototype.getId
// element get for id
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
//
// element set for id
// setter function is is DSS_KMItem.prototype.setId
//
function DSS_KMItem_getId() { return this._id;}

DSS_KMItem.prototype.getId = DSS_KMItem_getId;

function DSS_KMItem_setId(value) { this._id = value;}

DSS_KMItem.prototype.setId = DSS_KMItem_setId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMItem
//
function DSS_KMItem_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._id.serialize(cxfjsutils, 'id', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMItem.prototype.serialize = DSS_KMItem_serialize;

function DSS_KMItem_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMItem();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing id');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMCriterion
//
function DSS_KMCriterion () {
    this.typeMarker = 'DSS_KMCriterion';
}

//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMCriterion
//
function DSS_KMCriterion_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMCriterion.prototype.serialize = DSS_KMCriterion_serialize;

function DSS_KMCriterion_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMCriterion();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyAtSpecifiedTimeResponse
//
function DSS_evaluateIterativelyAtSpecifiedTimeResponse () {
    this.typeMarker = 'DSS_evaluateIterativelyAtSpecifiedTimeResponse';
    this._iterativeEvaluationResponse = null;
}

//
// accessor is DSS_evaluateIterativelyAtSpecifiedTimeResponse.prototype.getIterativeEvaluationResponse
// element get for iterativeEvaluationResponse
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationResponse
// - required element
//
// element set for iterativeEvaluationResponse
// setter function is is DSS_evaluateIterativelyAtSpecifiedTimeResponse.prototype.setIterativeEvaluationResponse
//
function DSS_evaluateIterativelyAtSpecifiedTimeResponse_getIterativeEvaluationResponse() { return this._iterativeEvaluationResponse;}

DSS_evaluateIterativelyAtSpecifiedTimeResponse.prototype.getIterativeEvaluationResponse = DSS_evaluateIterativelyAtSpecifiedTimeResponse_getIterativeEvaluationResponse;

function DSS_evaluateIterativelyAtSpecifiedTimeResponse_setIterativeEvaluationResponse(value) { this._iterativeEvaluationResponse = value;}

DSS_evaluateIterativelyAtSpecifiedTimeResponse.prototype.setIterativeEvaluationResponse = DSS_evaluateIterativelyAtSpecifiedTimeResponse_setIterativeEvaluationResponse;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyAtSpecifiedTimeResponse
//
function DSS_evaluateIterativelyAtSpecifiedTimeResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._iterativeEvaluationResponse.serialize(cxfjsutils, 'iterativeEvaluationResponse', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_evaluateIterativelyAtSpecifiedTimeResponse.prototype.serialize = DSS_evaluateIterativelyAtSpecifiedTimeResponse_serialize;

function DSS_evaluateIterativelyAtSpecifiedTimeResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_evaluateIterativelyAtSpecifiedTimeResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing iterativeEvaluationResponse');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_IterativeEvaluationResponse_deserialize(cxfjsutils, curElement);
    }
    newobject.setIterativeEvaluationResponse(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}RankedKM
//
function DSS_RankedKM () {
    this.typeMarker = 'DSS_RankedKM';
    this._kmSearchScore = '';
    this._kmDescription = null;
}

//
// accessor is DSS_RankedKM.prototype.getKmSearchScore
// element get for kmSearchScore
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMSearchScore
// - required element
//
// element set for kmSearchScore
// setter function is is DSS_RankedKM.prototype.setKmSearchScore
//
function DSS_RankedKM_getKmSearchScore() { return this._kmSearchScore;}

DSS_RankedKM.prototype.getKmSearchScore = DSS_RankedKM_getKmSearchScore;

function DSS_RankedKM_setKmSearchScore(value) { this._kmSearchScore = value;}

DSS_RankedKM.prototype.setKmSearchScore = DSS_RankedKM_setKmSearchScore;
//
// accessor is DSS_RankedKM.prototype.getKmDescription
// element get for kmDescription
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMDescription
// - required element
//
// element set for kmDescription
// setter function is is DSS_RankedKM.prototype.setKmDescription
//
function DSS_RankedKM_getKmDescription() { return this._kmDescription;}

DSS_RankedKM.prototype.getKmDescription = DSS_RankedKM_getKmDescription;

function DSS_RankedKM_setKmDescription(value) { this._kmDescription = value;}

DSS_RankedKM.prototype.setKmDescription = DSS_RankedKM_setKmDescription;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}RankedKM
//
function DSS_RankedKM_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<kmSearchScore>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._kmSearchScore);
     xml = xml + '</kmSearchScore>';
    }
    // block for local variables
    {
     xml = xml + this._kmDescription.serialize(cxfjsutils, 'kmDescription', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_RankedKM.prototype.serialize = DSS_RankedKM_serialize;

function DSS_RankedKM_deserialize (cxfjsutils, element) {
    var newobject = new DSS_RankedKM();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmSearchScore');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setKmSearchScore(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmDescription');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_KMDescription_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}ExtendedKMDescription
//
function DSS_ExtendedKMDescription () {
    this.typeMarker = 'DSS_ExtendedKMDescription';
    this._kmId = null;
    this._status = '';
    this._traitValue = [];
    this._relatedKM = [];
}

//
// accessor is DSS_ExtendedKMDescription.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_ExtendedKMDescription.prototype.setKmId
//
function DSS_ExtendedKMDescription_getKmId() { return this._kmId;}

DSS_ExtendedKMDescription.prototype.getKmId = DSS_ExtendedKMDescription_getKmId;

function DSS_ExtendedKMDescription_setKmId(value) { this._kmId = value;}

DSS_ExtendedKMDescription.prototype.setKmId = DSS_ExtendedKMDescription_setKmId;
//
// accessor is DSS_ExtendedKMDescription.prototype.getStatus
// element get for status
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMStatus
// - required element
//
// element set for status
// setter function is is DSS_ExtendedKMDescription.prototype.setStatus
//
function DSS_ExtendedKMDescription_getStatus() { return this._status;}

DSS_ExtendedKMDescription.prototype.getStatus = DSS_ExtendedKMDescription_getStatus;

function DSS_ExtendedKMDescription_setStatus(value) { this._status = value;}

DSS_ExtendedKMDescription.prototype.setStatus = DSS_ExtendedKMDescription_setStatus;
//
// accessor is DSS_ExtendedKMDescription.prototype.getTraitValue
// element get for traitValue
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMTraitValue
// - required element
// - array
//
// element set for traitValue
// setter function is is DSS_ExtendedKMDescription.prototype.setTraitValue
//
function DSS_ExtendedKMDescription_getTraitValue() { return this._traitValue;}

DSS_ExtendedKMDescription.prototype.getTraitValue = DSS_ExtendedKMDescription_getTraitValue;

function DSS_ExtendedKMDescription_setTraitValue(value) { this._traitValue = value;}

DSS_ExtendedKMDescription.prototype.setTraitValue = DSS_ExtendedKMDescription_setTraitValue;
//
// accessor is DSS_ExtendedKMDescription.prototype.getRelatedKM
// element get for relatedKM
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}RelatedKM
// - required element
// - array
//
// element set for relatedKM
// setter function is is DSS_ExtendedKMDescription.prototype.setRelatedKM
//
function DSS_ExtendedKMDescription_getRelatedKM() { return this._relatedKM;}

DSS_ExtendedKMDescription.prototype.getRelatedKM = DSS_ExtendedKMDescription_getRelatedKM;

function DSS_ExtendedKMDescription_setRelatedKM(value) { this._relatedKM = value;}

DSS_ExtendedKMDescription.prototype.setRelatedKM = DSS_ExtendedKMDescription_setRelatedKM;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}ExtendedKMDescription
//
function DSS_ExtendedKMDescription_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    // block for local variables
    {
     xml = xml + '<status>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._status);
     xml = xml + '</status>';
    }
    // block for local variables
    {
     if (this._traitValue != null) {
      for (var ax = 0;ax < this._traitValue.length;ax ++) {
       if (this._traitValue[ax] == null) {
        xml = xml + '<traitValue/>';
       } else {
        xml = xml + this._traitValue[ax].serialize(cxfjsutils, 'traitValue', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._relatedKM != null) {
      for (var ax = 0;ax < this._relatedKM.length;ax ++) {
       if (this._relatedKM[ax] == null) {
        xml = xml + '<relatedKM/>';
       } else {
        xml = xml + this._relatedKM[ax].serialize(cxfjsutils, 'relatedKM', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_ExtendedKMDescription.prototype.serialize = DSS_ExtendedKMDescription_serialize;

function DSS_ExtendedKMDescription_deserialize (cxfjsutils, element) {
    var newobject = new DSS_ExtendedKMDescription();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing status');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setStatus(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing traitValue');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'traitValue')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_KMTraitValue_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'traitValue'));
     newobject.setTraitValue(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing relatedKM');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'relatedKM')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_RelatedKM_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'relatedKM'));
     newobject.setRelatedKM(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMTraitCriterionValue
//
function DSS_KMTraitCriterionValue () {
    this.typeMarker = 'DSS_KMTraitCriterionValue';
    this._traitCriterionId = null;
    this._value = null;
}

//
// accessor is DSS_KMTraitCriterionValue.prototype.getTraitCriterionId
// element get for traitCriterionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
//
// element set for traitCriterionId
// setter function is is DSS_KMTraitCriterionValue.prototype.setTraitCriterionId
//
function DSS_KMTraitCriterionValue_getTraitCriterionId() { return this._traitCriterionId;}

DSS_KMTraitCriterionValue.prototype.getTraitCriterionId = DSS_KMTraitCriterionValue_getTraitCriterionId;

function DSS_KMTraitCriterionValue_setTraitCriterionId(value) { this._traitCriterionId = value;}

DSS_KMTraitCriterionValue.prototype.setTraitCriterionId = DSS_KMTraitCriterionValue_setTraitCriterionId;
//
// accessor is DSS_KMTraitCriterionValue.prototype.getValue
// element get for value
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticPayload
// - required element
//
// element set for value
// setter function is is DSS_KMTraitCriterionValue.prototype.setValue
//
function DSS_KMTraitCriterionValue_getValue() { return this._value;}

DSS_KMTraitCriterionValue.prototype.getValue = DSS_KMTraitCriterionValue_getValue;

function DSS_KMTraitCriterionValue_setValue(value) { this._value = value;}

DSS_KMTraitCriterionValue.prototype.setValue = DSS_KMTraitCriterionValue_setValue;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMTraitCriterionValue
//
function DSS_KMTraitCriterionValue_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._traitCriterionId.serialize(cxfjsutils, 'traitCriterionId', null);
    }
    // block for local variables
    {
     xml = xml + this._value.serialize(cxfjsutils, 'value', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMTraitCriterionValue.prototype.serialize = DSS_KMTraitCriterionValue_serialize;

function DSS_KMTraitCriterionValue_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMTraitCriterionValue();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing traitCriterionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setTraitCriterionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing value');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_SemanticPayload_deserialize(cxfjsutils, curElement);
    }
    newobject.setValue(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}ServiceProfile
//
function DSS_ServiceProfile () {
    this.typeMarker = 'DSS_ServiceProfile';
    this._name = '';
    this._description = '';
    this._entityId = null;
}

//
// accessor is DSS_ServiceProfile.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_ServiceProfile.prototype.setName
//
function DSS_ServiceProfile_getName() { return this._name;}

DSS_ServiceProfile.prototype.getName = DSS_ServiceProfile_getName;

function DSS_ServiceProfile_setName(value) { this._name = value;}

DSS_ServiceProfile.prototype.setName = DSS_ServiceProfile_setName;
//
// accessor is DSS_ServiceProfile.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_ServiceProfile.prototype.setDescription
//
function DSS_ServiceProfile_getDescription() { return this._description;}

DSS_ServiceProfile.prototype.getDescription = DSS_ServiceProfile_getDescription;

function DSS_ServiceProfile_setDescription(value) { this._description = value;}

DSS_ServiceProfile.prototype.setDescription = DSS_ServiceProfile_setDescription;
//
// accessor is DSS_ServiceProfile.prototype.getEntityId
// element get for entityId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for entityId
// setter function is is DSS_ServiceProfile.prototype.setEntityId
//
function DSS_ServiceProfile_getEntityId() { return this._entityId;}

DSS_ServiceProfile.prototype.getEntityId = DSS_ServiceProfile_getEntityId;

function DSS_ServiceProfile_setEntityId(value) { this._entityId = value;}

DSS_ServiceProfile.prototype.setEntityId = DSS_ServiceProfile_setEntityId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}ServiceProfile
//
function DSS_ServiceProfile_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._entityId.serialize(cxfjsutils, 'entityId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_ServiceProfile.prototype.serialize = DSS_ServiceProfile_serialize;

function DSS_ServiceProfile_deserialize (cxfjsutils, element) {
    var newobject = new DSS_ServiceProfile();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing entityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}Trait
//
function DSS_Trait () {
    this.typeMarker = 'DSS_Trait';
    this._name = '';
    this._description = '';
    this._entityId = null;
    this._traitValueIsLanguageDependent = '';
    this._informationModelSSId = null;
    this._allowedTraitCriterion = [];
}

//
// accessor is DSS_Trait.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_Trait.prototype.setName
//
function DSS_Trait_getName() { return this._name;}

DSS_Trait.prototype.getName = DSS_Trait_getName;

function DSS_Trait_setName(value) { this._name = value;}

DSS_Trait.prototype.setName = DSS_Trait_setName;
//
// accessor is DSS_Trait.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_Trait.prototype.setDescription
//
function DSS_Trait_getDescription() { return this._description;}

DSS_Trait.prototype.getDescription = DSS_Trait_getDescription;

function DSS_Trait_setDescription(value) { this._description = value;}

DSS_Trait.prototype.setDescription = DSS_Trait_setDescription;
//
// accessor is DSS_Trait.prototype.getEntityId
// element get for entityId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for entityId
// setter function is is DSS_Trait.prototype.setEntityId
//
function DSS_Trait_getEntityId() { return this._entityId;}

DSS_Trait.prototype.getEntityId = DSS_Trait_getEntityId;

function DSS_Trait_setEntityId(value) { this._entityId = value;}

DSS_Trait.prototype.setEntityId = DSS_Trait_setEntityId;
//
// accessor is DSS_Trait.prototype.getTraitValueIsLanguageDependent
// element get for traitValueIsLanguageDependent
// - element type is {http://www.w3.org/2001/XMLSchema}boolean
// - required element
//
// element set for traitValueIsLanguageDependent
// setter function is is DSS_Trait.prototype.setTraitValueIsLanguageDependent
//
function DSS_Trait_getTraitValueIsLanguageDependent() { return this._traitValueIsLanguageDependent;}

DSS_Trait.prototype.getTraitValueIsLanguageDependent = DSS_Trait_getTraitValueIsLanguageDependent;

function DSS_Trait_setTraitValueIsLanguageDependent(value) { this._traitValueIsLanguageDependent = value;}

DSS_Trait.prototype.setTraitValueIsLanguageDependent = DSS_Trait_setTraitValueIsLanguageDependent;
//
// accessor is DSS_Trait.prototype.getInformationModelSSId
// element get for informationModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for informationModelSSId
// setter function is is DSS_Trait.prototype.setInformationModelSSId
//
function DSS_Trait_getInformationModelSSId() { return this._informationModelSSId;}

DSS_Trait.prototype.getInformationModelSSId = DSS_Trait_getInformationModelSSId;

function DSS_Trait_setInformationModelSSId(value) { this._informationModelSSId = value;}

DSS_Trait.prototype.setInformationModelSSId = DSS_Trait_setInformationModelSSId;
//
// accessor is DSS_Trait.prototype.getAllowedTraitCriterion
// element get for allowedTraitCriterion
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}TraitCriterion
// - required element
// - array
//
// element set for allowedTraitCriterion
// setter function is is DSS_Trait.prototype.setAllowedTraitCriterion
//
function DSS_Trait_getAllowedTraitCriterion() { return this._allowedTraitCriterion;}

DSS_Trait.prototype.getAllowedTraitCriterion = DSS_Trait_getAllowedTraitCriterion;

function DSS_Trait_setAllowedTraitCriterion(value) { this._allowedTraitCriterion = value;}

DSS_Trait.prototype.setAllowedTraitCriterion = DSS_Trait_setAllowedTraitCriterion;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}Trait
//
function DSS_Trait_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._entityId.serialize(cxfjsutils, 'entityId', null);
    }
    // block for local variables
    {
     xml = xml + '<traitValueIsLanguageDependent>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._traitValueIsLanguageDependent);
     xml = xml + '</traitValueIsLanguageDependent>';
    }
    // block for local variables
    {
     xml = xml + this._informationModelSSId.serialize(cxfjsutils, 'informationModelSSId', null);
    }
    // block for local variables
    {
     if (this._allowedTraitCriterion != null) {
      for (var ax = 0;ax < this._allowedTraitCriterion.length;ax ++) {
       if (this._allowedTraitCriterion[ax] == null) {
        xml = xml + '<allowedTraitCriterion/>';
       } else {
        xml = xml + this._allowedTraitCriterion[ax].serialize(cxfjsutils, 'allowedTraitCriterion', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_Trait.prototype.serialize = DSS_Trait_serialize;

function DSS_Trait_deserialize (cxfjsutils, element) {
    var newobject = new DSS_Trait();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing entityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing traitValueIsLanguageDependent');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = (value == 'true');
    }
    newobject.setTraitValueIsLanguageDependent(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing informationModelSSId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInformationModelSSId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing allowedTraitCriterion');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'allowedTraitCriterion')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_TraitCriterion_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'allowedTraitCriterion'));
     newobject.setAllowedTraitCriterion(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMDescription
//
function DSS_KMDescription () {
    this.typeMarker = 'DSS_KMDescription';
    this._kmId = null;
    this._status = '';
    this._traitValue = [];
}

//
// accessor is DSS_KMDescription.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_KMDescription.prototype.setKmId
//
function DSS_KMDescription_getKmId() { return this._kmId;}

DSS_KMDescription.prototype.getKmId = DSS_KMDescription_getKmId;

function DSS_KMDescription_setKmId(value) { this._kmId = value;}

DSS_KMDescription.prototype.setKmId = DSS_KMDescription_setKmId;
//
// accessor is DSS_KMDescription.prototype.getStatus
// element get for status
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMStatus
// - required element
//
// element set for status
// setter function is is DSS_KMDescription.prototype.setStatus
//
function DSS_KMDescription_getStatus() { return this._status;}

DSS_KMDescription.prototype.getStatus = DSS_KMDescription_getStatus;

function DSS_KMDescription_setStatus(value) { this._status = value;}

DSS_KMDescription.prototype.setStatus = DSS_KMDescription_setStatus;
//
// accessor is DSS_KMDescription.prototype.getTraitValue
// element get for traitValue
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMTraitValue
// - required element
// - array
//
// element set for traitValue
// setter function is is DSS_KMDescription.prototype.setTraitValue
//
function DSS_KMDescription_getTraitValue() { return this._traitValue;}

DSS_KMDescription.prototype.getTraitValue = DSS_KMDescription_getTraitValue;

function DSS_KMDescription_setTraitValue(value) { this._traitValue = value;}

DSS_KMDescription.prototype.setTraitValue = DSS_KMDescription_setTraitValue;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMDescription
//
function DSS_KMDescription_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    // block for local variables
    {
     xml = xml + '<status>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._status);
     xml = xml + '</status>';
    }
    // block for local variables
    {
     if (this._traitValue != null) {
      for (var ax = 0;ax < this._traitValue.length;ax ++) {
       if (this._traitValue[ax] == null) {
        xml = xml + '<traitValue/>';
       } else {
        xml = xml + this._traitValue[ax].serialize(cxfjsutils, 'traitValue', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMDescription.prototype.serialize = DSS_KMDescription_serialize;

function DSS_KMDescription_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMDescription();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing status');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setStatus(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing traitValue');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'traitValue')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_KMTraitValue_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'traitValue'));
     newobject.setTraitValue(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}LanguageSupportRequirement
//
function DSS_LanguageSupportRequirement () {
    this.typeMarker = 'DSS_LanguageSupportRequirement';
    this._name = '';
    this._description = '';
    this._entityId = null;
    this._type = '';
    this._supportedLanguage = [];
}

//
// accessor is DSS_LanguageSupportRequirement.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_LanguageSupportRequirement.prototype.setName
//
function DSS_LanguageSupportRequirement_getName() { return this._name;}

DSS_LanguageSupportRequirement.prototype.getName = DSS_LanguageSupportRequirement_getName;

function DSS_LanguageSupportRequirement_setName(value) { this._name = value;}

DSS_LanguageSupportRequirement.prototype.setName = DSS_LanguageSupportRequirement_setName;
//
// accessor is DSS_LanguageSupportRequirement.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_LanguageSupportRequirement.prototype.setDescription
//
function DSS_LanguageSupportRequirement_getDescription() { return this._description;}

DSS_LanguageSupportRequirement.prototype.getDescription = DSS_LanguageSupportRequirement_getDescription;

function DSS_LanguageSupportRequirement_setDescription(value) { this._description = value;}

DSS_LanguageSupportRequirement.prototype.setDescription = DSS_LanguageSupportRequirement_setDescription;
//
// accessor is DSS_LanguageSupportRequirement.prototype.getEntityId
// element get for entityId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for entityId
// setter function is is DSS_LanguageSupportRequirement.prototype.setEntityId
//
function DSS_LanguageSupportRequirement_getEntityId() { return this._entityId;}

DSS_LanguageSupportRequirement.prototype.getEntityId = DSS_LanguageSupportRequirement_getEntityId;

function DSS_LanguageSupportRequirement_setEntityId(value) { this._entityId = value;}

DSS_LanguageSupportRequirement.prototype.setEntityId = DSS_LanguageSupportRequirement_setEntityId;
//
// accessor is DSS_LanguageSupportRequirement.prototype.getType
// element get for type
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticRequirementType
// - required element
//
// element set for type
// setter function is is DSS_LanguageSupportRequirement.prototype.setType
//
function DSS_LanguageSupportRequirement_getType() { return this._type;}

DSS_LanguageSupportRequirement.prototype.getType = DSS_LanguageSupportRequirement_getType;

function DSS_LanguageSupportRequirement_setType(value) { this._type = value;}

DSS_LanguageSupportRequirement.prototype.setType = DSS_LanguageSupportRequirement_setType;
//
// accessor is DSS_LanguageSupportRequirement.prototype.getSupportedLanguage
// element get for supportedLanguage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Language
// - required element
// - array
//
// element set for supportedLanguage
// setter function is is DSS_LanguageSupportRequirement.prototype.setSupportedLanguage
//
function DSS_LanguageSupportRequirement_getSupportedLanguage() { return this._supportedLanguage;}

DSS_LanguageSupportRequirement.prototype.getSupportedLanguage = DSS_LanguageSupportRequirement_getSupportedLanguage;

function DSS_LanguageSupportRequirement_setSupportedLanguage(value) { this._supportedLanguage = value;}

DSS_LanguageSupportRequirement.prototype.setSupportedLanguage = DSS_LanguageSupportRequirement_setSupportedLanguage;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}LanguageSupportRequirement
//
function DSS_LanguageSupportRequirement_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._entityId.serialize(cxfjsutils, 'entityId', null);
    }
    // block for local variables
    {
     xml = xml + '<type>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._type);
     xml = xml + '</type>';
    }
    // block for local variables
    {
     if (this._supportedLanguage != null) {
      for (var ax = 0;ax < this._supportedLanguage.length;ax ++) {
       if (this._supportedLanguage[ax] == null) {
        xml = xml + '<supportedLanguage/>';
       } else {
        xml = xml + '<supportedLanguage>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._supportedLanguage[ax]);
        xml = xml + '</supportedLanguage>';
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_LanguageSupportRequirement.prototype.serialize = DSS_LanguageSupportRequirement_serialize;

function DSS_LanguageSupportRequirement_deserialize (cxfjsutils, element) {
    var newobject = new DSS_LanguageSupportRequirement();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing entityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing type');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setType(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing supportedLanguage');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'supportedLanguage')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'supportedLanguage'));
     newobject.setSupportedLanguage(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}EvaluationResultCriterion
//
function DSS_EvaluationResultCriterion () {
    this.typeMarker = 'DSS_EvaluationResultCriterion';
    this._informationModelSSId = null;
}

//
// accessor is DSS_EvaluationResultCriterion.prototype.getInformationModelSSId
// element get for informationModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for informationModelSSId
// setter function is is DSS_EvaluationResultCriterion.prototype.setInformationModelSSId
//
function DSS_EvaluationResultCriterion_getInformationModelSSId() { return this._informationModelSSId;}

DSS_EvaluationResultCriterion.prototype.getInformationModelSSId = DSS_EvaluationResultCriterion_getInformationModelSSId;

function DSS_EvaluationResultCriterion_setInformationModelSSId(value) { this._informationModelSSId = value;}

DSS_EvaluationResultCriterion.prototype.setInformationModelSSId = DSS_EvaluationResultCriterion_setInformationModelSSId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}EvaluationResultCriterion
//
function DSS_EvaluationResultCriterion_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._informationModelSSId.serialize(cxfjsutils, 'informationModelSSId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_EvaluationResultCriterion.prototype.serialize = DSS_EvaluationResultCriterion_serialize;

function DSS_EvaluationResultCriterion_deserialize (cxfjsutils, element) {
    var newobject = new DSS_EvaluationResultCriterion();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing informationModelSSId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInformationModelSSId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}ScopingEntity
//
function DSS_ScopingEntity () {
    this.typeMarker = 'DSS_ScopingEntity';
    this._name = '';
    this._description = '';
    this._childScopingEntity = [];
    this._id = '';
    this._parentSEId = null;
}

//
// accessor is DSS_ScopingEntity.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_ScopingEntity.prototype.setName
//
function DSS_ScopingEntity_getName() { return this._name;}

DSS_ScopingEntity.prototype.getName = DSS_ScopingEntity_getName;

function DSS_ScopingEntity_setName(value) { this._name = value;}

DSS_ScopingEntity.prototype.setName = DSS_ScopingEntity_setName;
//
// accessor is DSS_ScopingEntity.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_ScopingEntity.prototype.setDescription
//
function DSS_ScopingEntity_getDescription() { return this._description;}

DSS_ScopingEntity.prototype.getDescription = DSS_ScopingEntity_getDescription;

function DSS_ScopingEntity_setDescription(value) { this._description = value;}

DSS_ScopingEntity.prototype.setDescription = DSS_ScopingEntity_setDescription;
//
// accessor is DSS_ScopingEntity.prototype.getChildScopingEntity
// element get for childScopingEntity
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ScopingEntity
// - required element
// - array
//
// element set for childScopingEntity
// setter function is is DSS_ScopingEntity.prototype.setChildScopingEntity
//
function DSS_ScopingEntity_getChildScopingEntity() { return this._childScopingEntity;}

DSS_ScopingEntity.prototype.getChildScopingEntity = DSS_ScopingEntity_getChildScopingEntity;

function DSS_ScopingEntity_setChildScopingEntity(value) { this._childScopingEntity = value;}

DSS_ScopingEntity.prototype.setChildScopingEntity = DSS_ScopingEntity_setChildScopingEntity;
//
// accessor is DSS_ScopingEntity.prototype.getId
// element get for id
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for id
// setter function is is DSS_ScopingEntity.prototype.setId
//
function DSS_ScopingEntity_getId() { return this._id;}

DSS_ScopingEntity.prototype.getId = DSS_ScopingEntity_getId;

function DSS_ScopingEntity_setId(value) { this._id = value;}

DSS_ScopingEntity.prototype.setId = DSS_ScopingEntity_setId;
//
// accessor is DSS_ScopingEntity.prototype.getParentSEId
// element get for parentSEId
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - optional element
//
// element set for parentSEId
// setter function is is DSS_ScopingEntity.prototype.setParentSEId
//
function DSS_ScopingEntity_getParentSEId() { return this._parentSEId;}

DSS_ScopingEntity.prototype.getParentSEId = DSS_ScopingEntity_getParentSEId;

function DSS_ScopingEntity_setParentSEId(value) { this._parentSEId = value;}

DSS_ScopingEntity.prototype.setParentSEId = DSS_ScopingEntity_setParentSEId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}ScopingEntity
//
function DSS_ScopingEntity_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     if (this._childScopingEntity != null) {
      for (var ax = 0;ax < this._childScopingEntity.length;ax ++) {
       if (this._childScopingEntity[ax] == null) {
        xml = xml + '<childScopingEntity/>';
       } else {
        xml = xml + this._childScopingEntity[ax].serialize(cxfjsutils, 'childScopingEntity', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_ScopingEntity.prototype.serialize = DSS_ScopingEntity_serialize;

function DSS_ScopingEntity_deserialize (cxfjsutils, element) {
    var newobject = new DSS_ScopingEntity();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing childScopingEntity');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'childScopingEntity')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_ScopingEntity_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'childScopingEntity'));
     newobject.setChildScopingEntity(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationRequest
//
function DSS_IterativeEvaluationRequest () {
    this.typeMarker = 'DSS_IterativeEvaluationRequest';
    this._iterativeKMEvaluationRequest = [];
    this._dataRequirementItemData = [];
    this._clientLanguage = '';
    this._clientTimeZoneOffset = '';
}

//
// accessor is DSS_IterativeEvaluationRequest.prototype.getIterativeKMEvaluationRequest
// element get for iterativeKMEvaluationRequest
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}IterativeKMEvaluationRequest
// - required element
// - array
//
// element set for iterativeKMEvaluationRequest
// setter function is is DSS_IterativeEvaluationRequest.prototype.setIterativeKMEvaluationRequest
//
function DSS_IterativeEvaluationRequest_getIterativeKMEvaluationRequest() { return this._iterativeKMEvaluationRequest;}

DSS_IterativeEvaluationRequest.prototype.getIterativeKMEvaluationRequest = DSS_IterativeEvaluationRequest_getIterativeKMEvaluationRequest;

function DSS_IterativeEvaluationRequest_setIterativeKMEvaluationRequest(value) { this._iterativeKMEvaluationRequest = value;}

DSS_IterativeEvaluationRequest.prototype.setIterativeKMEvaluationRequest = DSS_IterativeEvaluationRequest_setIterativeKMEvaluationRequest;
//
// accessor is DSS_IterativeEvaluationRequest.prototype.getDataRequirementItemData
// element get for dataRequirementItemData
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}DataRequirementItemData
// - required element
// - array
//
// element set for dataRequirementItemData
// setter function is is DSS_IterativeEvaluationRequest.prototype.setDataRequirementItemData
//
function DSS_IterativeEvaluationRequest_getDataRequirementItemData() { return this._dataRequirementItemData;}

DSS_IterativeEvaluationRequest.prototype.getDataRequirementItemData = DSS_IterativeEvaluationRequest_getDataRequirementItemData;

function DSS_IterativeEvaluationRequest_setDataRequirementItemData(value) { this._dataRequirementItemData = value;}

DSS_IterativeEvaluationRequest.prototype.setDataRequirementItemData = DSS_IterativeEvaluationRequest_setDataRequirementItemData;
//
// accessor is DSS_IterativeEvaluationRequest.prototype.getClientLanguage
// element get for clientLanguage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Language
// - required element
//
// element set for clientLanguage
// setter function is is DSS_IterativeEvaluationRequest.prototype.setClientLanguage
//
function DSS_IterativeEvaluationRequest_getClientLanguage() { return this._clientLanguage;}

DSS_IterativeEvaluationRequest.prototype.getClientLanguage = DSS_IterativeEvaluationRequest_getClientLanguage;

function DSS_IterativeEvaluationRequest_setClientLanguage(value) { this._clientLanguage = value;}

DSS_IterativeEvaluationRequest.prototype.setClientLanguage = DSS_IterativeEvaluationRequest_setClientLanguage;
//
// accessor is DSS_IterativeEvaluationRequest.prototype.getClientTimeZoneOffset
// element get for clientTimeZoneOffset
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for clientTimeZoneOffset
// setter function is is DSS_IterativeEvaluationRequest.prototype.setClientTimeZoneOffset
//
function DSS_IterativeEvaluationRequest_getClientTimeZoneOffset() { return this._clientTimeZoneOffset;}

DSS_IterativeEvaluationRequest.prototype.getClientTimeZoneOffset = DSS_IterativeEvaluationRequest_getClientTimeZoneOffset;

function DSS_IterativeEvaluationRequest_setClientTimeZoneOffset(value) { this._clientTimeZoneOffset = value;}

DSS_IterativeEvaluationRequest.prototype.setClientTimeZoneOffset = DSS_IterativeEvaluationRequest_setClientTimeZoneOffset;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationRequest
//
function DSS_IterativeEvaluationRequest_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._iterativeKMEvaluationRequest != null) {
      for (var ax = 0;ax < this._iterativeKMEvaluationRequest.length;ax ++) {
       if (this._iterativeKMEvaluationRequest[ax] == null) {
        xml = xml + '<iterativeKMEvaluationRequest/>';
       } else {
        xml = xml + this._iterativeKMEvaluationRequest[ax].serialize(cxfjsutils, 'iterativeKMEvaluationRequest', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._dataRequirementItemData != null) {
      for (var ax = 0;ax < this._dataRequirementItemData.length;ax ++) {
       if (this._dataRequirementItemData[ax] == null) {
        xml = xml + '<dataRequirementItemData/>';
       } else {
        xml = xml + this._dataRequirementItemData[ax].serialize(cxfjsutils, 'dataRequirementItemData', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_IterativeEvaluationRequest.prototype.serialize = DSS_IterativeEvaluationRequest_serialize;

function DSS_IterativeEvaluationRequest_deserialize (cxfjsutils, element) {
    var newobject = new DSS_IterativeEvaluationRequest();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing iterativeKMEvaluationRequest');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'iterativeKMEvaluationRequest')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_IterativeKMEvaluationRequest_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'iterativeKMEvaluationRequest'));
     newobject.setIterativeKMEvaluationRequest(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing dataRequirementItemData');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'dataRequirementItemData')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_DataRequirementItemData_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'dataRequirementItemData'));
     newobject.setDataRequirementItemData(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}getKMEvaluationResultSemanticsResponse
//
function DSS_getKMEvaluationResultSemanticsResponse () {
    this.typeMarker = 'DSS_getKMEvaluationResultSemanticsResponse';
    this._kmEvaluationResultSemanticsList = null;
}

//
// accessor is DSS_getKMEvaluationResultSemanticsResponse.prototype.getKmEvaluationResultSemanticsList
// element get for kmEvaluationResultSemanticsList
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultSemanticsList
// - required element
//
// element set for kmEvaluationResultSemanticsList
// setter function is is DSS_getKMEvaluationResultSemanticsResponse.prototype.setKmEvaluationResultSemanticsList
//
function DSS_getKMEvaluationResultSemanticsResponse_getKmEvaluationResultSemanticsList() { return this._kmEvaluationResultSemanticsList;}

DSS_getKMEvaluationResultSemanticsResponse.prototype.getKmEvaluationResultSemanticsList = DSS_getKMEvaluationResultSemanticsResponse_getKmEvaluationResultSemanticsList;

function DSS_getKMEvaluationResultSemanticsResponse_setKmEvaluationResultSemanticsList(value) { this._kmEvaluationResultSemanticsList = value;}

DSS_getKMEvaluationResultSemanticsResponse.prototype.setKmEvaluationResultSemanticsList = DSS_getKMEvaluationResultSemanticsResponse_setKmEvaluationResultSemanticsList;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}getKMEvaluationResultSemanticsResponse
//
function DSS_getKMEvaluationResultSemanticsResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmEvaluationResultSemanticsList.serialize(cxfjsutils, 'kmEvaluationResultSemanticsList', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_getKMEvaluationResultSemanticsResponse.prototype.serialize = DSS_getKMEvaluationResultSemanticsResponse_serialize;

function DSS_getKMEvaluationResultSemanticsResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_getKMEvaluationResultSemanticsResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmEvaluationResultSemanticsList');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_KMEvaluationResultSemanticsList_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmEvaluationResultSemanticsList(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}AllowedDataRequirement
//
function DSS_AllowedDataRequirement () {
    this.typeMarker = 'DSS_AllowedDataRequirement';
    this._informationModelSSId = null;
    this._allowedQueryModelSSId = [];
}

//
// accessor is DSS_AllowedDataRequirement.prototype.getInformationModelSSId
// element get for informationModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for informationModelSSId
// setter function is is DSS_AllowedDataRequirement.prototype.setInformationModelSSId
//
function DSS_AllowedDataRequirement_getInformationModelSSId() { return this._informationModelSSId;}

DSS_AllowedDataRequirement.prototype.getInformationModelSSId = DSS_AllowedDataRequirement_getInformationModelSSId;

function DSS_AllowedDataRequirement_setInformationModelSSId(value) { this._informationModelSSId = value;}

DSS_AllowedDataRequirement.prototype.setInformationModelSSId = DSS_AllowedDataRequirement_setInformationModelSSId;
//
// accessor is DSS_AllowedDataRequirement.prototype.getAllowedQueryModelSSId
// element get for allowedQueryModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
// - array
//
// element set for allowedQueryModelSSId
// setter function is is DSS_AllowedDataRequirement.prototype.setAllowedQueryModelSSId
//
function DSS_AllowedDataRequirement_getAllowedQueryModelSSId() { return this._allowedQueryModelSSId;}

DSS_AllowedDataRequirement.prototype.getAllowedQueryModelSSId = DSS_AllowedDataRequirement_getAllowedQueryModelSSId;

function DSS_AllowedDataRequirement_setAllowedQueryModelSSId(value) { this._allowedQueryModelSSId = value;}

DSS_AllowedDataRequirement.prototype.setAllowedQueryModelSSId = DSS_AllowedDataRequirement_setAllowedQueryModelSSId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}AllowedDataRequirement
//
function DSS_AllowedDataRequirement_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._informationModelSSId.serialize(cxfjsutils, 'informationModelSSId', null);
    }
    // block for local variables
    {
     if (this._allowedQueryModelSSId != null) {
      for (var ax = 0;ax < this._allowedQueryModelSSId.length;ax ++) {
       if (this._allowedQueryModelSSId[ax] == null) {
        xml = xml + '<allowedQueryModelSSId/>';
       } else {
        xml = xml + this._allowedQueryModelSSId[ax].serialize(cxfjsutils, 'allowedQueryModelSSId', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_AllowedDataRequirement.prototype.serialize = DSS_AllowedDataRequirement_serialize;

function DSS_AllowedDataRequirement_deserialize (cxfjsutils, element) {
    var newobject = new DSS_AllowedDataRequirement();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing informationModelSSId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInformationModelSSId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing allowedQueryModelSSId');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'allowedQueryModelSSId')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'allowedQueryModelSSId'));
     newobject.setAllowedQueryModelSSId(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}describeTraitResponse
//
function DSS_describeTraitResponse () {
    this.typeMarker = 'DSS_describeTraitResponse';
    this._trait = null;
}

//
// accessor is DSS_describeTraitResponse.prototype.getTrait
// element get for trait
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Trait
// - required element
//
// element set for trait
// setter function is is DSS_describeTraitResponse.prototype.setTrait
//
function DSS_describeTraitResponse_getTrait() { return this._trait;}

DSS_describeTraitResponse.prototype.getTrait = DSS_describeTraitResponse_getTrait;

function DSS_describeTraitResponse_setTrait(value) { this._trait = value;}

DSS_describeTraitResponse.prototype.setTrait = DSS_describeTraitResponse_setTrait;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}describeTraitResponse
//
function DSS_describeTraitResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._trait.serialize(cxfjsutils, 'trait', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_describeTraitResponse.prototype.serialize = DSS_describeTraitResponse_serialize;

function DSS_describeTraitResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_describeTraitResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing trait');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_Trait_deserialize(cxfjsutils, curElement);
    }
    newobject.setTrait(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}describeProfileResponse
//
function DSS_describeProfileResponse () {
    this.typeMarker = 'DSS_describeProfileResponse';
    this._serviceProfile = null;
}

//
// accessor is DSS_describeProfileResponse.prototype.getServiceProfile
// element get for serviceProfile
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ServiceProfile
// - required element
//
// element set for serviceProfile
// setter function is is DSS_describeProfileResponse.prototype.setServiceProfile
//
function DSS_describeProfileResponse_getServiceProfile() { return this._serviceProfile;}

DSS_describeProfileResponse.prototype.getServiceProfile = DSS_describeProfileResponse_getServiceProfile;

function DSS_describeProfileResponse_setServiceProfile(value) { this._serviceProfile = value;}

DSS_describeProfileResponse.prototype.setServiceProfile = DSS_describeProfileResponse_setServiceProfile;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}describeProfileResponse
//
function DSS_describeProfileResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._serviceProfile.serialize(cxfjsutils, 'serviceProfile', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_describeProfileResponse.prototype.serialize = DSS_describeProfileResponse_serialize;

function DSS_describeProfileResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_describeProfileResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing serviceProfile');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ServiceProfile_deserialize(cxfjsutils, curElement);
    }
    newobject.setServiceProfile(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedTraitCriterionException
//
function DSS_UnrecognizedTraitCriterionException () {
    this.typeMarker = 'DSS_UnrecognizedTraitCriterionException';
    this._errorMessage = [];
    this._itemId = null;
}

//
// accessor is DSS_UnrecognizedTraitCriterionException.prototype.getErrorMessage
// element get for errorMessage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ErrorMessage
// - required element
// - array
//
// element set for errorMessage
// setter function is is DSS_UnrecognizedTraitCriterionException.prototype.setErrorMessage
//
function DSS_UnrecognizedTraitCriterionException_getErrorMessage() { return this._errorMessage;}

DSS_UnrecognizedTraitCriterionException.prototype.getErrorMessage = DSS_UnrecognizedTraitCriterionException_getErrorMessage;

function DSS_UnrecognizedTraitCriterionException_setErrorMessage(value) { this._errorMessage = value;}

DSS_UnrecognizedTraitCriterionException.prototype.setErrorMessage = DSS_UnrecognizedTraitCriterionException_setErrorMessage;
//
// accessor is DSS_UnrecognizedTraitCriterionException.prototype.getItemId
// element get for itemId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
//
// element set for itemId
// setter function is is DSS_UnrecognizedTraitCriterionException.prototype.setItemId
//
function DSS_UnrecognizedTraitCriterionException_getItemId() { return this._itemId;}

DSS_UnrecognizedTraitCriterionException.prototype.getItemId = DSS_UnrecognizedTraitCriterionException_getItemId;

function DSS_UnrecognizedTraitCriterionException_setItemId(value) { this._itemId = value;}

DSS_UnrecognizedTraitCriterionException.prototype.setItemId = DSS_UnrecognizedTraitCriterionException_setItemId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedTraitCriterionException
//
function DSS_UnrecognizedTraitCriterionException_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._errorMessage != null) {
      for (var ax = 0;ax < this._errorMessage.length;ax ++) {
       if (this._errorMessage[ax] == null) {
        xml = xml + '<errorMessage/>';
       } else {
        xml = xml + '<errorMessage>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._errorMessage[ax]);
        xml = xml + '</errorMessage>';
       }
      }
     }
    }
    // block for local variables
    {
     xml = xml + this._itemId.serialize(cxfjsutils, 'itemId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_UnrecognizedTraitCriterionException.prototype.serialize = DSS_UnrecognizedTraitCriterionException_serialize;

function DSS_UnrecognizedTraitCriterionException_deserialize (cxfjsutils, element) {
    var newobject = new DSS_UnrecognizedTraitCriterionException();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing errorMessage');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage'));
     newobject.setErrorMessage(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing itemId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setItemId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}EvaluationRequestBase
//
function DSS_EvaluationRequestBase () {
    this.typeMarker = 'DSS_EvaluationRequestBase';
    this._clientLanguage = '';
    this._clientTimeZoneOffset = '';
}

//
// accessor is DSS_EvaluationRequestBase.prototype.getClientLanguage
// element get for clientLanguage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Language
// - required element
//
// element set for clientLanguage
// setter function is is DSS_EvaluationRequestBase.prototype.setClientLanguage
//
function DSS_EvaluationRequestBase_getClientLanguage() { return this._clientLanguage;}

DSS_EvaluationRequestBase.prototype.getClientLanguage = DSS_EvaluationRequestBase_getClientLanguage;

function DSS_EvaluationRequestBase_setClientLanguage(value) { this._clientLanguage = value;}

DSS_EvaluationRequestBase.prototype.setClientLanguage = DSS_EvaluationRequestBase_setClientLanguage;
//
// accessor is DSS_EvaluationRequestBase.prototype.getClientTimeZoneOffset
// element get for clientTimeZoneOffset
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for clientTimeZoneOffset
// setter function is is DSS_EvaluationRequestBase.prototype.setClientTimeZoneOffset
//
function DSS_EvaluationRequestBase_getClientTimeZoneOffset() { return this._clientTimeZoneOffset;}

DSS_EvaluationRequestBase.prototype.getClientTimeZoneOffset = DSS_EvaluationRequestBase_getClientTimeZoneOffset;

function DSS_EvaluationRequestBase_setClientTimeZoneOffset(value) { this._clientTimeZoneOffset = value;}

DSS_EvaluationRequestBase.prototype.setClientTimeZoneOffset = DSS_EvaluationRequestBase_setClientTimeZoneOffset;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}EvaluationRequestBase
//
function DSS_EvaluationRequestBase_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_EvaluationRequestBase.prototype.serialize = DSS_EvaluationRequestBase_serialize;

function DSS_EvaluationRequestBase_deserialize (cxfjsutils, element) {
    var newobject = new DSS_EvaluationRequestBase();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}ComputableDefinition
//
function DSS_ComputableDefinition () {
    this.typeMarker = 'DSS_ComputableDefinition';
}

//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}ComputableDefinition
//
function DSS_ComputableDefinition_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_ComputableDefinition.prototype.serialize = DSS_ComputableDefinition_serialize;

function DSS_ComputableDefinition_deserialize (cxfjsutils, element) {
    var newobject = new DSS_ComputableDefinition();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
//
function DSS_ItemIdentifier () {
    this.typeMarker = 'DSS_ItemIdentifier';
    this._containingEntityId = null;
    this._itemId = '';
}

//
// accessor is DSS_ItemIdentifier.prototype.getContainingEntityId
// element get for containingEntityId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for containingEntityId
// setter function is is DSS_ItemIdentifier.prototype.setContainingEntityId
//
function DSS_ItemIdentifier_getContainingEntityId() { return this._containingEntityId;}

DSS_ItemIdentifier.prototype.getContainingEntityId = DSS_ItemIdentifier_getContainingEntityId;

function DSS_ItemIdentifier_setContainingEntityId(value) { this._containingEntityId = value;}

DSS_ItemIdentifier.prototype.setContainingEntityId = DSS_ItemIdentifier_setContainingEntityId;
//
// accessor is DSS_ItemIdentifier.prototype.getItemId
// element get for itemId
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for itemId
// setter function is is DSS_ItemIdentifier.prototype.setItemId
//
function DSS_ItemIdentifier_getItemId() { return this._itemId;}

DSS_ItemIdentifier.prototype.getItemId = DSS_ItemIdentifier_getItemId;

function DSS_ItemIdentifier_setItemId(value) { this._itemId = value;}

DSS_ItemIdentifier.prototype.setItemId = DSS_ItemIdentifier_setItemId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
//
function DSS_ItemIdentifier_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' itemId=' + '"' + this.getItemId() + '"';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._containingEntityId.serialize(cxfjsutils, 'containingEntityId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_ItemIdentifier.prototype.serialize = DSS_ItemIdentifier_serialize;

function DSS_ItemIdentifier_deserialize (cxfjsutils, element) {
    var newobject = new DSS_ItemIdentifier();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing containingEntityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setContainingEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Simple type (enumeration) {http://www.omg.org/spec/CDSS/201105/dss}URL
//
//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
//
function DSS_EntityIdentifier () {
    this.typeMarker = 'DSS_EntityIdentifier';
    this._scopingEntityId = '';
    this._businessId = '';
    this._version = '';
}

//
// accessor is DSS_EntityIdentifier.prototype.getScopingEntityId
// element get for scopingEntityId
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for scopingEntityId
// setter function is is DSS_EntityIdentifier.prototype.setScopingEntityId
//
function DSS_EntityIdentifier_getScopingEntityId() { return this._scopingEntityId;}

DSS_EntityIdentifier.prototype.getScopingEntityId = DSS_EntityIdentifier_getScopingEntityId;

function DSS_EntityIdentifier_setScopingEntityId(value) { this._scopingEntityId = value;}

DSS_EntityIdentifier.prototype.setScopingEntityId = DSS_EntityIdentifier_setScopingEntityId;
//
// accessor is DSS_EntityIdentifier.prototype.getBusinessId
// element get for businessId
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for businessId
// setter function is is DSS_EntityIdentifier.prototype.setBusinessId
//
function DSS_EntityIdentifier_getBusinessId() { return this._businessId;}

DSS_EntityIdentifier.prototype.getBusinessId = DSS_EntityIdentifier_getBusinessId;

function DSS_EntityIdentifier_setBusinessId(value) { this._businessId = value;}

DSS_EntityIdentifier.prototype.setBusinessId = DSS_EntityIdentifier_setBusinessId;
//
// accessor is DSS_EntityIdentifier.prototype.getVersion
// element get for version
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for version
// setter function is is DSS_EntityIdentifier.prototype.setVersion
//
function DSS_EntityIdentifier_getVersion() { return this._version;}

DSS_EntityIdentifier.prototype.getVersion = DSS_EntityIdentifier_getVersion;

function DSS_EntityIdentifier_setVersion(value) { this._version = value;}

DSS_EntityIdentifier.prototype.setVersion = DSS_EntityIdentifier_setVersion;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
//
function DSS_EntityIdentifier_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' scopingEntityId=' + '"' + this.getScopingEntityId() + '"' + ' businessId=' + '"' + this.getBusinessId() + '"' + ' version=' + '"' + this.getVersion() + '"';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_EntityIdentifier.prototype.serialize = DSS_EntityIdentifier_serialize;

function DSS_EntityIdentifier_deserialize (cxfjsutils, element) {
    var newobject = new DSS_EntityIdentifier();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMTraitInclusionSpecification
//
function DSS_KMTraitInclusionSpecification () {
    this.typeMarker = 'DSS_KMTraitInclusionSpecification';
    this._idOfTraitToIncludeInSearchResult = [];
}

//
// accessor is DSS_KMTraitInclusionSpecification.prototype.getIdOfTraitToIncludeInSearchResult
// element get for idOfTraitToIncludeInSearchResult
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
// - array
//
// element set for idOfTraitToIncludeInSearchResult
// setter function is is DSS_KMTraitInclusionSpecification.prototype.setIdOfTraitToIncludeInSearchResult
//
function DSS_KMTraitInclusionSpecification_getIdOfTraitToIncludeInSearchResult() { return this._idOfTraitToIncludeInSearchResult;}

DSS_KMTraitInclusionSpecification.prototype.getIdOfTraitToIncludeInSearchResult = DSS_KMTraitInclusionSpecification_getIdOfTraitToIncludeInSearchResult;

function DSS_KMTraitInclusionSpecification_setIdOfTraitToIncludeInSearchResult(value) { this._idOfTraitToIncludeInSearchResult = value;}

DSS_KMTraitInclusionSpecification.prototype.setIdOfTraitToIncludeInSearchResult = DSS_KMTraitInclusionSpecification_setIdOfTraitToIncludeInSearchResult;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMTraitInclusionSpecification
//
function DSS_KMTraitInclusionSpecification_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._idOfTraitToIncludeInSearchResult != null) {
      for (var ax = 0;ax < this._idOfTraitToIncludeInSearchResult.length;ax ++) {
       if (this._idOfTraitToIncludeInSearchResult[ax] == null) {
        xml = xml + '<idOfTraitToIncludeInSearchResult/>';
       } else {
        xml = xml + this._idOfTraitToIncludeInSearchResult[ax].serialize(cxfjsutils, 'idOfTraitToIncludeInSearchResult', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMTraitInclusionSpecification.prototype.serialize = DSS_KMTraitInclusionSpecification_serialize;

function DSS_KMTraitInclusionSpecification_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMTraitInclusionSpecification();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing idOfTraitToIncludeInSearchResult');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'idOfTraitToIncludeInSearchResult')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'idOfTraitToIncludeInSearchResult'));
     newobject.setIdOfTraitToIncludeInSearchResult(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}getKMEvaluationResultSemantics
//
function DSS_getKMEvaluationResultSemantics () {
    this.typeMarker = 'DSS_getKMEvaluationResultSemantics';
    this._interactionId = null;
    this._kmId = null;
}

//
// accessor is DSS_getKMEvaluationResultSemantics.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_getKMEvaluationResultSemantics.prototype.setInteractionId
//
function DSS_getKMEvaluationResultSemantics_getInteractionId() { return this._interactionId;}

DSS_getKMEvaluationResultSemantics.prototype.getInteractionId = DSS_getKMEvaluationResultSemantics_getInteractionId;

function DSS_getKMEvaluationResultSemantics_setInteractionId(value) { this._interactionId = value;}

DSS_getKMEvaluationResultSemantics.prototype.setInteractionId = DSS_getKMEvaluationResultSemantics_setInteractionId;
//
// accessor is DSS_getKMEvaluationResultSemantics.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_getKMEvaluationResultSemantics.prototype.setKmId
//
function DSS_getKMEvaluationResultSemantics_getKmId() { return this._kmId;}

DSS_getKMEvaluationResultSemantics.prototype.getKmId = DSS_getKMEvaluationResultSemantics_getKmId;

function DSS_getKMEvaluationResultSemantics_setKmId(value) { this._kmId = value;}

DSS_getKMEvaluationResultSemantics.prototype.setKmId = DSS_getKMEvaluationResultSemantics_setKmId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}getKMEvaluationResultSemantics
//
function DSS_getKMEvaluationResultSemantics_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_getKMEvaluationResultSemantics.prototype.serialize = DSS_getKMEvaluationResultSemantics_serialize;

function DSS_getKMEvaluationResultSemantics_deserialize (cxfjsutils, element) {
    var newobject = new DSS_getKMEvaluationResultSemantics();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}IntermediateKMEvaluationResponse
//
function DSS_IntermediateKMEvaluationResponse () {
    this.typeMarker = 'DSS_IntermediateKMEvaluationResponse';
    this._kmId = null;
    this._warning = [];
    this._requiredDRGId = [];
}

//
// accessor is DSS_IntermediateKMEvaluationResponse.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_IntermediateKMEvaluationResponse.prototype.setKmId
//
function DSS_IntermediateKMEvaluationResponse_getKmId() { return this._kmId;}

DSS_IntermediateKMEvaluationResponse.prototype.getKmId = DSS_IntermediateKMEvaluationResponse_getKmId;

function DSS_IntermediateKMEvaluationResponse_setKmId(value) { this._kmId = value;}

DSS_IntermediateKMEvaluationResponse.prototype.setKmId = DSS_IntermediateKMEvaluationResponse_setKmId;
//
// accessor is DSS_IntermediateKMEvaluationResponse.prototype.getWarning
// element get for warning
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Warning
// - required element
// - array
//
// element set for warning
// setter function is is DSS_IntermediateKMEvaluationResponse.prototype.setWarning
//
function DSS_IntermediateKMEvaluationResponse_getWarning() { return this._warning;}

DSS_IntermediateKMEvaluationResponse.prototype.getWarning = DSS_IntermediateKMEvaluationResponse_getWarning;

function DSS_IntermediateKMEvaluationResponse_setWarning(value) { this._warning = value;}

DSS_IntermediateKMEvaluationResponse.prototype.setWarning = DSS_IntermediateKMEvaluationResponse_setWarning;
//
// accessor is DSS_IntermediateKMEvaluationResponse.prototype.getRequiredDRGId
// element get for requiredDRGId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
// - array
//
// element set for requiredDRGId
// setter function is is DSS_IntermediateKMEvaluationResponse.prototype.setRequiredDRGId
//
function DSS_IntermediateKMEvaluationResponse_getRequiredDRGId() { return this._requiredDRGId;}

DSS_IntermediateKMEvaluationResponse.prototype.getRequiredDRGId = DSS_IntermediateKMEvaluationResponse_getRequiredDRGId;

function DSS_IntermediateKMEvaluationResponse_setRequiredDRGId(value) { this._requiredDRGId = value;}

DSS_IntermediateKMEvaluationResponse.prototype.setRequiredDRGId = DSS_IntermediateKMEvaluationResponse_setRequiredDRGId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}IntermediateKMEvaluationResponse
//
function DSS_IntermediateKMEvaluationResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    // block for local variables
    {
     if (this._warning != null) {
      for (var ax = 0;ax < this._warning.length;ax ++) {
       if (this._warning[ax] == null) {
        xml = xml + '<warning/>';
       } else {
        xml = xml + this._warning[ax].serialize(cxfjsutils, 'warning', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._requiredDRGId != null) {
      for (var ax = 0;ax < this._requiredDRGId.length;ax ++) {
       if (this._requiredDRGId[ax] == null) {
        xml = xml + '<requiredDRGId/>';
       } else {
        xml = xml + this._requiredDRGId[ax].serialize(cxfjsutils, 'requiredDRGId', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_IntermediateKMEvaluationResponse.prototype.serialize = DSS_IntermediateKMEvaluationResponse_serialize;

function DSS_IntermediateKMEvaluationResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_IntermediateKMEvaluationResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing warning');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'warning')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_Warning_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'warning'));
     newobject.setWarning(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing requiredDRGId');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'requiredDRGId')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'requiredDRGId'));
     newobject.setRequiredDRGId(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}evaluate
//
function DSS_evaluate () {
    this.typeMarker = 'DSS_evaluate';
    this._interactionId = null;
    this._evaluationRequest = null;
}

//
// accessor is DSS_evaluate.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_evaluate.prototype.setInteractionId
//
function DSS_evaluate_getInteractionId() { return this._interactionId;}

DSS_evaluate.prototype.getInteractionId = DSS_evaluate_getInteractionId;

function DSS_evaluate_setInteractionId(value) { this._interactionId = value;}

DSS_evaluate.prototype.setInteractionId = DSS_evaluate_setInteractionId;
//
// accessor is DSS_evaluate.prototype.getEvaluationRequest
// element get for evaluationRequest
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EvaluationRequest
// - required element
//
// element set for evaluationRequest
// setter function is is DSS_evaluate.prototype.setEvaluationRequest
//
function DSS_evaluate_getEvaluationRequest() { return this._evaluationRequest;}

DSS_evaluate.prototype.getEvaluationRequest = DSS_evaluate_getEvaluationRequest;

function DSS_evaluate_setEvaluationRequest(value) { this._evaluationRequest = value;}

DSS_evaluate.prototype.setEvaluationRequest = DSS_evaluate_setEvaluationRequest;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}evaluate
//
function DSS_evaluate_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + this._evaluationRequest.serialize(cxfjsutils, 'evaluationRequest', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_evaluate.prototype.serialize = DSS_evaluate_serialize;

function DSS_evaluate_deserialize (cxfjsutils, element) {
    var newobject = new DSS_evaluate();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing evaluationRequest');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EvaluationRequest_deserialize(cxfjsutils, curElement);
    }
    newobject.setEvaluationRequest(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}listProfilesResponse
//
function DSS_listProfilesResponse () {
    this.typeMarker = 'DSS_listProfilesResponse';
    this._profilesByType = null;
}

//
// accessor is DSS_listProfilesResponse.prototype.getProfilesByType
// element get for profilesByType
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ProfilesByType
// - required element
//
// element set for profilesByType
// setter function is is DSS_listProfilesResponse.prototype.setProfilesByType
//
function DSS_listProfilesResponse_getProfilesByType() { return this._profilesByType;}

DSS_listProfilesResponse.prototype.getProfilesByType = DSS_listProfilesResponse_getProfilesByType;

function DSS_listProfilesResponse_setProfilesByType(value) { this._profilesByType = value;}

DSS_listProfilesResponse.prototype.setProfilesByType = DSS_listProfilesResponse_setProfilesByType;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}listProfilesResponse
//
function DSS_listProfilesResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._profilesByType.serialize(cxfjsutils, 'profilesByType', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_listProfilesResponse.prototype.serialize = DSS_listProfilesResponse_serialize;

function DSS_listProfilesResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_listProfilesResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing profilesByType');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ProfilesByType_deserialize(cxfjsutils, curElement);
    }
    newobject.setProfilesByType(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}DataRequirementItemData
//
function DSS_DataRequirementItemData () {
    this.typeMarker = 'DSS_DataRequirementItemData';
    this._driId = null;
    this._data = null;
}

//
// accessor is DSS_DataRequirementItemData.prototype.getDriId
// element get for driId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
//
// element set for driId
// setter function is is DSS_DataRequirementItemData.prototype.setDriId
//
function DSS_DataRequirementItemData_getDriId() { return this._driId;}

DSS_DataRequirementItemData.prototype.getDriId = DSS_DataRequirementItemData_getDriId;

function DSS_DataRequirementItemData_setDriId(value) { this._driId = value;}

DSS_DataRequirementItemData.prototype.setDriId = DSS_DataRequirementItemData_setDriId;
//
// accessor is DSS_DataRequirementItemData.prototype.getData
// element get for data
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticPayload
// - required element
//
// element set for data
// setter function is is DSS_DataRequirementItemData.prototype.setData
//
function DSS_DataRequirementItemData_getData() { return this._data;}

DSS_DataRequirementItemData.prototype.getData = DSS_DataRequirementItemData_getData;

function DSS_DataRequirementItemData_setData(value) { this._data = value;}

DSS_DataRequirementItemData.prototype.setData = DSS_DataRequirementItemData_setData;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}DataRequirementItemData
//
function DSS_DataRequirementItemData_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._driId.serialize(cxfjsutils, 'driId', null);
    }
    // block for local variables
    {
     xml = xml + this._data.serialize(cxfjsutils, 'data', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_DataRequirementItemData.prototype.serialize = DSS_DataRequirementItemData_serialize;

function DSS_DataRequirementItemData_deserialize (cxfjsutils, element) {
    var newobject = new DSS_DataRequirementItemData();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing driId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setDriId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing data');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_SemanticPayload_deserialize(cxfjsutils, curElement);
    }
    newobject.setData(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}FunctionalProfile
//
function DSS_FunctionalProfile () {
    this.typeMarker = 'DSS_FunctionalProfile';
    this._name = '';
    this._description = '';
    this._entityId = null;
    this._supportedOperation = [];
}

//
// accessor is DSS_FunctionalProfile.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_FunctionalProfile.prototype.setName
//
function DSS_FunctionalProfile_getName() { return this._name;}

DSS_FunctionalProfile.prototype.getName = DSS_FunctionalProfile_getName;

function DSS_FunctionalProfile_setName(value) { this._name = value;}

DSS_FunctionalProfile.prototype.setName = DSS_FunctionalProfile_setName;
//
// accessor is DSS_FunctionalProfile.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_FunctionalProfile.prototype.setDescription
//
function DSS_FunctionalProfile_getDescription() { return this._description;}

DSS_FunctionalProfile.prototype.getDescription = DSS_FunctionalProfile_getDescription;

function DSS_FunctionalProfile_setDescription(value) { this._description = value;}

DSS_FunctionalProfile.prototype.setDescription = DSS_FunctionalProfile_setDescription;
//
// accessor is DSS_FunctionalProfile.prototype.getEntityId
// element get for entityId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for entityId
// setter function is is DSS_FunctionalProfile.prototype.setEntityId
//
function DSS_FunctionalProfile_getEntityId() { return this._entityId;}

DSS_FunctionalProfile.prototype.getEntityId = DSS_FunctionalProfile_getEntityId;

function DSS_FunctionalProfile_setEntityId(value) { this._entityId = value;}

DSS_FunctionalProfile.prototype.setEntityId = DSS_FunctionalProfile_setEntityId;
//
// accessor is DSS_FunctionalProfile.prototype.getSupportedOperation
// element get for supportedOperation
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}OperationType
// - required element
// - array
//
// element set for supportedOperation
// setter function is is DSS_FunctionalProfile.prototype.setSupportedOperation
//
function DSS_FunctionalProfile_getSupportedOperation() { return this._supportedOperation;}

DSS_FunctionalProfile.prototype.getSupportedOperation = DSS_FunctionalProfile_getSupportedOperation;

function DSS_FunctionalProfile_setSupportedOperation(value) { this._supportedOperation = value;}

DSS_FunctionalProfile.prototype.setSupportedOperation = DSS_FunctionalProfile_setSupportedOperation;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}FunctionalProfile
//
function DSS_FunctionalProfile_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._entityId.serialize(cxfjsutils, 'entityId', null);
    }
    // block for local variables
    {
     if (this._supportedOperation != null) {
      for (var ax = 0;ax < this._supportedOperation.length;ax ++) {
       if (this._supportedOperation[ax] == null) {
        xml = xml + '<supportedOperation/>';
       } else {
        xml = xml + '<supportedOperation>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._supportedOperation[ax]);
        xml = xml + '</supportedOperation>';
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_FunctionalProfile.prototype.serialize = DSS_FunctionalProfile_serialize;

function DSS_FunctionalProfile_deserialize (cxfjsutils, element) {
    var newobject = new DSS_FunctionalProfile();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing entityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing supportedOperation');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'supportedOperation')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'supportedOperation'));
     newobject.setSupportedOperation(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMTraitValue
//
function DSS_KMTraitValue () {
    this.typeMarker = 'DSS_KMTraitValue';
    this._traitId = null;
    this._value = null;
}

//
// accessor is DSS_KMTraitValue.prototype.getTraitId
// element get for traitId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for traitId
// setter function is is DSS_KMTraitValue.prototype.setTraitId
//
function DSS_KMTraitValue_getTraitId() { return this._traitId;}

DSS_KMTraitValue.prototype.getTraitId = DSS_KMTraitValue_getTraitId;

function DSS_KMTraitValue_setTraitId(value) { this._traitId = value;}

DSS_KMTraitValue.prototype.setTraitId = DSS_KMTraitValue_setTraitId;
//
// accessor is DSS_KMTraitValue.prototype.getValue
// element get for value
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticPayload
// - required element
//
// element set for value
// setter function is is DSS_KMTraitValue.prototype.setValue
//
function DSS_KMTraitValue_getValue() { return this._value;}

DSS_KMTraitValue.prototype.getValue = DSS_KMTraitValue_getValue;

function DSS_KMTraitValue_setValue(value) { this._value = value;}

DSS_KMTraitValue.prototype.setValue = DSS_KMTraitValue_setValue;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMTraitValue
//
function DSS_KMTraitValue_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._traitId.serialize(cxfjsutils, 'traitId', null);
    }
    // block for local variables
    {
     xml = xml + this._value.serialize(cxfjsutils, 'value', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMTraitValue.prototype.serialize = DSS_KMTraitValue_serialize;

function DSS_KMTraitValue_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMTraitValue();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing traitId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setTraitId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing value');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_SemanticPayload_deserialize(cxfjsutils, curElement);
    }
    newobject.setValue(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}describeSemanticRequirementResponse
//
function DSS_describeSemanticRequirementResponse () {
    this.typeMarker = 'DSS_describeSemanticRequirementResponse';
    this._semanticRequirement = null;
}

//
// accessor is DSS_describeSemanticRequirementResponse.prototype.getSemanticRequirement
// element get for semanticRequirement
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticRequirement
// - required element
//
// element set for semanticRequirement
// setter function is is DSS_describeSemanticRequirementResponse.prototype.setSemanticRequirement
//
function DSS_describeSemanticRequirementResponse_getSemanticRequirement() { return this._semanticRequirement;}

DSS_describeSemanticRequirementResponse.prototype.getSemanticRequirement = DSS_describeSemanticRequirementResponse_getSemanticRequirement;

function DSS_describeSemanticRequirementResponse_setSemanticRequirement(value) { this._semanticRequirement = value;}

DSS_describeSemanticRequirementResponse.prototype.setSemanticRequirement = DSS_describeSemanticRequirementResponse_setSemanticRequirement;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}describeSemanticRequirementResponse
//
function DSS_describeSemanticRequirementResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._semanticRequirement.serialize(cxfjsutils, 'semanticRequirement', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_describeSemanticRequirementResponse.prototype.serialize = DSS_describeSemanticRequirementResponse_serialize;

function DSS_describeSemanticRequirementResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_describeSemanticRequirementResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing semanticRequirement');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_SemanticRequirement_deserialize(cxfjsutils, curElement);
    }
    newobject.setSemanticRequirement(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}describeSemanticSignifier
//
function DSS_describeSemanticSignifier () {
    this.typeMarker = 'DSS_describeSemanticSignifier';
    this._interactionId = null;
    this._semanticSignifierId = null;
}

//
// accessor is DSS_describeSemanticSignifier.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_describeSemanticSignifier.prototype.setInteractionId
//
function DSS_describeSemanticSignifier_getInteractionId() { return this._interactionId;}

DSS_describeSemanticSignifier.prototype.getInteractionId = DSS_describeSemanticSignifier_getInteractionId;

function DSS_describeSemanticSignifier_setInteractionId(value) { this._interactionId = value;}

DSS_describeSemanticSignifier.prototype.setInteractionId = DSS_describeSemanticSignifier_setInteractionId;
//
// accessor is DSS_describeSemanticSignifier.prototype.getSemanticSignifierId
// element get for semanticSignifierId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for semanticSignifierId
// setter function is is DSS_describeSemanticSignifier.prototype.setSemanticSignifierId
//
function DSS_describeSemanticSignifier_getSemanticSignifierId() { return this._semanticSignifierId;}

DSS_describeSemanticSignifier.prototype.getSemanticSignifierId = DSS_describeSemanticSignifier_getSemanticSignifierId;

function DSS_describeSemanticSignifier_setSemanticSignifierId(value) { this._semanticSignifierId = value;}

DSS_describeSemanticSignifier.prototype.setSemanticSignifierId = DSS_describeSemanticSignifier_setSemanticSignifierId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}describeSemanticSignifier
//
function DSS_describeSemanticSignifier_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + this._semanticSignifierId.serialize(cxfjsutils, 'semanticSignifierId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_describeSemanticSignifier.prototype.serialize = DSS_describeSemanticSignifier_serialize;

function DSS_describeSemanticSignifier_deserialize (cxfjsutils, element) {
    var newobject = new DSS_describeSemanticSignifier();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing semanticSignifierId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setSemanticSignifierId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}InvalidDataFormatException
//
function DSS_InvalidDataFormatException () {
    this.typeMarker = 'DSS_InvalidDataFormatException';
    this._errorMessage = [];
    this._informationModelSSId = null;
}

//
// accessor is DSS_InvalidDataFormatException.prototype.getErrorMessage
// element get for errorMessage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ErrorMessage
// - required element
// - array
//
// element set for errorMessage
// setter function is is DSS_InvalidDataFormatException.prototype.setErrorMessage
//
function DSS_InvalidDataFormatException_getErrorMessage() { return this._errorMessage;}

DSS_InvalidDataFormatException.prototype.getErrorMessage = DSS_InvalidDataFormatException_getErrorMessage;

function DSS_InvalidDataFormatException_setErrorMessage(value) { this._errorMessage = value;}

DSS_InvalidDataFormatException.prototype.setErrorMessage = DSS_InvalidDataFormatException_setErrorMessage;
//
// accessor is DSS_InvalidDataFormatException.prototype.getInformationModelSSId
// element get for informationModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for informationModelSSId
// setter function is is DSS_InvalidDataFormatException.prototype.setInformationModelSSId
//
function DSS_InvalidDataFormatException_getInformationModelSSId() { return this._informationModelSSId;}

DSS_InvalidDataFormatException.prototype.getInformationModelSSId = DSS_InvalidDataFormatException_getInformationModelSSId;

function DSS_InvalidDataFormatException_setInformationModelSSId(value) { this._informationModelSSId = value;}

DSS_InvalidDataFormatException.prototype.setInformationModelSSId = DSS_InvalidDataFormatException_setInformationModelSSId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}InvalidDataFormatException
//
function DSS_InvalidDataFormatException_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._errorMessage != null) {
      for (var ax = 0;ax < this._errorMessage.length;ax ++) {
       if (this._errorMessage[ax] == null) {
        xml = xml + '<errorMessage/>';
       } else {
        xml = xml + '<errorMessage>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._errorMessage[ax]);
        xml = xml + '</errorMessage>';
       }
      }
     }
    }
    // block for local variables
    {
     xml = xml + this._informationModelSSId.serialize(cxfjsutils, 'informationModelSSId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_InvalidDataFormatException.prototype.serialize = DSS_InvalidDataFormatException_serialize;

function DSS_InvalidDataFormatException_deserialize (cxfjsutils, element) {
    var newobject = new DSS_InvalidDataFormatException();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing errorMessage');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage'));
     newobject.setErrorMessage(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing informationModelSSId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInformationModelSSId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}Warning
//
function DSS_Warning () {
    this.typeMarker = 'DSS_Warning';
    this._value = null;
}

//
// accessor is DSS_Warning.prototype.getValue
// element get for value
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticPayload
// - required element
//
// element set for value
// setter function is is DSS_Warning.prototype.setValue
//
function DSS_Warning_getValue() { return this._value;}

DSS_Warning.prototype.getValue = DSS_Warning_getValue;

function DSS_Warning_setValue(value) { this._value = value;}

DSS_Warning.prototype.setValue = DSS_Warning_setValue;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}Warning
//
function DSS_Warning_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._value.serialize(cxfjsutils, 'value', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_Warning.prototype.serialize = DSS_Warning_serialize;

function DSS_Warning_deserialize (cxfjsutils, element) {
    var newobject = new DSS_Warning();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing value');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_SemanticPayload_deserialize(cxfjsutils, curElement);
    }
    newobject.setValue(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}RankedKMList
//
function DSS_RankedKMList () {
    this.typeMarker = 'DSS_RankedKMList';
    this._rankedKM = [];
}

//
// accessor is DSS_RankedKMList.prototype.getRankedKM
// element get for rankedKM
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}RankedKM
// - required element
// - array
//
// element set for rankedKM
// setter function is is DSS_RankedKMList.prototype.setRankedKM
//
function DSS_RankedKMList_getRankedKM() { return this._rankedKM;}

DSS_RankedKMList.prototype.getRankedKM = DSS_RankedKMList_getRankedKM;

function DSS_RankedKMList_setRankedKM(value) { this._rankedKM = value;}

DSS_RankedKMList.prototype.setRankedKM = DSS_RankedKMList_setRankedKM;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}RankedKMList
//
function DSS_RankedKMList_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._rankedKM != null) {
      for (var ax = 0;ax < this._rankedKM.length;ax ++) {
       if (this._rankedKM[ax] == null) {
        xml = xml + '<rankedKM/>';
       } else {
        xml = xml + this._rankedKM[ax].serialize(cxfjsutils, 'rankedKM', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_RankedKMList.prototype.serialize = DSS_RankedKMList_serialize;

function DSS_RankedKMList_deserialize (cxfjsutils, element) {
    var newobject = new DSS_RankedKMList();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing rankedKM');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'rankedKM')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_RankedKM_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'rankedKM'));
     newobject.setRankedKM(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}FinalKMEvaluationResponse
//
function DSS_FinalKMEvaluationResponse () {
    this.typeMarker = 'DSS_FinalKMEvaluationResponse';
    this._kmId = null;
    this._warning = [];
    this._kmEvaluationResultData = [];
}

//
// accessor is DSS_FinalKMEvaluationResponse.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_FinalKMEvaluationResponse.prototype.setKmId
//
function DSS_FinalKMEvaluationResponse_getKmId() { return this._kmId;}

DSS_FinalKMEvaluationResponse.prototype.getKmId = DSS_FinalKMEvaluationResponse_getKmId;

function DSS_FinalKMEvaluationResponse_setKmId(value) { this._kmId = value;}

DSS_FinalKMEvaluationResponse.prototype.setKmId = DSS_FinalKMEvaluationResponse_setKmId;
//
// accessor is DSS_FinalKMEvaluationResponse.prototype.getWarning
// element get for warning
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Warning
// - required element
// - array
//
// element set for warning
// setter function is is DSS_FinalKMEvaluationResponse.prototype.setWarning
//
function DSS_FinalKMEvaluationResponse_getWarning() { return this._warning;}

DSS_FinalKMEvaluationResponse.prototype.getWarning = DSS_FinalKMEvaluationResponse_getWarning;

function DSS_FinalKMEvaluationResponse_setWarning(value) { this._warning = value;}

DSS_FinalKMEvaluationResponse.prototype.setWarning = DSS_FinalKMEvaluationResponse_setWarning;
//
// accessor is DSS_FinalKMEvaluationResponse.prototype.getKmEvaluationResultData
// element get for kmEvaluationResultData
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultData
// - required element
// - array
//
// element set for kmEvaluationResultData
// setter function is is DSS_FinalKMEvaluationResponse.prototype.setKmEvaluationResultData
//
function DSS_FinalKMEvaluationResponse_getKmEvaluationResultData() { return this._kmEvaluationResultData;}

DSS_FinalKMEvaluationResponse.prototype.getKmEvaluationResultData = DSS_FinalKMEvaluationResponse_getKmEvaluationResultData;

function DSS_FinalKMEvaluationResponse_setKmEvaluationResultData(value) { this._kmEvaluationResultData = value;}

DSS_FinalKMEvaluationResponse.prototype.setKmEvaluationResultData = DSS_FinalKMEvaluationResponse_setKmEvaluationResultData;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}FinalKMEvaluationResponse
//
function DSS_FinalKMEvaluationResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    // block for local variables
    {
     if (this._warning != null) {
      for (var ax = 0;ax < this._warning.length;ax ++) {
       if (this._warning[ax] == null) {
        xml = xml + '<warning/>';
       } else {
        xml = xml + this._warning[ax].serialize(cxfjsutils, 'warning', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._kmEvaluationResultData != null) {
      for (var ax = 0;ax < this._kmEvaluationResultData.length;ax ++) {
       if (this._kmEvaluationResultData[ax] == null) {
        xml = xml + '<kmEvaluationResultData/>';
       } else {
        xml = xml + this._kmEvaluationResultData[ax].serialize(cxfjsutils, 'kmEvaluationResultData', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_FinalKMEvaluationResponse.prototype.serialize = DSS_FinalKMEvaluationResponse_serialize;

function DSS_FinalKMEvaluationResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_FinalKMEvaluationResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing warning');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'warning')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_Warning_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'warning'));
     newobject.setWarning(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmEvaluationResultData');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'kmEvaluationResultData')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_KMEvaluationResultData_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'kmEvaluationResultData'));
     newobject.setKmEvaluationResultData(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}DataRequirementCriterion
//
function DSS_DataRequirementCriterion () {
    this.typeMarker = 'DSS_DataRequirementCriterion';
    this._dataInformationModelSSId = null;
    this._queryInformationModelSSId = [];
}

//
// accessor is DSS_DataRequirementCriterion.prototype.getDataInformationModelSSId
// element get for dataInformationModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for dataInformationModelSSId
// setter function is is DSS_DataRequirementCriterion.prototype.setDataInformationModelSSId
//
function DSS_DataRequirementCriterion_getDataInformationModelSSId() { return this._dataInformationModelSSId;}

DSS_DataRequirementCriterion.prototype.getDataInformationModelSSId = DSS_DataRequirementCriterion_getDataInformationModelSSId;

function DSS_DataRequirementCriterion_setDataInformationModelSSId(value) { this._dataInformationModelSSId = value;}

DSS_DataRequirementCriterion.prototype.setDataInformationModelSSId = DSS_DataRequirementCriterion_setDataInformationModelSSId;
//
// accessor is DSS_DataRequirementCriterion.prototype.getQueryInformationModelSSId
// element get for queryInformationModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
// - array
//
// element set for queryInformationModelSSId
// setter function is is DSS_DataRequirementCriterion.prototype.setQueryInformationModelSSId
//
function DSS_DataRequirementCriterion_getQueryInformationModelSSId() { return this._queryInformationModelSSId;}

DSS_DataRequirementCriterion.prototype.getQueryInformationModelSSId = DSS_DataRequirementCriterion_getQueryInformationModelSSId;

function DSS_DataRequirementCriterion_setQueryInformationModelSSId(value) { this._queryInformationModelSSId = value;}

DSS_DataRequirementCriterion.prototype.setQueryInformationModelSSId = DSS_DataRequirementCriterion_setQueryInformationModelSSId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}DataRequirementCriterion
//
function DSS_DataRequirementCriterion_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._dataInformationModelSSId.serialize(cxfjsutils, 'dataInformationModelSSId', null);
    }
    // block for local variables
    {
     if (this._queryInformationModelSSId != null) {
      for (var ax = 0;ax < this._queryInformationModelSSId.length;ax ++) {
       if (this._queryInformationModelSSId[ax] == null) {
        xml = xml + '<queryInformationModelSSId/>';
       } else {
        xml = xml + this._queryInformationModelSSId[ax].serialize(cxfjsutils, 'queryInformationModelSSId', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_DataRequirementCriterion.prototype.serialize = DSS_DataRequirementCriterion_serialize;

function DSS_DataRequirementCriterion_deserialize (cxfjsutils, element) {
    var newobject = new DSS_DataRequirementCriterion();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing dataInformationModelSSId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setDataInformationModelSSId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing queryInformationModelSSId');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'queryInformationModelSSId')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'queryInformationModelSSId'));
     newobject.setQueryInformationModelSSId(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}InvalidTimeZoneOffsetException
//
function DSS_InvalidTimeZoneOffsetException () {
    this.typeMarker = 'DSS_InvalidTimeZoneOffsetException';
    this._errorMessage = [];
    this._invalidTimeZoneOffset = '';
}

//
// accessor is DSS_InvalidTimeZoneOffsetException.prototype.getErrorMessage
// element get for errorMessage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ErrorMessage
// - required element
// - array
//
// element set for errorMessage
// setter function is is DSS_InvalidTimeZoneOffsetException.prototype.setErrorMessage
//
function DSS_InvalidTimeZoneOffsetException_getErrorMessage() { return this._errorMessage;}

DSS_InvalidTimeZoneOffsetException.prototype.getErrorMessage = DSS_InvalidTimeZoneOffsetException_getErrorMessage;

function DSS_InvalidTimeZoneOffsetException_setErrorMessage(value) { this._errorMessage = value;}

DSS_InvalidTimeZoneOffsetException.prototype.setErrorMessage = DSS_InvalidTimeZoneOffsetException_setErrorMessage;
//
// accessor is DSS_InvalidTimeZoneOffsetException.prototype.getInvalidTimeZoneOffset
// element get for invalidTimeZoneOffset
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for invalidTimeZoneOffset
// setter function is is DSS_InvalidTimeZoneOffsetException.prototype.setInvalidTimeZoneOffset
//
function DSS_InvalidTimeZoneOffsetException_getInvalidTimeZoneOffset() { return this._invalidTimeZoneOffset;}

DSS_InvalidTimeZoneOffsetException.prototype.getInvalidTimeZoneOffset = DSS_InvalidTimeZoneOffsetException_getInvalidTimeZoneOffset;

function DSS_InvalidTimeZoneOffsetException_setInvalidTimeZoneOffset(value) { this._invalidTimeZoneOffset = value;}

DSS_InvalidTimeZoneOffsetException.prototype.setInvalidTimeZoneOffset = DSS_InvalidTimeZoneOffsetException_setInvalidTimeZoneOffset;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}InvalidTimeZoneOffsetException
//
function DSS_InvalidTimeZoneOffsetException_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._errorMessage != null) {
      for (var ax = 0;ax < this._errorMessage.length;ax ++) {
       if (this._errorMessage[ax] == null) {
        xml = xml + '<errorMessage/>';
       } else {
        xml = xml + '<errorMessage>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._errorMessage[ax]);
        xml = xml + '</errorMessage>';
       }
      }
     }
    }
    // block for local variables
    {
     xml = xml + '<invalidTimeZoneOffset>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._invalidTimeZoneOffset);
     xml = xml + '</invalidTimeZoneOffset>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_InvalidTimeZoneOffsetException.prototype.serialize = DSS_InvalidTimeZoneOffsetException_serialize;

function DSS_InvalidTimeZoneOffsetException_deserialize (cxfjsutils, element) {
    var newobject = new DSS_InvalidTimeZoneOffsetException();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing errorMessage');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage'));
     newobject.setErrorMessage(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing invalidTimeZoneOffset');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setInvalidTimeZoneOffset(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResponse
//
function DSS_KMEvaluationResponse () {
    this.typeMarker = 'DSS_KMEvaluationResponse';
    this._kmId = null;
    this._warning = [];
}

//
// accessor is DSS_KMEvaluationResponse.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_KMEvaluationResponse.prototype.setKmId
//
function DSS_KMEvaluationResponse_getKmId() { return this._kmId;}

DSS_KMEvaluationResponse.prototype.getKmId = DSS_KMEvaluationResponse_getKmId;

function DSS_KMEvaluationResponse_setKmId(value) { this._kmId = value;}

DSS_KMEvaluationResponse.prototype.setKmId = DSS_KMEvaluationResponse_setKmId;
//
// accessor is DSS_KMEvaluationResponse.prototype.getWarning
// element get for warning
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Warning
// - required element
// - array
//
// element set for warning
// setter function is is DSS_KMEvaluationResponse.prototype.setWarning
//
function DSS_KMEvaluationResponse_getWarning() { return this._warning;}

DSS_KMEvaluationResponse.prototype.getWarning = DSS_KMEvaluationResponse_getWarning;

function DSS_KMEvaluationResponse_setWarning(value) { this._warning = value;}

DSS_KMEvaluationResponse.prototype.setWarning = DSS_KMEvaluationResponse_setWarning;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResponse
//
function DSS_KMEvaluationResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    // block for local variables
    {
     if (this._warning != null) {
      for (var ax = 0;ax < this._warning.length;ax ++) {
       if (this._warning[ax] == null) {
        xml = xml + '<warning/>';
       } else {
        xml = xml + this._warning[ax].serialize(cxfjsutils, 'warning', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMEvaluationResponse.prototype.serialize = DSS_KMEvaluationResponse_serialize;

function DSS_KMEvaluationResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMEvaluationResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing warning');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'warning')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_Warning_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'warning'));
     newobject.setWarning(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}describeSemanticRequirement
//
function DSS_describeSemanticRequirement () {
    this.typeMarker = 'DSS_describeSemanticRequirement';
    this._interactionId = null;
    this._semanticRequirementId = null;
}

//
// accessor is DSS_describeSemanticRequirement.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_describeSemanticRequirement.prototype.setInteractionId
//
function DSS_describeSemanticRequirement_getInteractionId() { return this._interactionId;}

DSS_describeSemanticRequirement.prototype.getInteractionId = DSS_describeSemanticRequirement_getInteractionId;

function DSS_describeSemanticRequirement_setInteractionId(value) { this._interactionId = value;}

DSS_describeSemanticRequirement.prototype.setInteractionId = DSS_describeSemanticRequirement_setInteractionId;
//
// accessor is DSS_describeSemanticRequirement.prototype.getSemanticRequirementId
// element get for semanticRequirementId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for semanticRequirementId
// setter function is is DSS_describeSemanticRequirement.prototype.setSemanticRequirementId
//
function DSS_describeSemanticRequirement_getSemanticRequirementId() { return this._semanticRequirementId;}

DSS_describeSemanticRequirement.prototype.getSemanticRequirementId = DSS_describeSemanticRequirement_getSemanticRequirementId;

function DSS_describeSemanticRequirement_setSemanticRequirementId(value) { this._semanticRequirementId = value;}

DSS_describeSemanticRequirement.prototype.setSemanticRequirementId = DSS_describeSemanticRequirement_setSemanticRequirementId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}describeSemanticRequirement
//
function DSS_describeSemanticRequirement_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + this._semanticRequirementId.serialize(cxfjsutils, 'semanticRequirementId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_describeSemanticRequirement.prototype.serialize = DSS_describeSemanticRequirement_serialize;

function DSS_describeSemanticRequirement_deserialize (cxfjsutils, element) {
    var newobject = new DSS_describeSemanticRequirement();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing semanticRequirementId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setSemanticRequirementId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Simple type (enumeration) {http://www.omg.org/spec/CDSS/201105/dss}OperationType
//
// - EVALUATION.EVALUATE
// - EVALUATION.EVALUATE_AT_SPECIFIED_TIME
// - EVALUATION.EVALUATE_ITERATIVELY
// - EVALUATION.EVALUATE_ITERATIVELY_AT_SPECIFIED_TIME
// - METADATA_DISCOVERY.DESCRIBE_PROFILE
// - METADATA_DISCOVERY.DESCRIBE_SCOPING_ENTITY
// - METADATA_DISCOVERY.DESCRIBE_SCOPING_ENTITY_HIERARCHY
// - METADATA_DISCOVERY.DESCRIBE_SEMANTIC_REQUIREMENT
// - METADATA_DISCOVERY.DESCRIBE_SEMANTIC_SIGNIFIER
// - METADATA_DISCOVERY.DESCRIBE_TRAIT
// - METADATA_DISCOVERY.LIST_PROFILES
// - QUERY.FIND_KMS
// - QUERY.GET_KM_DATA_REQUIREMENTS
// - QUERY.GET_KM_DATA_REQUIREMENTS_FOR_EVALUATION_AT_SPECIFIED_TIME
// - QUERY.GET_KM_DESCRIPTION
// - QUERY.GET_KM_EVALUATION_RESULT_SEMANTICS
// - QUERY.LIST_KMS
//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}evaluateAtSpecifiedTimeResponse
//
function DSS_evaluateAtSpecifiedTimeResponse () {
    this.typeMarker = 'DSS_evaluateAtSpecifiedTimeResponse';
    this._evaluationResponse = null;
}

//
// accessor is DSS_evaluateAtSpecifiedTimeResponse.prototype.getEvaluationResponse
// element get for evaluationResponse
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EvaluationResponse
// - required element
//
// element set for evaluationResponse
// setter function is is DSS_evaluateAtSpecifiedTimeResponse.prototype.setEvaluationResponse
//
function DSS_evaluateAtSpecifiedTimeResponse_getEvaluationResponse() { return this._evaluationResponse;}

DSS_evaluateAtSpecifiedTimeResponse.prototype.getEvaluationResponse = DSS_evaluateAtSpecifiedTimeResponse_getEvaluationResponse;

function DSS_evaluateAtSpecifiedTimeResponse_setEvaluationResponse(value) { this._evaluationResponse = value;}

DSS_evaluateAtSpecifiedTimeResponse.prototype.setEvaluationResponse = DSS_evaluateAtSpecifiedTimeResponse_setEvaluationResponse;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}evaluateAtSpecifiedTimeResponse
//
function DSS_evaluateAtSpecifiedTimeResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._evaluationResponse.serialize(cxfjsutils, 'evaluationResponse', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_evaluateAtSpecifiedTimeResponse.prototype.serialize = DSS_evaluateAtSpecifiedTimeResponse_serialize;

function DSS_evaluateAtSpecifiedTimeResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_evaluateAtSpecifiedTimeResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing evaluationResponse');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EvaluationResponse_deserialize(cxfjsutils, curElement);
    }
    newobject.setEvaluationResponse(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}XSDComputableDefinition
//
function DSS_XSDComputableDefinition () {
    this.typeMarker = 'DSS_XSDComputableDefinition';
    this._xsdRootGlobalElementName = '';
    this._xsdURL = '';
    this._schematronURL = [];
    this._narrativeModelRestrictionGuideURL = null;
}

//
// accessor is DSS_XSDComputableDefinition.prototype.getXsdRootGlobalElementName
// element get for xsdRootGlobalElementName
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for xsdRootGlobalElementName
// setter function is is DSS_XSDComputableDefinition.prototype.setXsdRootGlobalElementName
//
function DSS_XSDComputableDefinition_getXsdRootGlobalElementName() { return this._xsdRootGlobalElementName;}

DSS_XSDComputableDefinition.prototype.getXsdRootGlobalElementName = DSS_XSDComputableDefinition_getXsdRootGlobalElementName;

function DSS_XSDComputableDefinition_setXsdRootGlobalElementName(value) { this._xsdRootGlobalElementName = value;}

DSS_XSDComputableDefinition.prototype.setXsdRootGlobalElementName = DSS_XSDComputableDefinition_setXsdRootGlobalElementName;
//
// accessor is DSS_XSDComputableDefinition.prototype.getXsdURL
// element get for xsdURL
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}URL
// - required element
//
// element set for xsdURL
// setter function is is DSS_XSDComputableDefinition.prototype.setXsdURL
//
function DSS_XSDComputableDefinition_getXsdURL() { return this._xsdURL;}

DSS_XSDComputableDefinition.prototype.getXsdURL = DSS_XSDComputableDefinition_getXsdURL;

function DSS_XSDComputableDefinition_setXsdURL(value) { this._xsdURL = value;}

DSS_XSDComputableDefinition.prototype.setXsdURL = DSS_XSDComputableDefinition_setXsdURL;
//
// accessor is DSS_XSDComputableDefinition.prototype.getSchematronURL
// element get for schematronURL
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}URL
// - required element
// - array
//
// element set for schematronURL
// setter function is is DSS_XSDComputableDefinition.prototype.setSchematronURL
//
function DSS_XSDComputableDefinition_getSchematronURL() { return this._schematronURL;}

DSS_XSDComputableDefinition.prototype.getSchematronURL = DSS_XSDComputableDefinition_getSchematronURL;

function DSS_XSDComputableDefinition_setSchematronURL(value) { this._schematronURL = value;}

DSS_XSDComputableDefinition.prototype.setSchematronURL = DSS_XSDComputableDefinition_setSchematronURL;
//
// accessor is DSS_XSDComputableDefinition.prototype.getNarrativeModelRestrictionGuideURL
// element get for narrativeModelRestrictionGuideURL
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}URL
// - optional element
//
// element set for narrativeModelRestrictionGuideURL
// setter function is is DSS_XSDComputableDefinition.prototype.setNarrativeModelRestrictionGuideURL
//
function DSS_XSDComputableDefinition_getNarrativeModelRestrictionGuideURL() { return this._narrativeModelRestrictionGuideURL;}

DSS_XSDComputableDefinition.prototype.getNarrativeModelRestrictionGuideURL = DSS_XSDComputableDefinition_getNarrativeModelRestrictionGuideURL;

function DSS_XSDComputableDefinition_setNarrativeModelRestrictionGuideURL(value) { this._narrativeModelRestrictionGuideURL = value;}

DSS_XSDComputableDefinition.prototype.setNarrativeModelRestrictionGuideURL = DSS_XSDComputableDefinition_setNarrativeModelRestrictionGuideURL;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}XSDComputableDefinition
//
function DSS_XSDComputableDefinition_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<xsdRootGlobalElementName>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._xsdRootGlobalElementName);
     xml = xml + '</xsdRootGlobalElementName>';
    }
    // block for local variables
    {
     xml = xml + '<xsdURL>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._xsdURL);
     xml = xml + '</xsdURL>';
    }
    // block for local variables
    {
     if (this._schematronURL != null) {
      for (var ax = 0;ax < this._schematronURL.length;ax ++) {
       if (this._schematronURL[ax] == null) {
        xml = xml + '<schematronURL/>';
       } else {
        xml = xml + '<schematronURL>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._schematronURL[ax]);
        xml = xml + '</schematronURL>';
       }
      }
     }
    }
    // block for local variables
    {
     if (this._narrativeModelRestrictionGuideURL != null) {
      xml = xml + '<narrativeModelRestrictionGuideURL>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._narrativeModelRestrictionGuideURL);
      xml = xml + '</narrativeModelRestrictionGuideURL>';
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_XSDComputableDefinition.prototype.serialize = DSS_XSDComputableDefinition_serialize;

function DSS_XSDComputableDefinition_deserialize (cxfjsutils, element) {
    var newobject = new DSS_XSDComputableDefinition();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing xsdRootGlobalElementName');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setXsdRootGlobalElementName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing xsdURL');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setXsdURL(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing schematronURL');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'schematronURL')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'schematronURL'));
     newobject.setSchematronURL(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing narrativeModelRestrictionGuideURL');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'narrativeModelRestrictionGuideURL')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = value;
     }
     newobject.setNarrativeModelRestrictionGuideURL(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMStatusCriterion
//
function DSS_KMStatusCriterion () {
    this.typeMarker = 'DSS_KMStatusCriterion';
    this._kmStatus = [];
}

//
// accessor is DSS_KMStatusCriterion.prototype.getKmStatus
// element get for kmStatus
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMStatus
// - required element
// - array
//
// element set for kmStatus
// setter function is is DSS_KMStatusCriterion.prototype.setKmStatus
//
function DSS_KMStatusCriterion_getKmStatus() { return this._kmStatus;}

DSS_KMStatusCriterion.prototype.getKmStatus = DSS_KMStatusCriterion_getKmStatus;

function DSS_KMStatusCriterion_setKmStatus(value) { this._kmStatus = value;}

DSS_KMStatusCriterion.prototype.setKmStatus = DSS_KMStatusCriterion_setKmStatus;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMStatusCriterion
//
function DSS_KMStatusCriterion_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._kmStatus != null) {
      for (var ax = 0;ax < this._kmStatus.length;ax ++) {
       if (this._kmStatus[ax] == null) {
        xml = xml + '<kmStatus/>';
       } else {
        xml = xml + '<kmStatus>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._kmStatus[ax]);
        xml = xml + '</kmStatus>';
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMStatusCriterion.prototype.serialize = DSS_KMStatusCriterion_serialize;

function DSS_KMStatusCriterion_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMStatusCriterion();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmStatus');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'kmStatus')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'kmStatus'));
     newobject.setKmStatus(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}DataRequirementBase
//
function DSS_DataRequirementBase () {
    this.typeMarker = 'DSS_DataRequirementBase';
    this._informationModelSSId = null;
}

//
// accessor is DSS_DataRequirementBase.prototype.getInformationModelSSId
// element get for informationModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for informationModelSSId
// setter function is is DSS_DataRequirementBase.prototype.setInformationModelSSId
//
function DSS_DataRequirementBase_getInformationModelSSId() { return this._informationModelSSId;}

DSS_DataRequirementBase.prototype.getInformationModelSSId = DSS_DataRequirementBase_getInformationModelSSId;

function DSS_DataRequirementBase_setInformationModelSSId(value) { this._informationModelSSId = value;}

DSS_DataRequirementBase.prototype.setInformationModelSSId = DSS_DataRequirementBase_setInformationModelSSId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}DataRequirementBase
//
function DSS_DataRequirementBase_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._informationModelSSId.serialize(cxfjsutils, 'informationModelSSId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_DataRequirementBase.prototype.serialize = DSS_DataRequirementBase_serialize;

function DSS_DataRequirementBase_deserialize (cxfjsutils, element) {
    var newobject = new DSS_DataRequirementBase();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing informationModelSSId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInformationModelSSId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Simple type (enumeration) {http://www.omg.org/spec/CDSS/201105/dss}Language
//
//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMList
//
function DSS_KMList () {
    this.typeMarker = 'DSS_KMList';
    this._kmDescription = [];
}

//
// accessor is DSS_KMList.prototype.getKmDescription
// element get for kmDescription
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMDescription
// - required element
// - array
//
// element set for kmDescription
// setter function is is DSS_KMList.prototype.setKmDescription
//
function DSS_KMList_getKmDescription() { return this._kmDescription;}

DSS_KMList.prototype.getKmDescription = DSS_KMList_getKmDescription;

function DSS_KMList_setKmDescription(value) { this._kmDescription = value;}

DSS_KMList.prototype.setKmDescription = DSS_KMList_setKmDescription;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMList
//
function DSS_KMList_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._kmDescription != null) {
      for (var ax = 0;ax < this._kmDescription.length;ax ++) {
       if (this._kmDescription[ax] == null) {
        xml = xml + '<kmDescription/>';
       } else {
        xml = xml + this._kmDescription[ax].serialize(cxfjsutils, 'kmDescription', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMList.prototype.serialize = DSS_KMList_serialize;

function DSS_KMList_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMList();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmDescription');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'kmDescription')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_KMDescription_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'kmDescription'));
     newobject.setKmDescription(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse
//
function DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse () {
    this.typeMarker = 'DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse';
    this._kmDataRequirements = null;
}

//
// accessor is DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse.prototype.getKmDataRequirements
// element get for kmDataRequirements
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirements
// - required element
//
// element set for kmDataRequirements
// setter function is is DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse.prototype.setKmDataRequirements
//
function DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse_getKmDataRequirements() { return this._kmDataRequirements;}

DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse.prototype.getKmDataRequirements = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse_getKmDataRequirements;

function DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse_setKmDataRequirements(value) { this._kmDataRequirements = value;}

DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse.prototype.setKmDataRequirements = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse_setKmDataRequirements;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse
//
function DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmDataRequirements.serialize(cxfjsutils, 'kmDataRequirements', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse.prototype.serialize = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse_serialize;

function DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmDataRequirements');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_KMDataRequirements_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmDataRequirements(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationRequest
//
function DSS_KMEvaluationRequest () {
    this.typeMarker = 'DSS_KMEvaluationRequest';
    this._kmId = null;
}

//
// accessor is DSS_KMEvaluationRequest.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_KMEvaluationRequest.prototype.setKmId
//
function DSS_KMEvaluationRequest_getKmId() { return this._kmId;}

DSS_KMEvaluationRequest.prototype.getKmId = DSS_KMEvaluationRequest_getKmId;

function DSS_KMEvaluationRequest_setKmId(value) { this._kmId = value;}

DSS_KMEvaluationRequest.prototype.setKmId = DSS_KMEvaluationRequest_setKmId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationRequest
//
function DSS_KMEvaluationRequest_serialize(cxfjsutils, elementName, extraNamespaces) {
    
	
	var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMEvaluationRequest.prototype.serialize = DSS_KMEvaluationRequest_serialize;

function DSS_KMEvaluationRequest_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMEvaluationRequest();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedLanguageException
//
function DSS_UnrecognizedLanguageException () {
    this.typeMarker = 'DSS_UnrecognizedLanguageException';
    this._errorMessage = [];
    this._unrecognizedLanguage = '';
}

//
// accessor is DSS_UnrecognizedLanguageException.prototype.getErrorMessage
// element get for errorMessage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ErrorMessage
// - required element
// - array
//
// element set for errorMessage
// setter function is is DSS_UnrecognizedLanguageException.prototype.setErrorMessage
//
function DSS_UnrecognizedLanguageException_getErrorMessage() { return this._errorMessage;}

DSS_UnrecognizedLanguageException.prototype.getErrorMessage = DSS_UnrecognizedLanguageException_getErrorMessage;

function DSS_UnrecognizedLanguageException_setErrorMessage(value) { this._errorMessage = value;}

DSS_UnrecognizedLanguageException.prototype.setErrorMessage = DSS_UnrecognizedLanguageException_setErrorMessage;
//
// accessor is DSS_UnrecognizedLanguageException.prototype.getUnrecognizedLanguage
// element get for unrecognizedLanguage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Language
// - required element
//
// element set for unrecognizedLanguage
// setter function is is DSS_UnrecognizedLanguageException.prototype.setUnrecognizedLanguage
//
function DSS_UnrecognizedLanguageException_getUnrecognizedLanguage() { return this._unrecognizedLanguage;}

DSS_UnrecognizedLanguageException.prototype.getUnrecognizedLanguage = DSS_UnrecognizedLanguageException_getUnrecognizedLanguage;

function DSS_UnrecognizedLanguageException_setUnrecognizedLanguage(value) { this._unrecognizedLanguage = value;}

DSS_UnrecognizedLanguageException.prototype.setUnrecognizedLanguage = DSS_UnrecognizedLanguageException_setUnrecognizedLanguage;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedLanguageException
//
function DSS_UnrecognizedLanguageException_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._errorMessage != null) {
      for (var ax = 0;ax < this._errorMessage.length;ax ++) {
       if (this._errorMessage[ax] == null) {
        xml = xml + '<errorMessage/>';
       } else {
        xml = xml + '<errorMessage>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._errorMessage[ax]);
        xml = xml + '</errorMessage>';
       }
      }
     }
    }
    // block for local variables
    {
     xml = xml + '<unrecognizedLanguage>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._unrecognizedLanguage);
     xml = xml + '</unrecognizedLanguage>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_UnrecognizedLanguageException.prototype.serialize = DSS_UnrecognizedLanguageException_serialize;

function DSS_UnrecognizedLanguageException_deserialize (cxfjsutils, element) {
    var newobject = new DSS_UnrecognizedLanguageException();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing errorMessage');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage'));
     newobject.setErrorMessage(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing unrecognizedLanguage');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setUnrecognizedLanguage(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}RelatedKMSearchCriterion
//
function DSS_RelatedKMSearchCriterion () {
    this.typeMarker = 'DSS_RelatedKMSearchCriterion';
    this._relationType = '';
    this._targetKMId = [];
}

//
// accessor is DSS_RelatedKMSearchCriterion.prototype.getRelationType
// element get for relationType
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMRelationshipType
// - required element
//
// element set for relationType
// setter function is is DSS_RelatedKMSearchCriterion.prototype.setRelationType
//
function DSS_RelatedKMSearchCriterion_getRelationType() { return this._relationType;}

DSS_RelatedKMSearchCriterion.prototype.getRelationType = DSS_RelatedKMSearchCriterion_getRelationType;

function DSS_RelatedKMSearchCriterion_setRelationType(value) { this._relationType = value;}

DSS_RelatedKMSearchCriterion.prototype.setRelationType = DSS_RelatedKMSearchCriterion_setRelationType;
//
// accessor is DSS_RelatedKMSearchCriterion.prototype.getTargetKMId
// element get for targetKMId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
// - array
//
// element set for targetKMId
// setter function is is DSS_RelatedKMSearchCriterion.prototype.setTargetKMId
//
function DSS_RelatedKMSearchCriterion_getTargetKMId() { return this._targetKMId;}

DSS_RelatedKMSearchCriterion.prototype.getTargetKMId = DSS_RelatedKMSearchCriterion_getTargetKMId;

function DSS_RelatedKMSearchCriterion_setTargetKMId(value) { this._targetKMId = value;}

DSS_RelatedKMSearchCriterion.prototype.setTargetKMId = DSS_RelatedKMSearchCriterion_setTargetKMId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}RelatedKMSearchCriterion
//
function DSS_RelatedKMSearchCriterion_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<relationType>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._relationType);
     xml = xml + '</relationType>';
    }
    // block for local variables
    {
     if (this._targetKMId != null) {
      for (var ax = 0;ax < this._targetKMId.length;ax ++) {
       if (this._targetKMId[ax] == null) {
        xml = xml + '<targetKMId/>';
       } else {
        xml = xml + this._targetKMId[ax].serialize(cxfjsutils, 'targetKMId', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_RelatedKMSearchCriterion.prototype.serialize = DSS_RelatedKMSearchCriterion_serialize;

function DSS_RelatedKMSearchCriterion_deserialize (cxfjsutils, element) {
    var newobject = new DSS_RelatedKMSearchCriterion();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing relationType');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setRelationType(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing targetKMId');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'targetKMId')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'targetKMId'));
     newobject.setTargetKMId(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMSearchCriteria
//
function DSS_KMSearchCriteria () {
    this.typeMarker = 'DSS_KMSearchCriteria';
    this._inclusionCriterion = [];
    this._exclusionCriterion = [];
    this._maximumKMsToReturn = '';
    this._minimumKMSearchScore = '';
    this._kmTraitInclusionSpecification = null;
}

//
// accessor is DSS_KMSearchCriteria.prototype.getInclusionCriterion
// element get for inclusionCriterion
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMCriterion
// - required element
// - array
//
// element set for inclusionCriterion
// setter function is is DSS_KMSearchCriteria.prototype.setInclusionCriterion
//
function DSS_KMSearchCriteria_getInclusionCriterion() { return this._inclusionCriterion;}

DSS_KMSearchCriteria.prototype.getInclusionCriterion = DSS_KMSearchCriteria_getInclusionCriterion;

function DSS_KMSearchCriteria_setInclusionCriterion(value) { this._inclusionCriterion = value;}

DSS_KMSearchCriteria.prototype.setInclusionCriterion = DSS_KMSearchCriteria_setInclusionCriterion;
//
// accessor is DSS_KMSearchCriteria.prototype.getExclusionCriterion
// element get for exclusionCriterion
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMCriterion
// - required element
// - array
//
// element set for exclusionCriterion
// setter function is is DSS_KMSearchCriteria.prototype.setExclusionCriterion
//
function DSS_KMSearchCriteria_getExclusionCriterion() { return this._exclusionCriterion;}

DSS_KMSearchCriteria.prototype.getExclusionCriterion = DSS_KMSearchCriteria_getExclusionCriterion;

function DSS_KMSearchCriteria_setExclusionCriterion(value) { this._exclusionCriterion = value;}

DSS_KMSearchCriteria.prototype.setExclusionCriterion = DSS_KMSearchCriteria_setExclusionCriterion;
//
// accessor is DSS_KMSearchCriteria.prototype.getMaximumKMsToReturn
// element get for maximumKMsToReturn
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}MaximumKMsToReturn
// - required element
//
// element set for maximumKMsToReturn
// setter function is is DSS_KMSearchCriteria.prototype.setMaximumKMsToReturn
//
function DSS_KMSearchCriteria_getMaximumKMsToReturn() { return this._maximumKMsToReturn;}

DSS_KMSearchCriteria.prototype.getMaximumKMsToReturn = DSS_KMSearchCriteria_getMaximumKMsToReturn;

function DSS_KMSearchCriteria_setMaximumKMsToReturn(value) { this._maximumKMsToReturn = value;}

DSS_KMSearchCriteria.prototype.setMaximumKMsToReturn = DSS_KMSearchCriteria_setMaximumKMsToReturn;
//
// accessor is DSS_KMSearchCriteria.prototype.getMinimumKMSearchScore
// element get for minimumKMSearchScore
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMSearchScore
// - required element
//
// element set for minimumKMSearchScore
// setter function is is DSS_KMSearchCriteria.prototype.setMinimumKMSearchScore
//
function DSS_KMSearchCriteria_getMinimumKMSearchScore() { return this._minimumKMSearchScore;}

DSS_KMSearchCriteria.prototype.getMinimumKMSearchScore = DSS_KMSearchCriteria_getMinimumKMSearchScore;

function DSS_KMSearchCriteria_setMinimumKMSearchScore(value) { this._minimumKMSearchScore = value;}

DSS_KMSearchCriteria.prototype.setMinimumKMSearchScore = DSS_KMSearchCriteria_setMinimumKMSearchScore;
//
// accessor is DSS_KMSearchCriteria.prototype.getKmTraitInclusionSpecification
// element get for kmTraitInclusionSpecification
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMTraitInclusionSpecification
// - required element
//
// element set for kmTraitInclusionSpecification
// setter function is is DSS_KMSearchCriteria.prototype.setKmTraitInclusionSpecification
//
function DSS_KMSearchCriteria_getKmTraitInclusionSpecification() { return this._kmTraitInclusionSpecification;}

DSS_KMSearchCriteria.prototype.getKmTraitInclusionSpecification = DSS_KMSearchCriteria_getKmTraitInclusionSpecification;

function DSS_KMSearchCriteria_setKmTraitInclusionSpecification(value) { this._kmTraitInclusionSpecification = value;}

DSS_KMSearchCriteria.prototype.setKmTraitInclusionSpecification = DSS_KMSearchCriteria_setKmTraitInclusionSpecification;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMSearchCriteria
//
function DSS_KMSearchCriteria_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._inclusionCriterion != null) {
      for (var ax = 0;ax < this._inclusionCriterion.length;ax ++) {
       if (this._inclusionCriterion[ax] == null) {
        xml = xml + '<inclusionCriterion/>';
       } else {
        xml = xml + this._inclusionCriterion[ax].serialize(cxfjsutils, 'inclusionCriterion', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._exclusionCriterion != null) {
      for (var ax = 0;ax < this._exclusionCriterion.length;ax ++) {
       if (this._exclusionCriterion[ax] == null) {
        xml = xml + '<exclusionCriterion/>';
       } else {
        xml = xml + this._exclusionCriterion[ax].serialize(cxfjsutils, 'exclusionCriterion', null);
       }
      }
     }
    }
    // block for local variables
    {
     xml = xml + '<maximumKMsToReturn>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._maximumKMsToReturn);
     xml = xml + '</maximumKMsToReturn>';
    }
    // block for local variables
    {
     xml = xml + '<minimumKMSearchScore>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._minimumKMSearchScore);
     xml = xml + '</minimumKMSearchScore>';
    }
    // block for local variables
    {
     xml = xml + this._kmTraitInclusionSpecification.serialize(cxfjsutils, 'kmTraitInclusionSpecification', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMSearchCriteria.prototype.serialize = DSS_KMSearchCriteria_serialize;

function DSS_KMSearchCriteria_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMSearchCriteria();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing inclusionCriterion');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'inclusionCriterion')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_KMCriterion_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'inclusionCriterion'));
     newobject.setInclusionCriterion(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing exclusionCriterion');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'exclusionCriterion')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_KMCriterion_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'exclusionCriterion'));
     newobject.setExclusionCriterion(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing maximumKMsToReturn');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setMaximumKMsToReturn(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing minimumKMSearchScore');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setMinimumKMSearchScore(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmTraitInclusionSpecification');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_KMTraitInclusionSpecification_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmTraitInclusionSpecification(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}TraitSetRequirement
//
function DSS_TraitSetRequirement () {
    this.typeMarker = 'DSS_TraitSetRequirement';
    this._name = '';
    this._description = '';
    this._entityId = null;
    this._type = '';
    this._traitRequirement = [];
}

//
// accessor is DSS_TraitSetRequirement.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_TraitSetRequirement.prototype.setName
//
function DSS_TraitSetRequirement_getName() { return this._name;}

DSS_TraitSetRequirement.prototype.getName = DSS_TraitSetRequirement_getName;

function DSS_TraitSetRequirement_setName(value) { this._name = value;}

DSS_TraitSetRequirement.prototype.setName = DSS_TraitSetRequirement_setName;
//
// accessor is DSS_TraitSetRequirement.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_TraitSetRequirement.prototype.setDescription
//
function DSS_TraitSetRequirement_getDescription() { return this._description;}

DSS_TraitSetRequirement.prototype.getDescription = DSS_TraitSetRequirement_getDescription;

function DSS_TraitSetRequirement_setDescription(value) { this._description = value;}

DSS_TraitSetRequirement.prototype.setDescription = DSS_TraitSetRequirement_setDescription;
//
// accessor is DSS_TraitSetRequirement.prototype.getEntityId
// element get for entityId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for entityId
// setter function is is DSS_TraitSetRequirement.prototype.setEntityId
//
function DSS_TraitSetRequirement_getEntityId() { return this._entityId;}

DSS_TraitSetRequirement.prototype.getEntityId = DSS_TraitSetRequirement_getEntityId;

function DSS_TraitSetRequirement_setEntityId(value) { this._entityId = value;}

DSS_TraitSetRequirement.prototype.setEntityId = DSS_TraitSetRequirement_setEntityId;
//
// accessor is DSS_TraitSetRequirement.prototype.getType
// element get for type
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticRequirementType
// - required element
//
// element set for type
// setter function is is DSS_TraitSetRequirement.prototype.setType
//
function DSS_TraitSetRequirement_getType() { return this._type;}

DSS_TraitSetRequirement.prototype.getType = DSS_TraitSetRequirement_getType;

function DSS_TraitSetRequirement_setType(value) { this._type = value;}

DSS_TraitSetRequirement.prototype.setType = DSS_TraitSetRequirement_setType;
//
// accessor is DSS_TraitSetRequirement.prototype.getTraitRequirement
// element get for traitRequirement
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}TraitRequirement
// - required element
// - array
//
// element set for traitRequirement
// setter function is is DSS_TraitSetRequirement.prototype.setTraitRequirement
//
function DSS_TraitSetRequirement_getTraitRequirement() { return this._traitRequirement;}

DSS_TraitSetRequirement.prototype.getTraitRequirement = DSS_TraitSetRequirement_getTraitRequirement;

function DSS_TraitSetRequirement_setTraitRequirement(value) { this._traitRequirement = value;}

DSS_TraitSetRequirement.prototype.setTraitRequirement = DSS_TraitSetRequirement_setTraitRequirement;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}TraitSetRequirement
//
function DSS_TraitSetRequirement_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._entityId.serialize(cxfjsutils, 'entityId', null);
    }
    // block for local variables
    {
     xml = xml + '<type>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._type);
     xml = xml + '</type>';
    }
    // block for local variables
    {
     if (this._traitRequirement != null) {
      for (var ax = 0;ax < this._traitRequirement.length;ax ++) {
       if (this._traitRequirement[ax] == null) {
        xml = xml + '<traitRequirement/>';
       } else {
        xml = xml + this._traitRequirement[ax].serialize(cxfjsutils, 'traitRequirement', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_TraitSetRequirement.prototype.serialize = DSS_TraitSetRequirement_serialize;

function DSS_TraitSetRequirement_deserialize (cxfjsutils, element) {
    var newobject = new DSS_TraitSetRequirement();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing entityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing type');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setType(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing traitRequirement');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'traitRequirement')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_TraitRequirement_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'traitRequirement'));
     newobject.setTraitRequirement(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}SemanticProfile
//
function DSS_SemanticProfile () {
    this.typeMarker = 'DSS_SemanticProfile';
    this._name = '';
    this._description = '';
    this._entityId = null;
    this._fulfilledSemanticRequirement = [];
}

//
// accessor is DSS_SemanticProfile.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_SemanticProfile.prototype.setName
//
function DSS_SemanticProfile_getName() { return this._name;}

DSS_SemanticProfile.prototype.getName = DSS_SemanticProfile_getName;

function DSS_SemanticProfile_setName(value) { this._name = value;}

DSS_SemanticProfile.prototype.setName = DSS_SemanticProfile_setName;
//
// accessor is DSS_SemanticProfile.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_SemanticProfile.prototype.setDescription
//
function DSS_SemanticProfile_getDescription() { return this._description;}

DSS_SemanticProfile.prototype.getDescription = DSS_SemanticProfile_getDescription;

function DSS_SemanticProfile_setDescription(value) { this._description = value;}

DSS_SemanticProfile.prototype.setDescription = DSS_SemanticProfile_setDescription;
//
// accessor is DSS_SemanticProfile.prototype.getEntityId
// element get for entityId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for entityId
// setter function is is DSS_SemanticProfile.prototype.setEntityId
//
function DSS_SemanticProfile_getEntityId() { return this._entityId;}

DSS_SemanticProfile.prototype.getEntityId = DSS_SemanticProfile_getEntityId;

function DSS_SemanticProfile_setEntityId(value) { this._entityId = value;}

DSS_SemanticProfile.prototype.setEntityId = DSS_SemanticProfile_setEntityId;
//
// accessor is DSS_SemanticProfile.prototype.getFulfilledSemanticRequirement
// element get for fulfilledSemanticRequirement
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticRequirement
// - required element
// - array
//
// element set for fulfilledSemanticRequirement
// setter function is is DSS_SemanticProfile.prototype.setFulfilledSemanticRequirement
//
function DSS_SemanticProfile_getFulfilledSemanticRequirement() { return this._fulfilledSemanticRequirement;}

DSS_SemanticProfile.prototype.getFulfilledSemanticRequirement = DSS_SemanticProfile_getFulfilledSemanticRequirement;

function DSS_SemanticProfile_setFulfilledSemanticRequirement(value) { this._fulfilledSemanticRequirement = value;}

DSS_SemanticProfile.prototype.setFulfilledSemanticRequirement = DSS_SemanticProfile_setFulfilledSemanticRequirement;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}SemanticProfile
//
function DSS_SemanticProfile_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._entityId.serialize(cxfjsutils, 'entityId', null);
    }
    // block for local variables
    {
     if (this._fulfilledSemanticRequirement != null) {
      for (var ax = 0;ax < this._fulfilledSemanticRequirement.length;ax ++) {
       if (this._fulfilledSemanticRequirement[ax] == null) {
        xml = xml + '<fulfilledSemanticRequirement/>';
       } else {
        xml = xml + this._fulfilledSemanticRequirement[ax].serialize(cxfjsutils, 'fulfilledSemanticRequirement', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_SemanticProfile.prototype.serialize = DSS_SemanticProfile_serialize;

function DSS_SemanticProfile_deserialize (cxfjsutils, element) {
    var newobject = new DSS_SemanticProfile();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing entityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing fulfilledSemanticRequirement');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'fulfilledSemanticRequirement')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_SemanticRequirement_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'fulfilledSemanticRequirement'));
     newobject.setFulfilledSemanticRequirement(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultSemantics
//
function DSS_KMEvaluationResultSemantics () {
    this.typeMarker = 'DSS_KMEvaluationResultSemantics';
    this._name = '';
    this._description = '';
    this._id = null;
    this._informationModelSSId = null;
}

//
// accessor is DSS_KMEvaluationResultSemantics.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_KMEvaluationResultSemantics.prototype.setName
//
function DSS_KMEvaluationResultSemantics_getName() { return this._name;}

DSS_KMEvaluationResultSemantics.prototype.getName = DSS_KMEvaluationResultSemantics_getName;

function DSS_KMEvaluationResultSemantics_setName(value) { this._name = value;}

DSS_KMEvaluationResultSemantics.prototype.setName = DSS_KMEvaluationResultSemantics_setName;
//
// accessor is DSS_KMEvaluationResultSemantics.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_KMEvaluationResultSemantics.prototype.setDescription
//
function DSS_KMEvaluationResultSemantics_getDescription() { return this._description;}

DSS_KMEvaluationResultSemantics.prototype.getDescription = DSS_KMEvaluationResultSemantics_getDescription;

function DSS_KMEvaluationResultSemantics_setDescription(value) { this._description = value;}

DSS_KMEvaluationResultSemantics.prototype.setDescription = DSS_KMEvaluationResultSemantics_setDescription;
//
// accessor is DSS_KMEvaluationResultSemantics.prototype.getId
// element get for id
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
//
// element set for id
// setter function is is DSS_KMEvaluationResultSemantics.prototype.setId
//
function DSS_KMEvaluationResultSemantics_getId() { return this._id;}

DSS_KMEvaluationResultSemantics.prototype.getId = DSS_KMEvaluationResultSemantics_getId;

function DSS_KMEvaluationResultSemantics_setId(value) { this._id = value;}

DSS_KMEvaluationResultSemantics.prototype.setId = DSS_KMEvaluationResultSemantics_setId;
//
// accessor is DSS_KMEvaluationResultSemantics.prototype.getInformationModelSSId
// element get for informationModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for informationModelSSId
// setter function is is DSS_KMEvaluationResultSemantics.prototype.setInformationModelSSId
//
function DSS_KMEvaluationResultSemantics_getInformationModelSSId() { return this._informationModelSSId;}

DSS_KMEvaluationResultSemantics.prototype.getInformationModelSSId = DSS_KMEvaluationResultSemantics_getInformationModelSSId;

function DSS_KMEvaluationResultSemantics_setInformationModelSSId(value) { this._informationModelSSId = value;}

DSS_KMEvaluationResultSemantics.prototype.setInformationModelSSId = DSS_KMEvaluationResultSemantics_setInformationModelSSId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultSemantics
//
function DSS_KMEvaluationResultSemantics_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._id.serialize(cxfjsutils, 'id', null);
    }
    // block for local variables
    {
     xml = xml + this._informationModelSSId.serialize(cxfjsutils, 'informationModelSSId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMEvaluationResultSemantics.prototype.serialize = DSS_KMEvaluationResultSemantics_serialize;

function DSS_KMEvaluationResultSemantics_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMEvaluationResultSemantics();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing id');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing informationModelSSId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInformationModelSSId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
//
function DSS_InteractionIdentifier () {
    this.typeMarker = 'DSS_InteractionIdentifier';
    this._scopingEntityId = '';
    this._interactionId = '';
    this._submissionTime = '';
}

//
// accessor is DSS_InteractionIdentifier.prototype.getScopingEntityId
// element get for scopingEntityId
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for scopingEntityId
// setter function is is DSS_InteractionIdentifier.prototype.setScopingEntityId
//
function DSS_InteractionIdentifier_getScopingEntityId() { return this._scopingEntityId;}

DSS_InteractionIdentifier.prototype.getScopingEntityId = DSS_InteractionIdentifier_getScopingEntityId;

function DSS_InteractionIdentifier_setScopingEntityId(value) { this._scopingEntityId = value;}

DSS_InteractionIdentifier.prototype.setScopingEntityId = DSS_InteractionIdentifier_setScopingEntityId;
//
// accessor is DSS_InteractionIdentifier.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for interactionId
// setter function is is DSS_InteractionIdentifier.prototype.setInteractionId
//
function DSS_InteractionIdentifier_getInteractionId() { return this._interactionId;}

DSS_InteractionIdentifier.prototype.getInteractionId = DSS_InteractionIdentifier_getInteractionId;

function DSS_InteractionIdentifier_setInteractionId(value) { this._interactionId = value;}

DSS_InteractionIdentifier.prototype.setInteractionId = DSS_InteractionIdentifier_setInteractionId;
//
// accessor is DSS_InteractionIdentifier.prototype.getSubmissionTime
// element get for submissionTime
// - element type is {http://www.w3.org/2001/XMLSchema}dateTime
// - required element
//
// element set for submissionTime
// setter function is is DSS_InteractionIdentifier.prototype.setSubmissionTime
//
function DSS_InteractionIdentifier_getSubmissionTime() { return this._submissionTime;}

DSS_InteractionIdentifier.prototype.getSubmissionTime = DSS_InteractionIdentifier_getSubmissionTime;

function DSS_InteractionIdentifier_setSubmissionTime(value) { this._submissionTime = value;}

DSS_InteractionIdentifier.prototype.setSubmissionTime = DSS_InteractionIdentifier_setSubmissionTime;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
//
function DSS_InteractionIdentifier_serialize(cxfjsutils, elementName, extraNamespaces) {

	var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' scopingEntityId=' + '"' + /*intIdObj.getScopingEntityId()*/this.getScopingEntityId() + '"' + " interactionId=" + '"' + /*intIdObj.getInteractionId()*/this.getInteractionId() + '"' + " submissionTime=" + '"' + /*intIdObj.getSubmissionTime()*/this.getSubmissionTime() + '"';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces ;
     }
     xml = xml + '>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_InteractionIdentifier.prototype.serialize = DSS_InteractionIdentifier_serialize;

function DSS_InteractionIdentifier_deserialize (cxfjsutils, element) {
    var newobject = new DSS_InteractionIdentifier();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntity
//
function DSS_describeScopingEntity () {
    this.typeMarker = 'DSS_describeScopingEntity';
    this._interactionId = null;
    this._scopingEntityId = '';
}

//
// accessor is DSS_describeScopingEntity.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_describeScopingEntity.prototype.setInteractionId
//
function DSS_describeScopingEntity_getInteractionId() { return this._interactionId;}

DSS_describeScopingEntity.prototype.getInteractionId = DSS_describeScopingEntity_getInteractionId;

function DSS_describeScopingEntity_setInteractionId(value) { this._interactionId = value;}

DSS_describeScopingEntity.prototype.setInteractionId = DSS_describeScopingEntity_setInteractionId;
//
// accessor is DSS_describeScopingEntity.prototype.getScopingEntityId
// element get for scopingEntityId
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for scopingEntityId
// setter function is is DSS_describeScopingEntity.prototype.setScopingEntityId
//
function DSS_describeScopingEntity_getScopingEntityId() { return this._scopingEntityId;}

DSS_describeScopingEntity.prototype.getScopingEntityId = DSS_describeScopingEntity_getScopingEntityId;

function DSS_describeScopingEntity_setScopingEntityId(value) { this._scopingEntityId = value;}

DSS_describeScopingEntity.prototype.setScopingEntityId = DSS_describeScopingEntity_setScopingEntityId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntity
//
function DSS_describeScopingEntity_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + '<scopingEntityId>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._scopingEntityId);
     xml = xml + '</scopingEntityId>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_describeScopingEntity.prototype.serialize = DSS_describeScopingEntity_serialize;

function DSS_describeScopingEntity_deserialize (cxfjsutils, element) {
    var newobject = new DSS_describeScopingEntity();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing scopingEntityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setScopingEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}getKMDescriptionResponse
//
function DSS_getKMDescriptionResponse () {
    this.typeMarker = 'DSS_getKMDescriptionResponse';
    this._extendedKMDescription = null;
}

//
// accessor is DSS_getKMDescriptionResponse.prototype.getExtendedKMDescription
// element get for extendedKMDescription
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ExtendedKMDescription
// - required element
//
// element set for extendedKMDescription
// setter function is is DSS_getKMDescriptionResponse.prototype.setExtendedKMDescription
//
function DSS_getKMDescriptionResponse_getExtendedKMDescription() { return this._extendedKMDescription;}

DSS_getKMDescriptionResponse.prototype.getExtendedKMDescription = DSS_getKMDescriptionResponse_getExtendedKMDescription;

function DSS_getKMDescriptionResponse_setExtendedKMDescription(value) { this._extendedKMDescription = value;}

DSS_getKMDescriptionResponse.prototype.setExtendedKMDescription = DSS_getKMDescriptionResponse_setExtendedKMDescription;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}getKMDescriptionResponse
//
function DSS_getKMDescriptionResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._extendedKMDescription.serialize(cxfjsutils, 'extendedKMDescription', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_getKMDescriptionResponse.prototype.serialize = DSS_getKMDescriptionResponse_serialize;

function DSS_getKMDescriptionResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_getKMDescriptionResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing extendedKMDescription');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ExtendedKMDescription_deserialize(cxfjsutils, curElement);
    }
    newobject.setExtendedKMDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}DSSException
//
function DSS_DSSException () {
    this.typeMarker = 'DSS_DSSException';
    this._errorMessage = [];
}

//
// accessor is DSS_DSSException.prototype.getErrorMessage
// element get for errorMessage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ErrorMessage
// - required element
// - array
//
// element set for errorMessage
// setter function is is DSS_DSSException.prototype.setErrorMessage
//
function DSS_DSSException_getErrorMessage() { return this._errorMessage;}

DSS_DSSException.prototype.getErrorMessage = DSS_DSSException_getErrorMessage;

function DSS_DSSException_setErrorMessage(value) { this._errorMessage = value;}

DSS_DSSException.prototype.setErrorMessage = DSS_DSSException_setErrorMessage;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}DSSException
//
function DSS_DSSException_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._errorMessage != null) {
      for (var ax = 0;ax < this._errorMessage.length;ax ++) {
       if (this._errorMessage[ax] == null) {
        xml = xml + '<errorMessage/>';
       } else {
        xml = xml + '<errorMessage>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._errorMessage[ax]);
        xml = xml + '</errorMessage>';
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_DSSException.prototype.serialize = DSS_DSSException_serialize;

function DSS_DSSException_deserialize (cxfjsutils, element) {
    var newobject = new DSS_DSSException();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing errorMessage');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage'));
     newobject.setErrorMessage(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}EvaluationException
//
function DSS_EvaluationException () {
    this.typeMarker = 'DSS_EvaluationException';
    this._errorMessage = [];
    this._kmId = null;
}

//
// accessor is DSS_EvaluationException.prototype.getErrorMessage
// element get for errorMessage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ErrorMessage
// - required element
// - array
//
// element set for errorMessage
// setter function is is DSS_EvaluationException.prototype.setErrorMessage
//
function DSS_EvaluationException_getErrorMessage() { return this._errorMessage;}

DSS_EvaluationException.prototype.getErrorMessage = DSS_EvaluationException_getErrorMessage;

function DSS_EvaluationException_setErrorMessage(value) { this._errorMessage = value;}

DSS_EvaluationException.prototype.setErrorMessage = DSS_EvaluationException_setErrorMessage;
//
// accessor is DSS_EvaluationException.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_EvaluationException.prototype.setKmId
//
function DSS_EvaluationException_getKmId() { return this._kmId;}

DSS_EvaluationException.prototype.getKmId = DSS_EvaluationException_getKmId;

function DSS_EvaluationException_setKmId(value) { this._kmId = value;}

DSS_EvaluationException.prototype.setKmId = DSS_EvaluationException_setKmId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}EvaluationException
//
function DSS_EvaluationException_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._errorMessage != null) {
      for (var ax = 0;ax < this._errorMessage.length;ax ++) {
       if (this._errorMessage[ax] == null) {
        xml = xml + '<errorMessage/>';
       } else {
        xml = xml + '<errorMessage>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._errorMessage[ax]);
        xml = xml + '</errorMessage>';
       }
      }
     }
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_EvaluationException.prototype.serialize = DSS_EvaluationException_serialize;

function DSS_EvaluationException_deserialize (cxfjsutils, element) {
    var newobject = new DSS_EvaluationException();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing errorMessage');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage'));
     newobject.setErrorMessage(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMLocalizedTraitValue
//
function DSS_KMLocalizedTraitValue () {
    this.typeMarker = 'DSS_KMLocalizedTraitValue';
    this._traitId = null;
    this._value = null;
    this._language = '';
}

//
// accessor is DSS_KMLocalizedTraitValue.prototype.getTraitId
// element get for traitId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for traitId
// setter function is is DSS_KMLocalizedTraitValue.prototype.setTraitId
//
function DSS_KMLocalizedTraitValue_getTraitId() { return this._traitId;}

DSS_KMLocalizedTraitValue.prototype.getTraitId = DSS_KMLocalizedTraitValue_getTraitId;

function DSS_KMLocalizedTraitValue_setTraitId(value) { this._traitId = value;}

DSS_KMLocalizedTraitValue.prototype.setTraitId = DSS_KMLocalizedTraitValue_setTraitId;
//
// accessor is DSS_KMLocalizedTraitValue.prototype.getValue
// element get for value
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticPayload
// - required element
//
// element set for value
// setter function is is DSS_KMLocalizedTraitValue.prototype.setValue
//
function DSS_KMLocalizedTraitValue_getValue() { return this._value;}

DSS_KMLocalizedTraitValue.prototype.getValue = DSS_KMLocalizedTraitValue_getValue;

function DSS_KMLocalizedTraitValue_setValue(value) { this._value = value;}

DSS_KMLocalizedTraitValue.prototype.setValue = DSS_KMLocalizedTraitValue_setValue;
//
// accessor is DSS_KMLocalizedTraitValue.prototype.getLanguage
// element get for language
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Language
// - required element
//
// element set for language
// setter function is is DSS_KMLocalizedTraitValue.prototype.setLanguage
//
function DSS_KMLocalizedTraitValue_getLanguage() { return this._language;}

DSS_KMLocalizedTraitValue.prototype.getLanguage = DSS_KMLocalizedTraitValue_getLanguage;

function DSS_KMLocalizedTraitValue_setLanguage(value) { this._language = value;}

DSS_KMLocalizedTraitValue.prototype.setLanguage = DSS_KMLocalizedTraitValue_setLanguage;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMLocalizedTraitValue
//
function DSS_KMLocalizedTraitValue_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._traitId.serialize(cxfjsutils, 'traitId', null);
    }
    // block for local variables
    {
     xml = xml + this._value.serialize(cxfjsutils, 'value', null);
    }
    // block for local variables
    {
     xml = xml + '<language>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._language);
     xml = xml + '</language>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMLocalizedTraitValue.prototype.serialize = DSS_KMLocalizedTraitValue_serialize;

function DSS_KMLocalizedTraitValue_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMLocalizedTraitValue();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing traitId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setTraitId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing value');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_SemanticPayload_deserialize(cxfjsutils, curElement);
    }
    newobject.setValue(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing language');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setLanguage(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}InformationModelAlternative
//
function DSS_InformationModelAlternative () {
    this.typeMarker = 'DSS_InformationModelAlternative';
    this._informationModelSSId = null;
    this._query = null;
    this._cpqpInUse = [];
}

//
// accessor is DSS_InformationModelAlternative.prototype.getInformationModelSSId
// element get for informationModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for informationModelSSId
// setter function is is DSS_InformationModelAlternative.prototype.setInformationModelSSId
//
function DSS_InformationModelAlternative_getInformationModelSSId() { return this._informationModelSSId;}

DSS_InformationModelAlternative.prototype.getInformationModelSSId = DSS_InformationModelAlternative_getInformationModelSSId;

function DSS_InformationModelAlternative_setInformationModelSSId(value) { this._informationModelSSId = value;}

DSS_InformationModelAlternative.prototype.setInformationModelSSId = DSS_InformationModelAlternative_setInformationModelSSId;
//
// accessor is DSS_InformationModelAlternative.prototype.getQuery
// element get for query
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticPayload
// - optional element
//
// element set for query
// setter function is is DSS_InformationModelAlternative.prototype.setQuery
//
function DSS_InformationModelAlternative_getQuery() { return this._query;}

DSS_InformationModelAlternative.prototype.getQuery = DSS_InformationModelAlternative_getQuery;

function DSS_InformationModelAlternative_setQuery(value) { this._query = value;}

DSS_InformationModelAlternative.prototype.setQuery = DSS_InformationModelAlternative_setQuery;
//
// accessor is DSS_InformationModelAlternative.prototype.getCpqpInUse
// element get for cpqpInUse
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}CPQPInUse
// - required element
// - array
//
// element set for cpqpInUse
// setter function is is DSS_InformationModelAlternative.prototype.setCpqpInUse
//
function DSS_InformationModelAlternative_getCpqpInUse() { return this._cpqpInUse;}

DSS_InformationModelAlternative.prototype.getCpqpInUse = DSS_InformationModelAlternative_getCpqpInUse;

function DSS_InformationModelAlternative_setCpqpInUse(value) { this._cpqpInUse = value;}

DSS_InformationModelAlternative.prototype.setCpqpInUse = DSS_InformationModelAlternative_setCpqpInUse;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}InformationModelAlternative
//
function DSS_InformationModelAlternative_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._informationModelSSId.serialize(cxfjsutils, 'informationModelSSId', null);
    }
    // block for local variables
    {
     if (this._query != null) {
      xml = xml + this._query.serialize(cxfjsutils, 'query', null);
     }
    }
    // block for local variables
    {
     if (this._cpqpInUse != null) {
      for (var ax = 0;ax < this._cpqpInUse.length;ax ++) {
       if (this._cpqpInUse[ax] == null) {
        xml = xml + '<cpqpInUse/>';
       } else {
        xml = xml + this._cpqpInUse[ax].serialize(cxfjsutils, 'cpqpInUse', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_InformationModelAlternative.prototype.serialize = DSS_InformationModelAlternative_serialize;

function DSS_InformationModelAlternative_deserialize (cxfjsutils, element) {
    var newobject = new DSS_InformationModelAlternative();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing informationModelSSId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInformationModelSSId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing query');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'query')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      item = DSS_SemanticPayload_deserialize(cxfjsutils, curElement);
     }
     newobject.setQuery(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing cpqpInUse');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'cpqpInUse')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_CPQPInUse_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'cpqpInUse'));
     newobject.setCpqpInUse(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityResponse
//
function DSS_describeScopingEntityResponse () {
    this.typeMarker = 'DSS_describeScopingEntityResponse';
    this._scopingEntity = null;
}

//
// accessor is DSS_describeScopingEntityResponse.prototype.getScopingEntity
// element get for scopingEntity
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ScopingEntity
// - required element
//
// element set for scopingEntity
// setter function is is DSS_describeScopingEntityResponse.prototype.setScopingEntity
//
function DSS_describeScopingEntityResponse_getScopingEntity() { return this._scopingEntity;}

DSS_describeScopingEntityResponse.prototype.getScopingEntity = DSS_describeScopingEntityResponse_getScopingEntity;

function DSS_describeScopingEntityResponse_setScopingEntity(value) { this._scopingEntity = value;}

DSS_describeScopingEntityResponse.prototype.setScopingEntity = DSS_describeScopingEntityResponse_setScopingEntity;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityResponse
//
function DSS_describeScopingEntityResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._scopingEntity.serialize(cxfjsutils, 'scopingEntity', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_describeScopingEntityResponse.prototype.serialize = DSS_describeScopingEntityResponse_serialize;

function DSS_describeScopingEntityResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_describeScopingEntityResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing scopingEntity');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ScopingEntity_deserialize(cxfjsutils, curElement);
    }
    newobject.setScopingEntity(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirements
//
function DSS_getKMDataRequirements () {
    this.typeMarker = 'DSS_getKMDataRequirements';
    this._interactionId = null;
    this._kmId = null;
}

//
// accessor is DSS_getKMDataRequirements.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_getKMDataRequirements.prototype.setInteractionId
//
function DSS_getKMDataRequirements_getInteractionId() { return this._interactionId;}

DSS_getKMDataRequirements.prototype.getInteractionId = DSS_getKMDataRequirements_getInteractionId;

function DSS_getKMDataRequirements_setInteractionId(value) { this._interactionId = value;}

DSS_getKMDataRequirements.prototype.setInteractionId = DSS_getKMDataRequirements_setInteractionId;
//
// accessor is DSS_getKMDataRequirements.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_getKMDataRequirements.prototype.setKmId
//
function DSS_getKMDataRequirements_getKmId() { return this._kmId;}

DSS_getKMDataRequirements.prototype.getKmId = DSS_getKMDataRequirements_getKmId;

function DSS_getKMDataRequirements_setKmId(value) { this._kmId = value;}

DSS_getKMDataRequirements.prototype.setKmId = DSS_getKMDataRequirements_setKmId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirements
//
function DSS_getKMDataRequirements_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_getKMDataRequirements.prototype.serialize = DSS_getKMDataRequirements_serialize;

function DSS_getKMDataRequirements_deserialize (cxfjsutils, element) {
    var newobject = new DSS_getKMDataRequirements();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}DSSRuntimeException
//
function DSS_DSSRuntimeException () {
    this.typeMarker = 'DSS_DSSRuntimeException';
    this._errorMessage = [];
}

//
// accessor is DSS_DSSRuntimeException.prototype.getErrorMessage
// element get for errorMessage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ErrorMessage
// - required element
// - array
//
// element set for errorMessage
// setter function is is DSS_DSSRuntimeException.prototype.setErrorMessage
//
function DSS_DSSRuntimeException_getErrorMessage() { return this._errorMessage;}

DSS_DSSRuntimeException.prototype.getErrorMessage = DSS_DSSRuntimeException_getErrorMessage;

function DSS_DSSRuntimeException_setErrorMessage(value) { this._errorMessage = value;}

DSS_DSSRuntimeException.prototype.setErrorMessage = DSS_DSSRuntimeException_setErrorMessage;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}DSSRuntimeException
//
function DSS_DSSRuntimeException_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._errorMessage != null) {
      for (var ax = 0;ax < this._errorMessage.length;ax ++) {
       if (this._errorMessage[ax] == null) {
        xml = xml + '<errorMessage/>';
       } else {
        xml = xml + '<errorMessage>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._errorMessage[ax]);
        xml = xml + '</errorMessage>';
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_DSSRuntimeException.prototype.serialize = DSS_DSSRuntimeException_serialize;

function DSS_DSSRuntimeException_deserialize (cxfjsutils, element) {
    var newobject = new DSS_DSSRuntimeException();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing errorMessage');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage'));
     newobject.setErrorMessage(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}findKMs
//
function DSS_findKMs () {
    this.typeMarker = 'DSS_findKMs';
    this._interactionId = null;
    this._clientLanguage = '';
    this._kmSearchCriteria = null;
}

//
// accessor is DSS_findKMs.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_findKMs.prototype.setInteractionId
//
function DSS_findKMs_getInteractionId() { return this._interactionId;}

DSS_findKMs.prototype.getInteractionId = DSS_findKMs_getInteractionId;

function DSS_findKMs_setInteractionId(value) { this._interactionId = value;}

DSS_findKMs.prototype.setInteractionId = DSS_findKMs_setInteractionId;
//
// accessor is DSS_findKMs.prototype.getClientLanguage
// element get for clientLanguage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}Language
// - required element
//
// element set for clientLanguage
// setter function is is DSS_findKMs.prototype.setClientLanguage
//
function DSS_findKMs_getClientLanguage() { return this._clientLanguage;}

DSS_findKMs.prototype.getClientLanguage = DSS_findKMs_getClientLanguage;

function DSS_findKMs_setClientLanguage(value) { this._clientLanguage = value;}

DSS_findKMs.prototype.setClientLanguage = DSS_findKMs_setClientLanguage;
//
// accessor is DSS_findKMs.prototype.getKmSearchCriteria
// element get for kmSearchCriteria
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}KMSearchCriteria
// - required element
//
// element set for kmSearchCriteria
// setter function is is DSS_findKMs.prototype.setKmSearchCriteria
//
function DSS_findKMs_getKmSearchCriteria() { return this._kmSearchCriteria;}

DSS_findKMs.prototype.getKmSearchCriteria = DSS_findKMs_getKmSearchCriteria;

function DSS_findKMs_setKmSearchCriteria(value) { this._kmSearchCriteria = value;}

DSS_findKMs.prototype.setKmSearchCriteria = DSS_findKMs_setKmSearchCriteria;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}findKMs
//
function DSS_findKMs_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + '<clientLanguage>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._clientLanguage);
     xml = xml + '</clientLanguage>';
    }
    // block for local variables
    {
     xml = xml + this._kmSearchCriteria.serialize(cxfjsutils, 'kmSearchCriteria', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_findKMs.prototype.serialize = DSS_findKMs_serialize;

function DSS_findKMs_deserialize (cxfjsutils, element) {
    var newobject = new DSS_findKMs();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing clientLanguage');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setClientLanguage(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmSearchCriteria');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_KMSearchCriteria_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmSearchCriteria(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsForEvaluationAtSpecifiedTime
//
function DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime () {
    this.typeMarker = 'DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime';
    this._interactionId = null;
    this._specifiedTime = '';
    this._kmId = null;
}

//
// accessor is DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime.prototype.setInteractionId
//
function DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_getInteractionId() { return this._interactionId;}

DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime.prototype.getInteractionId = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_getInteractionId;

function DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_setInteractionId(value) { this._interactionId = value;}

DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime.prototype.setInteractionId = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_setInteractionId;
//
// accessor is DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime.prototype.getSpecifiedTime
// element get for specifiedTime
// - element type is {http://www.w3.org/2001/XMLSchema}dateTime
// - required element
//
// element set for specifiedTime
// setter function is is DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime.prototype.setSpecifiedTime
//
function DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_getSpecifiedTime() { return this._specifiedTime;}

DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime.prototype.getSpecifiedTime = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_getSpecifiedTime;

function DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_setSpecifiedTime(value) { this._specifiedTime = value;}

DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime.prototype.setSpecifiedTime = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_setSpecifiedTime;
//
// accessor is DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime.prototype.setKmId
//
function DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_getKmId() { return this._kmId;}

DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime.prototype.getKmId = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_getKmId;

function DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_setKmId(value) { this._kmId = value;}

DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime.prototype.setKmId = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_setKmId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsForEvaluationAtSpecifiedTime
//
function DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + '<specifiedTime>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._specifiedTime);
     xml = xml + '</specifiedTime>';
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime.prototype.serialize = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_serialize;

function DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_deserialize (cxfjsutils, element) {
    var newobject = new DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing specifiedTime');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setSpecifiedTime(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}findKMsResponse
//
function DSS_findKMsResponse () {
    this.typeMarker = 'DSS_findKMsResponse';
    this._rankedKMList = null;
}

//
// accessor is DSS_findKMsResponse.prototype.getRankedKMList
// element get for rankedKMList
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}RankedKMList
// - required element
//
// element set for rankedKMList
// setter function is is DSS_findKMsResponse.prototype.setRankedKMList
//
function DSS_findKMsResponse_getRankedKMList() { return this._rankedKMList;}

DSS_findKMsResponse.prototype.getRankedKMList = DSS_findKMsResponse_getRankedKMList;

function DSS_findKMsResponse_setRankedKMList(value) { this._rankedKMList = value;}

DSS_findKMsResponse.prototype.setRankedKMList = DSS_findKMsResponse_setRankedKMList;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}findKMsResponse
//
function DSS_findKMsResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._rankedKMList.serialize(cxfjsutils, 'rankedKMList', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_findKMsResponse.prototype.serialize = DSS_findKMsResponse_serialize;

function DSS_findKMsResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_findKMsResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing rankedKMList');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_RankedKMList_deserialize(cxfjsutils, curElement);
    }
    newobject.setRankedKMList(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}InvalidDriDataFormatException
//
function DSS_InvalidDriDataFormatException () {
    this.typeMarker = 'DSS_InvalidDriDataFormatException';
    this._errorMessage = [];
    this._informationModelSSId = null;
    this._driId = null;
}

//
// accessor is DSS_InvalidDriDataFormatException.prototype.getErrorMessage
// element get for errorMessage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ErrorMessage
// - required element
// - array
//
// element set for errorMessage
// setter function is is DSS_InvalidDriDataFormatException.prototype.setErrorMessage
//
function DSS_InvalidDriDataFormatException_getErrorMessage() { return this._errorMessage;}

DSS_InvalidDriDataFormatException.prototype.getErrorMessage = DSS_InvalidDriDataFormatException_getErrorMessage;

function DSS_InvalidDriDataFormatException_setErrorMessage(value) { this._errorMessage = value;}

DSS_InvalidDriDataFormatException.prototype.setErrorMessage = DSS_InvalidDriDataFormatException_setErrorMessage;
//
// accessor is DSS_InvalidDriDataFormatException.prototype.getInformationModelSSId
// element get for informationModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for informationModelSSId
// setter function is is DSS_InvalidDriDataFormatException.prototype.setInformationModelSSId
//
function DSS_InvalidDriDataFormatException_getInformationModelSSId() { return this._informationModelSSId;}

DSS_InvalidDriDataFormatException.prototype.getInformationModelSSId = DSS_InvalidDriDataFormatException_getInformationModelSSId;

function DSS_InvalidDriDataFormatException_setInformationModelSSId(value) { this._informationModelSSId = value;}

DSS_InvalidDriDataFormatException.prototype.setInformationModelSSId = DSS_InvalidDriDataFormatException_setInformationModelSSId;
//
// accessor is DSS_InvalidDriDataFormatException.prototype.getDriId
// element get for driId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
//
// element set for driId
// setter function is is DSS_InvalidDriDataFormatException.prototype.setDriId
//
function DSS_InvalidDriDataFormatException_getDriId() { return this._driId;}

DSS_InvalidDriDataFormatException.prototype.getDriId = DSS_InvalidDriDataFormatException_getDriId;

function DSS_InvalidDriDataFormatException_setDriId(value) { this._driId = value;}

DSS_InvalidDriDataFormatException.prototype.setDriId = DSS_InvalidDriDataFormatException_setDriId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}InvalidDriDataFormatException
//
function DSS_InvalidDriDataFormatException_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._errorMessage != null) {
      for (var ax = 0;ax < this._errorMessage.length;ax ++) {
       if (this._errorMessage[ax] == null) {
        xml = xml + '<errorMessage/>';
       } else {
        xml = xml + '<errorMessage>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._errorMessage[ax]);
        xml = xml + '</errorMessage>';
       }
      }
     }
    }
    // block for local variables
    {
     xml = xml + this._informationModelSSId.serialize(cxfjsutils, 'informationModelSSId', null);
    }
    // block for local variables
    {
     xml = xml + this._driId.serialize(cxfjsutils, 'driId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_InvalidDriDataFormatException.prototype.serialize = DSS_InvalidDriDataFormatException_serialize;

function DSS_InvalidDriDataFormatException_deserialize (cxfjsutils, element) {
    var newobject = new DSS_InvalidDriDataFormatException();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing errorMessage');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage'));
     newobject.setErrorMessage(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing informationModelSSId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInformationModelSSId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing driId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setDriId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}RequiredDataNotProvidedException
//
function DSS_RequiredDataNotProvidedException () {
    this.typeMarker = 'DSS_RequiredDataNotProvidedException';
    this._errorMessage = [];
    this._drgId = [];
}

//
// accessor is DSS_RequiredDataNotProvidedException.prototype.getErrorMessage
// element get for errorMessage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ErrorMessage
// - required element
// - array
//
// element set for errorMessage
// setter function is is DSS_RequiredDataNotProvidedException.prototype.setErrorMessage
//
function DSS_RequiredDataNotProvidedException_getErrorMessage() { return this._errorMessage;}

DSS_RequiredDataNotProvidedException.prototype.getErrorMessage = DSS_RequiredDataNotProvidedException_getErrorMessage;

function DSS_RequiredDataNotProvidedException_setErrorMessage(value) { this._errorMessage = value;}

DSS_RequiredDataNotProvidedException.prototype.setErrorMessage = DSS_RequiredDataNotProvidedException_setErrorMessage;
//
// accessor is DSS_RequiredDataNotProvidedException.prototype.getDrgId
// element get for drgId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
// - array
//
// element set for drgId
// setter function is is DSS_RequiredDataNotProvidedException.prototype.setDrgId
//
function DSS_RequiredDataNotProvidedException_getDrgId() { return this._drgId;}

DSS_RequiredDataNotProvidedException.prototype.getDrgId = DSS_RequiredDataNotProvidedException_getDrgId;

function DSS_RequiredDataNotProvidedException_setDrgId(value) { this._drgId = value;}

DSS_RequiredDataNotProvidedException.prototype.setDrgId = DSS_RequiredDataNotProvidedException_setDrgId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}RequiredDataNotProvidedException
//
function DSS_RequiredDataNotProvidedException_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._errorMessage != null) {
      for (var ax = 0;ax < this._errorMessage.length;ax ++) {
       if (this._errorMessage[ax] == null) {
        xml = xml + '<errorMessage/>';
       } else {
        xml = xml + '<errorMessage>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._errorMessage[ax]);
        xml = xml + '</errorMessage>';
       }
      }
     }
    }
    // block for local variables
    {
     if (this._drgId != null) {
      for (var ax = 0;ax < this._drgId.length;ax ++) {
       if (this._drgId[ax] == null) {
        xml = xml + '<drgId/>';
       } else {
        xml = xml + this._drgId[ax].serialize(cxfjsutils, 'drgId', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_RequiredDataNotProvidedException.prototype.serialize = DSS_RequiredDataNotProvidedException_serialize;

function DSS_RequiredDataNotProvidedException_deserialize (cxfjsutils, element) {
    var newobject = new DSS_RequiredDataNotProvidedException();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing errorMessage');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage'));
     newobject.setErrorMessage(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing drgId');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'drgId')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'drgId'));
     newobject.setDrgId(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedScopedEntityException
//
function DSS_UnrecognizedScopedEntityException () {
    this.typeMarker = 'DSS_UnrecognizedScopedEntityException';
    this._errorMessage = [];
    this._entityId = null;
    this._entityType = '';
}

//
// accessor is DSS_UnrecognizedScopedEntityException.prototype.getErrorMessage
// element get for errorMessage
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ErrorMessage
// - required element
// - array
//
// element set for errorMessage
// setter function is is DSS_UnrecognizedScopedEntityException.prototype.setErrorMessage
//
function DSS_UnrecognizedScopedEntityException_getErrorMessage() { return this._errorMessage;}

DSS_UnrecognizedScopedEntityException.prototype.getErrorMessage = DSS_UnrecognizedScopedEntityException_getErrorMessage;

function DSS_UnrecognizedScopedEntityException_setErrorMessage(value) { this._errorMessage = value;}

DSS_UnrecognizedScopedEntityException.prototype.setErrorMessage = DSS_UnrecognizedScopedEntityException_setErrorMessage;
//
// accessor is DSS_UnrecognizedScopedEntityException.prototype.getEntityId
// element get for entityId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for entityId
// setter function is is DSS_UnrecognizedScopedEntityException.prototype.setEntityId
//
function DSS_UnrecognizedScopedEntityException_getEntityId() { return this._entityId;}

DSS_UnrecognizedScopedEntityException.prototype.getEntityId = DSS_UnrecognizedScopedEntityException_getEntityId;

function DSS_UnrecognizedScopedEntityException_setEntityId(value) { this._entityId = value;}

DSS_UnrecognizedScopedEntityException.prototype.setEntityId = DSS_UnrecognizedScopedEntityException_setEntityId;
//
// accessor is DSS_UnrecognizedScopedEntityException.prototype.getEntityType
// element get for entityType
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityType
// - required element
//
// element set for entityType
// setter function is is DSS_UnrecognizedScopedEntityException.prototype.setEntityType
//
function DSS_UnrecognizedScopedEntityException_getEntityType() { return this._entityType;}

DSS_UnrecognizedScopedEntityException.prototype.getEntityType = DSS_UnrecognizedScopedEntityException_getEntityType;

function DSS_UnrecognizedScopedEntityException_setEntityType(value) { this._entityType = value;}

DSS_UnrecognizedScopedEntityException.prototype.setEntityType = DSS_UnrecognizedScopedEntityException_setEntityType;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedScopedEntityException
//
function DSS_UnrecognizedScopedEntityException_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._errorMessage != null) {
      for (var ax = 0;ax < this._errorMessage.length;ax ++) {
       if (this._errorMessage[ax] == null) {
        xml = xml + '<errorMessage/>';
       } else {
        xml = xml + '<errorMessage>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._errorMessage[ax]);
        xml = xml + '</errorMessage>';
       }
      }
     }
    }
    // block for local variables
    {
     xml = xml + this._entityId.serialize(cxfjsutils, 'entityId', null);
    }
    // block for local variables
    {
     xml = xml + '<entityType>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._entityType);
     xml = xml + '</entityType>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_UnrecognizedScopedEntityException.prototype.serialize = DSS_UnrecognizedScopedEntityException_serialize;

function DSS_UnrecognizedScopedEntityException_deserialize (cxfjsutils, element) {
    var newobject = new DSS_UnrecognizedScopedEntityException();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing errorMessage');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'errorMessage'));
     newobject.setErrorMessage(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing entityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing entityType');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setEntityType(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityHierarchyResponse
//
function DSS_describeScopingEntityHierarchyResponse () {
    this.typeMarker = 'DSS_describeScopingEntityHierarchyResponse';
    this._scopingEntity = null;
}

//
// accessor is DSS_describeScopingEntityHierarchyResponse.prototype.getScopingEntity
// element get for scopingEntity
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ScopingEntity
// - required element
//
// element set for scopingEntity
// setter function is is DSS_describeScopingEntityHierarchyResponse.prototype.setScopingEntity
//
function DSS_describeScopingEntityHierarchyResponse_getScopingEntity() { return this._scopingEntity;}

DSS_describeScopingEntityHierarchyResponse.prototype.getScopingEntity = DSS_describeScopingEntityHierarchyResponse_getScopingEntity;

function DSS_describeScopingEntityHierarchyResponse_setScopingEntity(value) { this._scopingEntity = value;}

DSS_describeScopingEntityHierarchyResponse.prototype.setScopingEntity = DSS_describeScopingEntityHierarchyResponse_setScopingEntity;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityHierarchyResponse
//
function DSS_describeScopingEntityHierarchyResponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._scopingEntity.serialize(cxfjsutils, 'scopingEntity', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_describeScopingEntityHierarchyResponse.prototype.serialize = DSS_describeScopingEntityHierarchyResponse_serialize;

function DSS_describeScopingEntityHierarchyResponse_deserialize (cxfjsutils, element) {
    var newobject = new DSS_describeScopingEntityHierarchyResponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing scopingEntity');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ScopingEntity_deserialize(cxfjsutils, curElement);
    }
    newobject.setScopingEntity(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}OtherSemanticRequirement
//
function DSS_OtherSemanticRequirement () {
    this.typeMarker = 'DSS_OtherSemanticRequirement';
    this._name = '';
    this._description = '';
    this._entityId = null;
    this._type = '';
    this._requirementSpecification = '';
}

//
// accessor is DSS_OtherSemanticRequirement.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_OtherSemanticRequirement.prototype.setName
//
function DSS_OtherSemanticRequirement_getName() { return this._name;}

DSS_OtherSemanticRequirement.prototype.getName = DSS_OtherSemanticRequirement_getName;

function DSS_OtherSemanticRequirement_setName(value) { this._name = value;}

DSS_OtherSemanticRequirement.prototype.setName = DSS_OtherSemanticRequirement_setName;
//
// accessor is DSS_OtherSemanticRequirement.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_OtherSemanticRequirement.prototype.setDescription
//
function DSS_OtherSemanticRequirement_getDescription() { return this._description;}

DSS_OtherSemanticRequirement.prototype.getDescription = DSS_OtherSemanticRequirement_getDescription;

function DSS_OtherSemanticRequirement_setDescription(value) { this._description = value;}

DSS_OtherSemanticRequirement.prototype.setDescription = DSS_OtherSemanticRequirement_setDescription;
//
// accessor is DSS_OtherSemanticRequirement.prototype.getEntityId
// element get for entityId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for entityId
// setter function is is DSS_OtherSemanticRequirement.prototype.setEntityId
//
function DSS_OtherSemanticRequirement_getEntityId() { return this._entityId;}

DSS_OtherSemanticRequirement.prototype.getEntityId = DSS_OtherSemanticRequirement_getEntityId;

function DSS_OtherSemanticRequirement_setEntityId(value) { this._entityId = value;}

DSS_OtherSemanticRequirement.prototype.setEntityId = DSS_OtherSemanticRequirement_setEntityId;
//
// accessor is DSS_OtherSemanticRequirement.prototype.getType
// element get for type
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticRequirementType
// - required element
//
// element set for type
// setter function is is DSS_OtherSemanticRequirement.prototype.setType
//
function DSS_OtherSemanticRequirement_getType() { return this._type;}

DSS_OtherSemanticRequirement.prototype.getType = DSS_OtherSemanticRequirement_getType;

function DSS_OtherSemanticRequirement_setType(value) { this._type = value;}

DSS_OtherSemanticRequirement.prototype.setType = DSS_OtherSemanticRequirement_setType;
//
// accessor is DSS_OtherSemanticRequirement.prototype.getRequirementSpecification
// element get for requirementSpecification
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for requirementSpecification
// setter function is is DSS_OtherSemanticRequirement.prototype.setRequirementSpecification
//
function DSS_OtherSemanticRequirement_getRequirementSpecification() { return this._requirementSpecification;}

DSS_OtherSemanticRequirement.prototype.getRequirementSpecification = DSS_OtherSemanticRequirement_getRequirementSpecification;

function DSS_OtherSemanticRequirement_setRequirementSpecification(value) { this._requirementSpecification = value;}

DSS_OtherSemanticRequirement.prototype.setRequirementSpecification = DSS_OtherSemanticRequirement_setRequirementSpecification;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}OtherSemanticRequirement
//
function DSS_OtherSemanticRequirement_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._entityId.serialize(cxfjsutils, 'entityId', null);
    }
    // block for local variables
    {
     xml = xml + '<type>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._type);
     xml = xml + '</type>';
    }
    // block for local variables
    {
     xml = xml + '<requirementSpecification>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._requirementSpecification);
     xml = xml + '</requirementSpecification>';
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_OtherSemanticRequirement.prototype.serialize = DSS_OtherSemanticRequirement_serialize;

function DSS_OtherSemanticRequirement_deserialize (cxfjsutils, element) {
    var newobject = new DSS_OtherSemanticRequirement();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing entityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing type');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setType(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing requirementSpecification');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setRequirementSpecification(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}describeProfile
//
function DSS_describeProfile () {
    this.typeMarker = 'DSS_describeProfile';
    this._interactionId = null;
    this._profileId = null;
}

//
// accessor is DSS_describeProfile.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_describeProfile.prototype.setInteractionId
//
function DSS_describeProfile_getInteractionId() { return this._interactionId;}

DSS_describeProfile.prototype.getInteractionId = DSS_describeProfile_getInteractionId;

function DSS_describeProfile_setInteractionId(value) { this._interactionId = value;}

DSS_describeProfile.prototype.setInteractionId = DSS_describeProfile_setInteractionId;
//
// accessor is DSS_describeProfile.prototype.getProfileId
// element get for profileId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for profileId
// setter function is is DSS_describeProfile.prototype.setProfileId
//
function DSS_describeProfile_getProfileId() { return this._profileId;}

DSS_describeProfile.prototype.getProfileId = DSS_describeProfile_getProfileId;

function DSS_describeProfile_setProfileId(value) { this._profileId = value;}

DSS_describeProfile.prototype.setProfileId = DSS_describeProfile_setProfileId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}describeProfile
//
function DSS_describeProfile_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + this._profileId.serialize(cxfjsutils, 'profileId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_describeProfile.prototype.serialize = DSS_describeProfile_serialize;

function DSS_describeProfile_deserialize (cxfjsutils, element) {
    var newobject = new DSS_describeProfile();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing profileId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setProfileId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}evaluateAtSpecifiedTime
//
function DSS_evaluateAtSpecifiedTime () {
    this.typeMarker = 'DSS_evaluateAtSpecifiedTime';
    this._interactionId = null;
    this._specifiedTime = '';
    this._evaluationRequest = null;
}

//
// accessor is DSS_evaluateAtSpecifiedTime.prototype.getInteractionId
// element get for interactionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier
// - required element
//
// element set for interactionId
// setter function is is DSS_evaluateAtSpecifiedTime.prototype.setInteractionId
//
function DSS_evaluateAtSpecifiedTime_getInteractionId() { return this._interactionId;}

DSS_evaluateAtSpecifiedTime.prototype.getInteractionId = DSS_evaluateAtSpecifiedTime_getInteractionId;

function DSS_evaluateAtSpecifiedTime_setInteractionId(value) { this._interactionId = value;}

DSS_evaluateAtSpecifiedTime.prototype.setInteractionId = DSS_evaluateAtSpecifiedTime_setInteractionId;
//
// accessor is DSS_evaluateAtSpecifiedTime.prototype.getSpecifiedTime
// element get for specifiedTime
// - element type is {http://www.w3.org/2001/XMLSchema}dateTime
// - required element
//
// element set for specifiedTime
// setter function is is DSS_evaluateAtSpecifiedTime.prototype.setSpecifiedTime
//
function DSS_evaluateAtSpecifiedTime_getSpecifiedTime() { return this._specifiedTime;}

DSS_evaluateAtSpecifiedTime.prototype.getSpecifiedTime = DSS_evaluateAtSpecifiedTime_getSpecifiedTime;

function DSS_evaluateAtSpecifiedTime_setSpecifiedTime(value) { this._specifiedTime = value;}

DSS_evaluateAtSpecifiedTime.prototype.setSpecifiedTime = DSS_evaluateAtSpecifiedTime_setSpecifiedTime;
//
// accessor is DSS_evaluateAtSpecifiedTime.prototype.getEvaluationRequest
// element get for evaluationRequest
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EvaluationRequest
// - required element
//
// element set for evaluationRequest
// setter function is is DSS_evaluateAtSpecifiedTime.prototype.setEvaluationRequest
//
function DSS_evaluateAtSpecifiedTime_getEvaluationRequest() { return this._evaluationRequest;}

DSS_evaluateAtSpecifiedTime.prototype.getEvaluationRequest = DSS_evaluateAtSpecifiedTime_getEvaluationRequest;

function DSS_evaluateAtSpecifiedTime_setEvaluationRequest(value) { this._evaluationRequest = value;}

DSS_evaluateAtSpecifiedTime.prototype.setEvaluationRequest = DSS_evaluateAtSpecifiedTime_setEvaluationRequest;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}evaluateAtSpecifiedTime
//
function DSS_evaluateAtSpecifiedTime_serialize(cxfjsutils, elementName, extraNamespaces) {
    
	var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._interactionId.serialize(cxfjsutils, 'interactionId', null);
    }
    // block for local variables
    {
     xml = xml + '<specifiedTime>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._specifiedTime);
     xml = xml + '</specifiedTime>';
    }
    // block for local variables
    {
     xml = xml + this._evaluationRequest.serialize(cxfjsutils, 'evaluationRequest', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_evaluateAtSpecifiedTime.prototype.serialize = DSS_evaluateAtSpecifiedTime_serialize;

function DSS_evaluateAtSpecifiedTime_deserialize (cxfjsutils, element) {
    var newobject = new DSS_evaluateAtSpecifiedTime();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing interactionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_InteractionIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInteractionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing specifiedTime');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setSpecifiedTime(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing evaluationRequest');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EvaluationRequest_deserialize(cxfjsutils, curElement);
    }
    newobject.setEvaluationRequest(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMConsumerProvidedQueryParameter
//
function DSS_KMConsumerProvidedQueryParameter () {
    this.typeMarker = 'DSS_KMConsumerProvidedQueryParameter';
    this._name = '';
    this._description = '';
    this._id = null;
    this._informationModelSSId = null;
}

//
// accessor is DSS_KMConsumerProvidedQueryParameter.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_KMConsumerProvidedQueryParameter.prototype.setName
//
function DSS_KMConsumerProvidedQueryParameter_getName() { return this._name;}

DSS_KMConsumerProvidedQueryParameter.prototype.getName = DSS_KMConsumerProvidedQueryParameter_getName;

function DSS_KMConsumerProvidedQueryParameter_setName(value) { this._name = value;}

DSS_KMConsumerProvidedQueryParameter.prototype.setName = DSS_KMConsumerProvidedQueryParameter_setName;
//
// accessor is DSS_KMConsumerProvidedQueryParameter.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_KMConsumerProvidedQueryParameter.prototype.setDescription
//
function DSS_KMConsumerProvidedQueryParameter_getDescription() { return this._description;}

DSS_KMConsumerProvidedQueryParameter.prototype.getDescription = DSS_KMConsumerProvidedQueryParameter_getDescription;

function DSS_KMConsumerProvidedQueryParameter_setDescription(value) { this._description = value;}

DSS_KMConsumerProvidedQueryParameter.prototype.setDescription = DSS_KMConsumerProvidedQueryParameter_setDescription;
//
// accessor is DSS_KMConsumerProvidedQueryParameter.prototype.getId
// element get for id
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
//
// element set for id
// setter function is is DSS_KMConsumerProvidedQueryParameter.prototype.setId
//
function DSS_KMConsumerProvidedQueryParameter_getId() { return this._id;}

DSS_KMConsumerProvidedQueryParameter.prototype.getId = DSS_KMConsumerProvidedQueryParameter_getId;

function DSS_KMConsumerProvidedQueryParameter_setId(value) { this._id = value;}

DSS_KMConsumerProvidedQueryParameter.prototype.setId = DSS_KMConsumerProvidedQueryParameter_setId;
//
// accessor is DSS_KMConsumerProvidedQueryParameter.prototype.getInformationModelSSId
// element get for informationModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for informationModelSSId
// setter function is is DSS_KMConsumerProvidedQueryParameter.prototype.setInformationModelSSId
//
function DSS_KMConsumerProvidedQueryParameter_getInformationModelSSId() { return this._informationModelSSId;}

DSS_KMConsumerProvidedQueryParameter.prototype.getInformationModelSSId = DSS_KMConsumerProvidedQueryParameter_getInformationModelSSId;

function DSS_KMConsumerProvidedQueryParameter_setInformationModelSSId(value) { this._informationModelSSId = value;}

DSS_KMConsumerProvidedQueryParameter.prototype.setInformationModelSSId = DSS_KMConsumerProvidedQueryParameter_setInformationModelSSId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMConsumerProvidedQueryParameter
//
function DSS_KMConsumerProvidedQueryParameter_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._id.serialize(cxfjsutils, 'id', null);
    }
    // block for local variables
    {
     xml = xml + this._informationModelSSId.serialize(cxfjsutils, 'informationModelSSId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMConsumerProvidedQueryParameter.prototype.serialize = DSS_KMConsumerProvidedQueryParameter_serialize;

function DSS_KMConsumerProvidedQueryParameter_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMConsumerProvidedQueryParameter();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing id');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing informationModelSSId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setInformationModelSSId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}ScopedDO
//
function DSS_ScopedDO () {
    this.typeMarker = 'DSS_ScopedDO';
    this._name = '';
    this._description = '';
    this._entityId = null;
}

//
// accessor is DSS_ScopedDO.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_ScopedDO.prototype.setName
//
function DSS_ScopedDO_getName() { return this._name;}

DSS_ScopedDO.prototype.getName = DSS_ScopedDO_getName;

function DSS_ScopedDO_setName(value) { this._name = value;}

DSS_ScopedDO.prototype.setName = DSS_ScopedDO_setName;
//
// accessor is DSS_ScopedDO.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_ScopedDO.prototype.setDescription
//
function DSS_ScopedDO_getDescription() { return this._description;}

DSS_ScopedDO.prototype.getDescription = DSS_ScopedDO_getDescription;

function DSS_ScopedDO_setDescription(value) { this._description = value;}

DSS_ScopedDO.prototype.setDescription = DSS_ScopedDO_setDescription;
//
// accessor is DSS_ScopedDO.prototype.getEntityId
// element get for entityId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for entityId
// setter function is is DSS_ScopedDO.prototype.setEntityId
//
function DSS_ScopedDO_getEntityId() { return this._entityId;}

DSS_ScopedDO.prototype.getEntityId = DSS_ScopedDO_getEntityId;

function DSS_ScopedDO_setEntityId(value) { this._entityId = value;}

DSS_ScopedDO.prototype.setEntityId = DSS_ScopedDO_setEntityId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}ScopedDO
//
function DSS_ScopedDO_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._entityId.serialize(cxfjsutils, 'entityId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_ScopedDO.prototype.serialize = DSS_ScopedDO_serialize;

function DSS_ScopedDO_deserialize (cxfjsutils, element) {
    var newobject = new DSS_ScopedDO();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing entityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}InformationModelRequirement
//
function DSS_InformationModelRequirement () {
    this.typeMarker = 'DSS_InformationModelRequirement';
    this._name = '';
    this._description = '';
    this._entityId = null;
    this._type = '';
    this._allowedWarningModelSSId = [];
    this._allowedEvaluationResultModelSSId = [];
    this._allowedDataRequirement = [];
    this._requiredDataRequirement = [];
    this._requiredEvaluationResultModelSSId = [];
}

//
// accessor is DSS_InformationModelRequirement.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_InformationModelRequirement.prototype.setName
//
function DSS_InformationModelRequirement_getName() { return this._name;}

DSS_InformationModelRequirement.prototype.getName = DSS_InformationModelRequirement_getName;

function DSS_InformationModelRequirement_setName(value) { this._name = value;}

DSS_InformationModelRequirement.prototype.setName = DSS_InformationModelRequirement_setName;
//
// accessor is DSS_InformationModelRequirement.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_InformationModelRequirement.prototype.setDescription
//
function DSS_InformationModelRequirement_getDescription() { return this._description;}

DSS_InformationModelRequirement.prototype.getDescription = DSS_InformationModelRequirement_getDescription;

function DSS_InformationModelRequirement_setDescription(value) { this._description = value;}

DSS_InformationModelRequirement.prototype.setDescription = DSS_InformationModelRequirement_setDescription;
//
// accessor is DSS_InformationModelRequirement.prototype.getEntityId
// element get for entityId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for entityId
// setter function is is DSS_InformationModelRequirement.prototype.setEntityId
//
function DSS_InformationModelRequirement_getEntityId() { return this._entityId;}

DSS_InformationModelRequirement.prototype.getEntityId = DSS_InformationModelRequirement_getEntityId;

function DSS_InformationModelRequirement_setEntityId(value) { this._entityId = value;}

DSS_InformationModelRequirement.prototype.setEntityId = DSS_InformationModelRequirement_setEntityId;
//
// accessor is DSS_InformationModelRequirement.prototype.getType
// element get for type
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticRequirementType
// - required element
//
// element set for type
// setter function is is DSS_InformationModelRequirement.prototype.setType
//
function DSS_InformationModelRequirement_getType() { return this._type;}

DSS_InformationModelRequirement.prototype.getType = DSS_InformationModelRequirement_getType;

function DSS_InformationModelRequirement_setType(value) { this._type = value;}

DSS_InformationModelRequirement.prototype.setType = DSS_InformationModelRequirement_setType;
//
// accessor is DSS_InformationModelRequirement.prototype.getAllowedWarningModelSSId
// element get for allowedWarningModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
// - array
//
// element set for allowedWarningModelSSId
// setter function is is DSS_InformationModelRequirement.prototype.setAllowedWarningModelSSId
//
function DSS_InformationModelRequirement_getAllowedWarningModelSSId() { return this._allowedWarningModelSSId;}

DSS_InformationModelRequirement.prototype.getAllowedWarningModelSSId = DSS_InformationModelRequirement_getAllowedWarningModelSSId;

function DSS_InformationModelRequirement_setAllowedWarningModelSSId(value) { this._allowedWarningModelSSId = value;}

DSS_InformationModelRequirement.prototype.setAllowedWarningModelSSId = DSS_InformationModelRequirement_setAllowedWarningModelSSId;
//
// accessor is DSS_InformationModelRequirement.prototype.getAllowedEvaluationResultModelSSId
// element get for allowedEvaluationResultModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
// - array
//
// element set for allowedEvaluationResultModelSSId
// setter function is is DSS_InformationModelRequirement.prototype.setAllowedEvaluationResultModelSSId
//
function DSS_InformationModelRequirement_getAllowedEvaluationResultModelSSId() { return this._allowedEvaluationResultModelSSId;}

DSS_InformationModelRequirement.prototype.getAllowedEvaluationResultModelSSId = DSS_InformationModelRequirement_getAllowedEvaluationResultModelSSId;

function DSS_InformationModelRequirement_setAllowedEvaluationResultModelSSId(value) { this._allowedEvaluationResultModelSSId = value;}

DSS_InformationModelRequirement.prototype.setAllowedEvaluationResultModelSSId = DSS_InformationModelRequirement_setAllowedEvaluationResultModelSSId;
//
// accessor is DSS_InformationModelRequirement.prototype.getAllowedDataRequirement
// element get for allowedDataRequirement
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}AllowedDataRequirement
// - required element
// - array
//
// element set for allowedDataRequirement
// setter function is is DSS_InformationModelRequirement.prototype.setAllowedDataRequirement
//
function DSS_InformationModelRequirement_getAllowedDataRequirement() { return this._allowedDataRequirement;}

DSS_InformationModelRequirement.prototype.getAllowedDataRequirement = DSS_InformationModelRequirement_getAllowedDataRequirement;

function DSS_InformationModelRequirement_setAllowedDataRequirement(value) { this._allowedDataRequirement = value;}

DSS_InformationModelRequirement.prototype.setAllowedDataRequirement = DSS_InformationModelRequirement_setAllowedDataRequirement;
//
// accessor is DSS_InformationModelRequirement.prototype.getRequiredDataRequirement
// element get for requiredDataRequirement
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}RequiredDataRequirement
// - required element
// - array
//
// element set for requiredDataRequirement
// setter function is is DSS_InformationModelRequirement.prototype.setRequiredDataRequirement
//
function DSS_InformationModelRequirement_getRequiredDataRequirement() { return this._requiredDataRequirement;}

DSS_InformationModelRequirement.prototype.getRequiredDataRequirement = DSS_InformationModelRequirement_getRequiredDataRequirement;

function DSS_InformationModelRequirement_setRequiredDataRequirement(value) { this._requiredDataRequirement = value;}

DSS_InformationModelRequirement.prototype.setRequiredDataRequirement = DSS_InformationModelRequirement_setRequiredDataRequirement;
//
// accessor is DSS_InformationModelRequirement.prototype.getRequiredEvaluationResultModelSSId
// element get for requiredEvaluationResultModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
// - array
//
// element set for requiredEvaluationResultModelSSId
// setter function is is DSS_InformationModelRequirement.prototype.setRequiredEvaluationResultModelSSId
//
function DSS_InformationModelRequirement_getRequiredEvaluationResultModelSSId() { return this._requiredEvaluationResultModelSSId;}

DSS_InformationModelRequirement.prototype.getRequiredEvaluationResultModelSSId = DSS_InformationModelRequirement_getRequiredEvaluationResultModelSSId;

function DSS_InformationModelRequirement_setRequiredEvaluationResultModelSSId(value) { this._requiredEvaluationResultModelSSId = value;}

DSS_InformationModelRequirement.prototype.setRequiredEvaluationResultModelSSId = DSS_InformationModelRequirement_setRequiredEvaluationResultModelSSId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}InformationModelRequirement
//
function DSS_InformationModelRequirement_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._entityId.serialize(cxfjsutils, 'entityId', null);
    }
    // block for local variables
    {
     xml = xml + '<type>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._type);
     xml = xml + '</type>';
    }
    // block for local variables
    {
     if (this._allowedWarningModelSSId != null) {
      for (var ax = 0;ax < this._allowedWarningModelSSId.length;ax ++) {
       if (this._allowedWarningModelSSId[ax] == null) {
        xml = xml + '<allowedWarningModelSSId/>';
       } else {
        xml = xml + this._allowedWarningModelSSId[ax].serialize(cxfjsutils, 'allowedWarningModelSSId', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._allowedEvaluationResultModelSSId != null) {
      for (var ax = 0;ax < this._allowedEvaluationResultModelSSId.length;ax ++) {
       if (this._allowedEvaluationResultModelSSId[ax] == null) {
        xml = xml + '<allowedEvaluationResultModelSSId/>';
       } else {
        xml = xml + this._allowedEvaluationResultModelSSId[ax].serialize(cxfjsutils, 'allowedEvaluationResultModelSSId', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._allowedDataRequirement != null) {
      for (var ax = 0;ax < this._allowedDataRequirement.length;ax ++) {
       if (this._allowedDataRequirement[ax] == null) {
        xml = xml + '<allowedDataRequirement/>';
       } else {
        xml = xml + this._allowedDataRequirement[ax].serialize(cxfjsutils, 'allowedDataRequirement', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._requiredDataRequirement != null) {
      for (var ax = 0;ax < this._requiredDataRequirement.length;ax ++) {
       if (this._requiredDataRequirement[ax] == null) {
        xml = xml + '<requiredDataRequirement/>';
       } else {
        xml = xml + this._requiredDataRequirement[ax].serialize(cxfjsutils, 'requiredDataRequirement', null);
       }
      }
     }
    }
    // block for local variables
    {
     if (this._requiredEvaluationResultModelSSId != null) {
      for (var ax = 0;ax < this._requiredEvaluationResultModelSSId.length;ax ++) {
       if (this._requiredEvaluationResultModelSSId[ax] == null) {
        xml = xml + '<requiredEvaluationResultModelSSId/>';
       } else {
        xml = xml + this._requiredEvaluationResultModelSSId[ax].serialize(cxfjsutils, 'requiredEvaluationResultModelSSId', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_InformationModelRequirement.prototype.serialize = DSS_InformationModelRequirement_serialize;

function DSS_InformationModelRequirement_deserialize (cxfjsutils, element) {
    var newobject = new DSS_InformationModelRequirement();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing entityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing type');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setType(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing allowedWarningModelSSId');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'allowedWarningModelSSId')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'allowedWarningModelSSId'));
     newobject.setAllowedWarningModelSSId(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing allowedEvaluationResultModelSSId');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'allowedEvaluationResultModelSSId')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'allowedEvaluationResultModelSSId'));
     newobject.setAllowedEvaluationResultModelSSId(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing allowedDataRequirement');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'allowedDataRequirement')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_AllowedDataRequirement_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'allowedDataRequirement'));
     newobject.setAllowedDataRequirement(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing requiredDataRequirement');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'requiredDataRequirement')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_RequiredDataRequirement_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'requiredDataRequirement'));
     newobject.setRequiredDataRequirement(item);
     var item = null;
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing requiredEvaluationResultModelSSId');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'requiredEvaluationResultModelSSId')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'requiredEvaluationResultModelSSId'));
     newobject.setRequiredEvaluationResultModelSSId(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultData
//
function DSS_KMEvaluationResultData () {
    this.typeMarker = 'DSS_KMEvaluationResultData';
    this._evaluationResultId = null;
    this._data = null;
}

//
// accessor is DSS_KMEvaluationResultData.prototype.getEvaluationResultId
// element get for evaluationResultId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
//
// element set for evaluationResultId
// setter function is is DSS_KMEvaluationResultData.prototype.setEvaluationResultId
//
function DSS_KMEvaluationResultData_getEvaluationResultId() { return this._evaluationResultId;}

DSS_KMEvaluationResultData.prototype.getEvaluationResultId = DSS_KMEvaluationResultData_getEvaluationResultId;

function DSS_KMEvaluationResultData_setEvaluationResultId(value) { this._evaluationResultId = value;}

DSS_KMEvaluationResultData.prototype.setEvaluationResultId = DSS_KMEvaluationResultData_setEvaluationResultId;
//
// accessor is DSS_KMEvaluationResultData.prototype.getData
// element get for data
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}SemanticPayload
// - required element
//
// element set for data
// setter function is is DSS_KMEvaluationResultData.prototype.setData
//
function DSS_KMEvaluationResultData_getData() { return this._data;}

DSS_KMEvaluationResultData.prototype.getData = DSS_KMEvaluationResultData_getData;

function DSS_KMEvaluationResultData_setData(value) { this._data = value;}

DSS_KMEvaluationResultData.prototype.setData = DSS_KMEvaluationResultData_setData;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultData
//
function DSS_KMEvaluationResultData_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._evaluationResultId.serialize(cxfjsutils, 'evaluationResultId', null);
    }
    // block for local variables
    {
     xml = xml + this._data.serialize(cxfjsutils, 'data', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_KMEvaluationResultData.prototype.serialize = DSS_KMEvaluationResultData_serialize;

function DSS_KMEvaluationResultData_deserialize (cxfjsutils, element) {
    var newobject = new DSS_KMEvaluationResultData();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing evaluationResultId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setEvaluationResultId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing data');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_SemanticPayload_deserialize(cxfjsutils, curElement);
    }
    newobject.setData(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}ProfilesOfType
//
function DSS_ProfilesOfType () {
    this.typeMarker = 'DSS_ProfilesOfType';
    this._type = '';
    this._profileId = [];
}

//
// accessor is DSS_ProfilesOfType.prototype.getType
// element get for type
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ProfileType
// - required element
//
// element set for type
// setter function is is DSS_ProfilesOfType.prototype.setType
//
function DSS_ProfilesOfType_getType() { return this._type;}

DSS_ProfilesOfType.prototype.getType = DSS_ProfilesOfType_getType;

function DSS_ProfilesOfType_setType(value) { this._type = value;}

DSS_ProfilesOfType.prototype.setType = DSS_ProfilesOfType_setType;
//
// accessor is DSS_ProfilesOfType.prototype.getProfileId
// element get for profileId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
// - array
//
// element set for profileId
// setter function is is DSS_ProfilesOfType.prototype.setProfileId
//
function DSS_ProfilesOfType_getProfileId() { return this._profileId;}

DSS_ProfilesOfType.prototype.getProfileId = DSS_ProfilesOfType_getProfileId;

function DSS_ProfilesOfType_setProfileId(value) { this._profileId = value;}

DSS_ProfilesOfType.prototype.setProfileId = DSS_ProfilesOfType_setProfileId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}ProfilesOfType
//
function DSS_ProfilesOfType_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<type>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._type);
     xml = xml + '</type>';
    }
    // block for local variables
    {
     if (this._profileId != null) {
      for (var ax = 0;ax < this._profileId.length;ax ++) {
       if (this._profileId[ax] == null) {
        xml = xml + '<profileId/>';
       } else {
        xml = xml + this._profileId[ax].serialize(cxfjsutils, 'profileId', null);
       }
      }
     }
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_ProfilesOfType.prototype.serialize = DSS_ProfilesOfType_serialize;

function DSS_ProfilesOfType_deserialize (cxfjsutils, element) {
    var newobject = new DSS_ProfilesOfType();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing type');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setType(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing profileId');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'profileId')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, '', 'profileId'));
     newobject.setProfileId(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}TraitCriterion
//
function DSS_TraitCriterion () {
    this.typeMarker = 'DSS_TraitCriterion';
    this._name = '';
    this._description = '';
    this._traitCriterionId = null;
    this._criterionModelSSId = null;
}

//
// accessor is DSS_TraitCriterion.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_TraitCriterion.prototype.setName
//
function DSS_TraitCriterion_getName() { return this._name;}

DSS_TraitCriterion.prototype.getName = DSS_TraitCriterion_getName;

function DSS_TraitCriterion_setName(value) { this._name = value;}

DSS_TraitCriterion.prototype.setName = DSS_TraitCriterion_setName;
//
// accessor is DSS_TraitCriterion.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_TraitCriterion.prototype.setDescription
//
function DSS_TraitCriterion_getDescription() { return this._description;}

DSS_TraitCriterion.prototype.getDescription = DSS_TraitCriterion_getDescription;

function DSS_TraitCriterion_setDescription(value) { this._description = value;}

DSS_TraitCriterion.prototype.setDescription = DSS_TraitCriterion_setDescription;
//
// accessor is DSS_TraitCriterion.prototype.getTraitCriterionId
// element get for traitCriterionId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier
// - required element
//
// element set for traitCriterionId
// setter function is is DSS_TraitCriterion.prototype.setTraitCriterionId
//
function DSS_TraitCriterion_getTraitCriterionId() { return this._traitCriterionId;}

DSS_TraitCriterion.prototype.getTraitCriterionId = DSS_TraitCriterion_getTraitCriterionId;

function DSS_TraitCriterion_setTraitCriterionId(value) { this._traitCriterionId = value;}

DSS_TraitCriterion.prototype.setTraitCriterionId = DSS_TraitCriterion_setTraitCriterionId;
//
// accessor is DSS_TraitCriterion.prototype.getCriterionModelSSId
// element get for criterionModelSSId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for criterionModelSSId
// setter function is is DSS_TraitCriterion.prototype.setCriterionModelSSId
//
function DSS_TraitCriterion_getCriterionModelSSId() { return this._criterionModelSSId;}

DSS_TraitCriterion.prototype.getCriterionModelSSId = DSS_TraitCriterion_getCriterionModelSSId;

function DSS_TraitCriterion_setCriterionModelSSId(value) { this._criterionModelSSId = value;}

DSS_TraitCriterion.prototype.setCriterionModelSSId = DSS_TraitCriterion_setCriterionModelSSId;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}TraitCriterion
//
function DSS_TraitCriterion_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._traitCriterionId.serialize(cxfjsutils, 'traitCriterionId', null);
    }
    // block for local variables
    {
     xml = xml + this._criterionModelSSId.serialize(cxfjsutils, 'criterionModelSSId', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_TraitCriterion.prototype.serialize = DSS_TraitCriterion_serialize;

function DSS_TraitCriterion_deserialize (cxfjsutils, element) {
    var newobject = new DSS_TraitCriterion();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing traitCriterionId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_ItemIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setTraitCriterionId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing criterionModelSSId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setCriterionModelSSId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}SemanticSignifier
//
function DSS_SemanticSignifier () {
    this.typeMarker = 'DSS_SemanticSignifier';
    this._name = '';
    this._description = '';
    this._entityId = null;
    this._xsdComputableDefinition = null;
}

//
// accessor is DSS_SemanticSignifier.prototype.getName
// element get for name
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for name
// setter function is is DSS_SemanticSignifier.prototype.setName
//
function DSS_SemanticSignifier_getName() { return this._name;}

DSS_SemanticSignifier.prototype.getName = DSS_SemanticSignifier_getName;

function DSS_SemanticSignifier_setName(value) { this._name = value;}

DSS_SemanticSignifier.prototype.setName = DSS_SemanticSignifier_setName;
//
// accessor is DSS_SemanticSignifier.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for description
// setter function is is DSS_SemanticSignifier.prototype.setDescription
//
function DSS_SemanticSignifier_getDescription() { return this._description;}

DSS_SemanticSignifier.prototype.getDescription = DSS_SemanticSignifier_getDescription;

function DSS_SemanticSignifier_setDescription(value) { this._description = value;}

DSS_SemanticSignifier.prototype.setDescription = DSS_SemanticSignifier_setDescription;
//
// accessor is DSS_SemanticSignifier.prototype.getEntityId
// element get for entityId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for entityId
// setter function is is DSS_SemanticSignifier.prototype.setEntityId
//
function DSS_SemanticSignifier_getEntityId() { return this._entityId;}

DSS_SemanticSignifier.prototype.getEntityId = DSS_SemanticSignifier_getEntityId;

function DSS_SemanticSignifier_setEntityId(value) { this._entityId = value;}

DSS_SemanticSignifier.prototype.setEntityId = DSS_SemanticSignifier_setEntityId;
//
// accessor is DSS_SemanticSignifier.prototype.getXsdComputableDefinition
// element get for xsdComputableDefinition
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}XSDComputableDefinition
// - required element
//
// element set for xsdComputableDefinition
// setter function is is DSS_SemanticSignifier.prototype.setXsdComputableDefinition
//
function DSS_SemanticSignifier_getXsdComputableDefinition() { return this._xsdComputableDefinition;}

DSS_SemanticSignifier.prototype.getXsdComputableDefinition = DSS_SemanticSignifier_getXsdComputableDefinition;

function DSS_SemanticSignifier_setXsdComputableDefinition(value) { this._xsdComputableDefinition = value;}

DSS_SemanticSignifier.prototype.setXsdComputableDefinition = DSS_SemanticSignifier_setXsdComputableDefinition;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}SemanticSignifier
//
function DSS_SemanticSignifier_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<name>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._name);
     xml = xml + '</name>';
    }
    // block for local variables
    {
     xml = xml + '<description>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._description);
     xml = xml + '</description>';
    }
    // block for local variables
    {
     xml = xml + this._entityId.serialize(cxfjsutils, 'entityId', null);
    }
    // block for local variables
    {
     xml = xml + this._xsdComputableDefinition.serialize(cxfjsutils, 'xsdComputableDefinition', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_SemanticSignifier.prototype.serialize = DSS_SemanticSignifier_serialize;

function DSS_SemanticSignifier_deserialize (cxfjsutils, element) {
    var newobject = new DSS_SemanticSignifier();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing name');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setName(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setDescription(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing entityId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setEntityId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing xsdComputableDefinition');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_XSDComputableDefinition_deserialize(cxfjsutils, curElement);
    }
    newobject.setXsdComputableDefinition(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.omg.org/spec/CDSS/201105/dss}IterativeKMEvaluationRequest
//
function DSS_IterativeKMEvaluationRequest () {
    this.typeMarker = 'DSS_IterativeKMEvaluationRequest';
    this._kmId = null;
    this._previousState = null;
}

//
// accessor is DSS_IterativeKMEvaluationRequest.prototype.getKmId
// element get for kmId
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier
// - required element
//
// element set for kmId
// setter function is is DSS_IterativeKMEvaluationRequest.prototype.setKmId
//
function DSS_IterativeKMEvaluationRequest_getKmId() { return this._kmId;}

DSS_IterativeKMEvaluationRequest.prototype.getKmId = DSS_IterativeKMEvaluationRequest_getKmId;

function DSS_IterativeKMEvaluationRequest_setKmId(value) { this._kmId = value;}

DSS_IterativeKMEvaluationRequest.prototype.setKmId = DSS_IterativeKMEvaluationRequest_setKmId;
//
// accessor is DSS_IterativeKMEvaluationRequest.prototype.getPreviousState
// element get for previousState
// - element type is {http://www.omg.org/spec/CDSS/201105/dss}IntermediateState
// - required element
//
// element set for previousState
// setter function is is DSS_IterativeKMEvaluationRequest.prototype.setPreviousState
//
function DSS_IterativeKMEvaluationRequest_getPreviousState() { return this._previousState;}

DSS_IterativeKMEvaluationRequest.prototype.getPreviousState = DSS_IterativeKMEvaluationRequest_getPreviousState;

function DSS_IterativeKMEvaluationRequest_setPreviousState(value) { this._previousState = value;}

DSS_IterativeKMEvaluationRequest.prototype.setPreviousState = DSS_IterativeKMEvaluationRequest_setPreviousState;
//
// Serialize {http://www.omg.org/spec/CDSS/201105/dss}IterativeKMEvaluationRequest
//
function DSS_IterativeKMEvaluationRequest_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName != null) {
     xml = xml + '<';
     xml = xml + elementName;
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._kmId.serialize(cxfjsutils, 'kmId', null);
    }
    // block for local variables
    {
     xml = xml + this._previousState.serialize(cxfjsutils, 'previousState', null);
    }
    if (elementName != null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

DSS_IterativeKMEvaluationRequest.prototype.serialize = DSS_IterativeKMEvaluationRequest_serialize;

function DSS_IterativeKMEvaluationRequest_deserialize (cxfjsutils, element) {
    var newobject = new DSS_IterativeKMEvaluationRequest();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing kmId');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_EntityIdentifier_deserialize(cxfjsutils, curElement);
    }
    newobject.setKmId(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing previousState');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = DSS_IntermediateState_deserialize(cxfjsutils, curElement);
    }
    newobject.setPreviousState(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Definitions for schema: http://www.omg.org/spec/CDSS/201105/dss
//  http://localhost:8081/opencds-decision-support-service/evaluate?wsdl=baseWsdl/dssBaseComponents.wsdl#types1
//
//
// Definitions for service: {http://www.omg.org/spec/CDSS/201105/dssWsdl}DecisionSupportService
//

// Javascript for {http://www.omg.org/spec/CDSS/201105/dssWsdl}Evaluation

function DSSWSDL_Evaluation () {
    this.jsutils = new CxfApacheOrgUtil();
    this.jsutils.interfaceObject = this;
    this.synchronous = false;
    this.url = null;
    this.client = null;
    this.response = null;
    this.globalElementSerializers = [];
    this.globalElementDeserializers = [];
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedScopingEntityException'] = DSS_UnrecognizedScopingEntityException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedScopingEntityException'] = DSS_UnrecognizedScopingEntityException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDescriptionBase'] = DSS_KMDescriptionBase_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDescriptionBase'] = DSS_KMDescriptionBase_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}UnsupportedLanguageException'] = DSS_UnsupportedLanguageException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}UnsupportedLanguageException'] = DSS_UnsupportedLanguageException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticRequirement'] = DSS_SemanticRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticRequirement'] = DSS_SemanticRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationRequestBase'] = DSS_KMEvaluationRequestBase_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationRequestBase'] = DSS_KMEvaluationRequestBase_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsResponse'] = DSS_getKMDataRequirementsResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsResponse'] = DSS_getKMDataRequirementsResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}CPQPInUse'] = DSS_CPQPInUse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}CPQPInUse'] = DSS_CPQPInUse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}listProfiles'] = DSS_listProfiles_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}listProfiles'] = DSS_listProfiles_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ServiceRequestBase'] = DSS_ServiceRequestBase_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ServiceRequestBase'] = DSS_ServiceRequestBase_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}IntermediateState'] = DSS_IntermediateState_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}IntermediateState'] = DSS_IntermediateState_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementGroup'] = DSS_KMDataRequirementGroup_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementGroup'] = DSS_KMDataRequirementGroup_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}listKMsResponse'] = DSS_listKMsResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}listKMsResponse'] = DSS_listKMsResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}RequiredDataRequirement'] = DSS_RequiredDataRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}RequiredDataRequirement'] = DSS_RequiredDataRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitCriterion'] = DSS_KMTraitCriterion_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitCriterion'] = DSS_KMTraitCriterion_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeKMEvaluationResponse'] = DSS_IterativeKMEvaluationResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeKMEvaluationResponse'] = DSS_IterativeKMEvaluationResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationResponse'] = DSS_IterativeEvaluationResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationResponse'] = DSS_IterativeEvaluationResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ProfilesByType'] = DSS_ProfilesByType_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ProfilesByType'] = DSS_ProfilesByType_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticPayload'] = DSS_SemanticPayload_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticPayload'] = DSS_SemanticPayload_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationRequest'] = DSS_EvaluationRequest_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationRequest'] = DSS_EvaluationRequest_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateResponse'] = DSS_evaluateResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateResponse'] = DSS_evaluateResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ConformanceProfile'] = DSS_ConformanceProfile_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ConformanceProfile'] = DSS_ConformanceProfile_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidTraitCriterionDataFormatException'] = DSS_InvalidTraitCriterionDataFormatException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidTraitCriterionDataFormatException'] = DSS_InvalidTraitCriterionDataFormatException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationResponse'] = DSS_EvaluationResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationResponse'] = DSS_EvaluationResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultSemanticsList'] = DSS_KMEvaluationResultSemanticsList_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultSemanticsList'] = DSS_KMEvaluationResultSemanticsList_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyResponse'] = DSS_evaluateIterativelyResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyResponse'] = DSS_evaluateIterativelyResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}TraitRequirement'] = DSS_TraitRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}TraitRequirement'] = DSS_TraitRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyAtSpecifiedTime'] = DSS_evaluateIterativelyAtSpecifiedTime_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyAtSpecifiedTime'] = DSS_evaluateIterativelyAtSpecifiedTime_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityHierarchy'] = DSS_describeScopingEntityHierarchy_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityHierarchy'] = DSS_describeScopingEntityHierarchy_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationResponseBase'] = DSS_EvaluationResponseBase_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationResponseBase'] = DSS_EvaluationResponseBase_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDescription'] = DSS_getKMDescription_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDescription'] = DSS_getKMDescription_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}DescribedDO'] = DSS_DescribedDO_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}DescribedDO'] = DSS_DescribedDO_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementItem'] = DSS_KMDataRequirementItem_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementItem'] = DSS_KMDataRequirementItem_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}listKMs'] = DSS_listKMs_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}listKMs'] = DSS_listKMs_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIteratively'] = DSS_evaluateIteratively_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIteratively'] = DSS_evaluateIteratively_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticSignifierResponse'] = DSS_describeSemanticSignifierResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticSignifierResponse'] = DSS_describeSemanticSignifierResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}RelatedKM'] = DSS_RelatedKM_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}RelatedKM'] = DSS_RelatedKM_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeTrait'] = DSS_describeTrait_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeTrait'] = DSS_describeTrait_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirements'] = DSS_KMDataRequirements_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirements'] = DSS_KMDataRequirements_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMItem'] = DSS_KMItem_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMItem'] = DSS_KMItem_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMCriterion'] = DSS_KMCriterion_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMCriterion'] = DSS_KMCriterion_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyAtSpecifiedTimeResponse'] = DSS_evaluateIterativelyAtSpecifiedTimeResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyAtSpecifiedTimeResponse'] = DSS_evaluateIterativelyAtSpecifiedTimeResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}RankedKM'] = DSS_RankedKM_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}RankedKM'] = DSS_RankedKM_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ExtendedKMDescription'] = DSS_ExtendedKMDescription_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ExtendedKMDescription'] = DSS_ExtendedKMDescription_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitCriterionValue'] = DSS_KMTraitCriterionValue_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitCriterionValue'] = DSS_KMTraitCriterionValue_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ServiceProfile'] = DSS_ServiceProfile_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ServiceProfile'] = DSS_ServiceProfile_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}Trait'] = DSS_Trait_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}Trait'] = DSS_Trait_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDescription'] = DSS_KMDescription_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDescription'] = DSS_KMDescription_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}LanguageSupportRequirement'] = DSS_LanguageSupportRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}LanguageSupportRequirement'] = DSS_LanguageSupportRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationResultCriterion'] = DSS_EvaluationResultCriterion_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationResultCriterion'] = DSS_EvaluationResultCriterion_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ScopingEntity'] = DSS_ScopingEntity_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ScopingEntity'] = DSS_ScopingEntity_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationRequest'] = DSS_IterativeEvaluationRequest_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationRequest'] = DSS_IterativeEvaluationRequest_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMEvaluationResultSemanticsResponse'] = DSS_getKMEvaluationResultSemanticsResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMEvaluationResultSemanticsResponse'] = DSS_getKMEvaluationResultSemanticsResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}AllowedDataRequirement'] = DSS_AllowedDataRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}AllowedDataRequirement'] = DSS_AllowedDataRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeTraitResponse'] = DSS_describeTraitResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeTraitResponse'] = DSS_describeTraitResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeProfileResponse'] = DSS_describeProfileResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeProfileResponse'] = DSS_describeProfileResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedTraitCriterionException'] = DSS_UnrecognizedTraitCriterionException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedTraitCriterionException'] = DSS_UnrecognizedTraitCriterionException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationRequestBase'] = DSS_EvaluationRequestBase_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationRequestBase'] = DSS_EvaluationRequestBase_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ComputableDefinition'] = DSS_ComputableDefinition_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ComputableDefinition'] = DSS_ComputableDefinition_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier'] = DSS_ItemIdentifier_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier'] = DSS_ItemIdentifier_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier'] = DSS_EntityIdentifier_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier'] = DSS_EntityIdentifier_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitInclusionSpecification'] = DSS_KMTraitInclusionSpecification_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitInclusionSpecification'] = DSS_KMTraitInclusionSpecification_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMEvaluationResultSemantics'] = DSS_getKMEvaluationResultSemantics_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMEvaluationResultSemantics'] = DSS_getKMEvaluationResultSemantics_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}IntermediateKMEvaluationResponse'] = DSS_IntermediateKMEvaluationResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}IntermediateKMEvaluationResponse'] = DSS_IntermediateKMEvaluationResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluate'] = DSS_evaluate_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluate'] = DSS_evaluate_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}listProfilesResponse'] = DSS_listProfilesResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}listProfilesResponse'] = DSS_listProfilesResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}DataRequirementItemData'] = DSS_DataRequirementItemData_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}DataRequirementItemData'] = DSS_DataRequirementItemData_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}FunctionalProfile'] = DSS_FunctionalProfile_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}FunctionalProfile'] = DSS_FunctionalProfile_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitValue'] = DSS_KMTraitValue_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitValue'] = DSS_KMTraitValue_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticRequirementResponse'] = DSS_describeSemanticRequirementResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticRequirementResponse'] = DSS_describeSemanticRequirementResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidDataFormatException'] = DSS_InvalidDataFormatException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidDataFormatException'] = DSS_InvalidDataFormatException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticSignifier'] = DSS_describeSemanticSignifier_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticSignifier'] = DSS_describeSemanticSignifier_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}Warning'] = DSS_Warning_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}Warning'] = DSS_Warning_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}RankedKMList'] = DSS_RankedKMList_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}RankedKMList'] = DSS_RankedKMList_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}FinalKMEvaluationResponse'] = DSS_FinalKMEvaluationResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}FinalKMEvaluationResponse'] = DSS_FinalKMEvaluationResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}DataRequirementCriterion'] = DSS_DataRequirementCriterion_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}DataRequirementCriterion'] = DSS_DataRequirementCriterion_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidTimeZoneOffsetException'] = DSS_InvalidTimeZoneOffsetException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidTimeZoneOffsetException'] = DSS_InvalidTimeZoneOffsetException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResponse'] = DSS_KMEvaluationResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResponse'] = DSS_KMEvaluationResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticRequirement'] = DSS_describeSemanticRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticRequirement'] = DSS_describeSemanticRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateAtSpecifiedTimeResponse'] = DSS_evaluateAtSpecifiedTimeResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateAtSpecifiedTimeResponse'] = DSS_evaluateAtSpecifiedTimeResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}XSDComputableDefinition'] = DSS_XSDComputableDefinition_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}XSDComputableDefinition'] = DSS_XSDComputableDefinition_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMStatusCriterion'] = DSS_KMStatusCriterion_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMStatusCriterion'] = DSS_KMStatusCriterion_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}DataRequirementBase'] = DSS_DataRequirementBase_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}DataRequirementBase'] = DSS_DataRequirementBase_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMList'] = DSS_KMList_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMList'] = DSS_KMList_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse'] = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse'] = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationRequest'] = DSS_KMEvaluationRequest_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationRequest'] = DSS_KMEvaluationRequest_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedLanguageException'] = DSS_UnrecognizedLanguageException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedLanguageException'] = DSS_UnrecognizedLanguageException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}RelatedKMSearchCriterion'] = DSS_RelatedKMSearchCriterion_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}RelatedKMSearchCriterion'] = DSS_RelatedKMSearchCriterion_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}TraitSetRequirement'] = DSS_TraitSetRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}TraitSetRequirement'] = DSS_TraitSetRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMSearchCriteria'] = DSS_KMSearchCriteria_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMSearchCriteria'] = DSS_KMSearchCriteria_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticProfile'] = DSS_SemanticProfile_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticProfile'] = DSS_SemanticProfile_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultSemantics'] = DSS_KMEvaluationResultSemantics_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultSemantics'] = DSS_KMEvaluationResultSemantics_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier'] = DSS_InteractionIdentifier_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier'] = DSS_InteractionIdentifier_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntity'] = DSS_describeScopingEntity_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntity'] = DSS_describeScopingEntity_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDescriptionResponse'] = DSS_getKMDescriptionResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDescriptionResponse'] = DSS_getKMDescriptionResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}DSSException'] = DSS_DSSException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}DSSException'] = DSS_DSSException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationException'] = DSS_EvaluationException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationException'] = DSS_EvaluationException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMLocalizedTraitValue'] = DSS_KMLocalizedTraitValue_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMLocalizedTraitValue'] = DSS_KMLocalizedTraitValue_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}InformationModelAlternative'] = DSS_InformationModelAlternative_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}InformationModelAlternative'] = DSS_InformationModelAlternative_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityResponse'] = DSS_describeScopingEntityResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityResponse'] = DSS_describeScopingEntityResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirements'] = DSS_getKMDataRequirements_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirements'] = DSS_getKMDataRequirements_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}DSSRuntimeException'] = DSS_DSSRuntimeException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}DSSRuntimeException'] = DSS_DSSRuntimeException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}findKMs'] = DSS_findKMs_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}findKMs'] = DSS_findKMs_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsForEvaluationAtSpecifiedTime'] = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsForEvaluationAtSpecifiedTime'] = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}findKMsResponse'] = DSS_findKMsResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}findKMsResponse'] = DSS_findKMsResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidDriDataFormatException'] = DSS_InvalidDriDataFormatException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidDriDataFormatException'] = DSS_InvalidDriDataFormatException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}RequiredDataNotProvidedException'] = DSS_RequiredDataNotProvidedException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}RequiredDataNotProvidedException'] = DSS_RequiredDataNotProvidedException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedScopedEntityException'] = DSS_UnrecognizedScopedEntityException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedScopedEntityException'] = DSS_UnrecognizedScopedEntityException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityHierarchyResponse'] = DSS_describeScopingEntityHierarchyResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityHierarchyResponse'] = DSS_describeScopingEntityHierarchyResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}OtherSemanticRequirement'] = DSS_OtherSemanticRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}OtherSemanticRequirement'] = DSS_OtherSemanticRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeProfile'] = DSS_describeProfile_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeProfile'] = DSS_describeProfile_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateAtSpecifiedTime'] = DSS_evaluateAtSpecifiedTime_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateAtSpecifiedTime'] = DSS_evaluateAtSpecifiedTime_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMConsumerProvidedQueryParameter'] = DSS_KMConsumerProvidedQueryParameter_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMConsumerProvidedQueryParameter'] = DSS_KMConsumerProvidedQueryParameter_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ScopedDO'] = DSS_ScopedDO_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ScopedDO'] = DSS_ScopedDO_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}InformationModelRequirement'] = DSS_InformationModelRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}InformationModelRequirement'] = DSS_InformationModelRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultData'] = DSS_KMEvaluationResultData_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultData'] = DSS_KMEvaluationResultData_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ProfilesOfType'] = DSS_ProfilesOfType_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ProfilesOfType'] = DSS_ProfilesOfType_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}TraitCriterion'] = DSS_TraitCriterion_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}TraitCriterion'] = DSS_TraitCriterion_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticSignifier'] = DSS_SemanticSignifier_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticSignifier'] = DSS_SemanticSignifier_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeKMEvaluationRequest'] = DSS_IterativeKMEvaluationRequest_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeKMEvaluationRequest'] = DSS_IterativeKMEvaluationRequest_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedScopingEntityException'] = DSS_UnrecognizedScopingEntityException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedScopingEntityException'] = DSS_UnrecognizedScopingEntityException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDescriptionBase'] = DSS_KMDescriptionBase_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDescriptionBase'] = DSS_KMDescriptionBase_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}UnsupportedLanguageException'] = DSS_UnsupportedLanguageException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}UnsupportedLanguageException'] = DSS_UnsupportedLanguageException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticRequirement'] = DSS_SemanticRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticRequirement'] = DSS_SemanticRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationRequestBase'] = DSS_KMEvaluationRequestBase_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationRequestBase'] = DSS_KMEvaluationRequestBase_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsResponse'] = DSS_getKMDataRequirementsResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsResponse'] = DSS_getKMDataRequirementsResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}CPQPInUse'] = DSS_CPQPInUse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}CPQPInUse'] = DSS_CPQPInUse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}listProfiles'] = DSS_listProfiles_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}listProfiles'] = DSS_listProfiles_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ServiceRequestBase'] = DSS_ServiceRequestBase_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ServiceRequestBase'] = DSS_ServiceRequestBase_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}IntermediateState'] = DSS_IntermediateState_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}IntermediateState'] = DSS_IntermediateState_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementGroup'] = DSS_KMDataRequirementGroup_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementGroup'] = DSS_KMDataRequirementGroup_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}listKMsResponse'] = DSS_listKMsResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}listKMsResponse'] = DSS_listKMsResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitCriterion'] = DSS_KMTraitCriterion_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitCriterion'] = DSS_KMTraitCriterion_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}RequiredDataRequirement'] = DSS_RequiredDataRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}RequiredDataRequirement'] = DSS_RequiredDataRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeKMEvaluationResponse'] = DSS_IterativeKMEvaluationResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeKMEvaluationResponse'] = DSS_IterativeKMEvaluationResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationResponse'] = DSS_IterativeEvaluationResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationResponse'] = DSS_IterativeEvaluationResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ProfilesByType'] = DSS_ProfilesByType_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ProfilesByType'] = DSS_ProfilesByType_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticPayload'] = DSS_SemanticPayload_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticPayload'] = DSS_SemanticPayload_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationRequest'] = DSS_EvaluationRequest_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationRequest'] = DSS_EvaluationRequest_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateResponse'] = DSS_evaluateResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateResponse'] = DSS_evaluateResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ConformanceProfile'] = DSS_ConformanceProfile_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ConformanceProfile'] = DSS_ConformanceProfile_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidTraitCriterionDataFormatException'] = DSS_InvalidTraitCriterionDataFormatException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidTraitCriterionDataFormatException'] = DSS_InvalidTraitCriterionDataFormatException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationResponse'] = DSS_EvaluationResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationResponse'] = DSS_EvaluationResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultSemanticsList'] = DSS_KMEvaluationResultSemanticsList_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultSemanticsList'] = DSS_KMEvaluationResultSemanticsList_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyResponse'] = DSS_evaluateIterativelyResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyResponse'] = DSS_evaluateIterativelyResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}TraitRequirement'] = DSS_TraitRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}TraitRequirement'] = DSS_TraitRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyAtSpecifiedTime'] = DSS_evaluateIterativelyAtSpecifiedTime_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyAtSpecifiedTime'] = DSS_evaluateIterativelyAtSpecifiedTime_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityHierarchy'] = DSS_describeScopingEntityHierarchy_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityHierarchy'] = DSS_describeScopingEntityHierarchy_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationResponseBase'] = DSS_EvaluationResponseBase_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationResponseBase'] = DSS_EvaluationResponseBase_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDescription'] = DSS_getKMDescription_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDescription'] = DSS_getKMDescription_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}DescribedDO'] = DSS_DescribedDO_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}DescribedDO'] = DSS_DescribedDO_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementItem'] = DSS_KMDataRequirementItem_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirementItem'] = DSS_KMDataRequirementItem_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}listKMs'] = DSS_listKMs_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}listKMs'] = DSS_listKMs_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIteratively'] = DSS_evaluateIteratively_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIteratively'] = DSS_evaluateIteratively_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticSignifierResponse'] = DSS_describeSemanticSignifierResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticSignifierResponse'] = DSS_describeSemanticSignifierResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}RelatedKM'] = DSS_RelatedKM_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}RelatedKM'] = DSS_RelatedKM_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeTrait'] = DSS_describeTrait_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeTrait'] = DSS_describeTrait_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirements'] = DSS_KMDataRequirements_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDataRequirements'] = DSS_KMDataRequirements_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMItem'] = DSS_KMItem_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMItem'] = DSS_KMItem_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMCriterion'] = DSS_KMCriterion_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMCriterion'] = DSS_KMCriterion_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyAtSpecifiedTimeResponse'] = DSS_evaluateIterativelyAtSpecifiedTimeResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateIterativelyAtSpecifiedTimeResponse'] = DSS_evaluateIterativelyAtSpecifiedTimeResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}RankedKM'] = DSS_RankedKM_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}RankedKM'] = DSS_RankedKM_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ExtendedKMDescription'] = DSS_ExtendedKMDescription_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ExtendedKMDescription'] = DSS_ExtendedKMDescription_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitCriterionValue'] = DSS_KMTraitCriterionValue_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitCriterionValue'] = DSS_KMTraitCriterionValue_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ServiceProfile'] = DSS_ServiceProfile_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ServiceProfile'] = DSS_ServiceProfile_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}Trait'] = DSS_Trait_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}Trait'] = DSS_Trait_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDescription'] = DSS_KMDescription_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMDescription'] = DSS_KMDescription_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}LanguageSupportRequirement'] = DSS_LanguageSupportRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}LanguageSupportRequirement'] = DSS_LanguageSupportRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationResultCriterion'] = DSS_EvaluationResultCriterion_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationResultCriterion'] = DSS_EvaluationResultCriterion_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ScopingEntity'] = DSS_ScopingEntity_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ScopingEntity'] = DSS_ScopingEntity_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationRequest'] = DSS_IterativeEvaluationRequest_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeEvaluationRequest'] = DSS_IterativeEvaluationRequest_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMEvaluationResultSemanticsResponse'] = DSS_getKMEvaluationResultSemanticsResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMEvaluationResultSemanticsResponse'] = DSS_getKMEvaluationResultSemanticsResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}AllowedDataRequirement'] = DSS_AllowedDataRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}AllowedDataRequirement'] = DSS_AllowedDataRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeTraitResponse'] = DSS_describeTraitResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeTraitResponse'] = DSS_describeTraitResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeProfileResponse'] = DSS_describeProfileResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeProfileResponse'] = DSS_describeProfileResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedTraitCriterionException'] = DSS_UnrecognizedTraitCriterionException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedTraitCriterionException'] = DSS_UnrecognizedTraitCriterionException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationRequestBase'] = DSS_EvaluationRequestBase_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationRequestBase'] = DSS_EvaluationRequestBase_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ComputableDefinition'] = DSS_ComputableDefinition_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ComputableDefinition'] = DSS_ComputableDefinition_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier'] = DSS_ItemIdentifier_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ItemIdentifier'] = DSS_ItemIdentifier_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier'] = DSS_EntityIdentifier_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}EntityIdentifier'] = DSS_EntityIdentifier_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitInclusionSpecification'] = DSS_KMTraitInclusionSpecification_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitInclusionSpecification'] = DSS_KMTraitInclusionSpecification_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMEvaluationResultSemantics'] = DSS_getKMEvaluationResultSemantics_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMEvaluationResultSemantics'] = DSS_getKMEvaluationResultSemantics_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}IntermediateKMEvaluationResponse'] = DSS_IntermediateKMEvaluationResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}IntermediateKMEvaluationResponse'] = DSS_IntermediateKMEvaluationResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluate'] = DSS_evaluate_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluate'] = DSS_evaluate_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}listProfilesResponse'] = DSS_listProfilesResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}listProfilesResponse'] = DSS_listProfilesResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}DataRequirementItemData'] = DSS_DataRequirementItemData_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}DataRequirementItemData'] = DSS_DataRequirementItemData_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}FunctionalProfile'] = DSS_FunctionalProfile_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}FunctionalProfile'] = DSS_FunctionalProfile_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitValue'] = DSS_KMTraitValue_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMTraitValue'] = DSS_KMTraitValue_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticRequirementResponse'] = DSS_describeSemanticRequirementResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticRequirementResponse'] = DSS_describeSemanticRequirementResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticSignifier'] = DSS_describeSemanticSignifier_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticSignifier'] = DSS_describeSemanticSignifier_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidDataFormatException'] = DSS_InvalidDataFormatException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidDataFormatException'] = DSS_InvalidDataFormatException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}Warning'] = DSS_Warning_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}Warning'] = DSS_Warning_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}RankedKMList'] = DSS_RankedKMList_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}RankedKMList'] = DSS_RankedKMList_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}FinalKMEvaluationResponse'] = DSS_FinalKMEvaluationResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}FinalKMEvaluationResponse'] = DSS_FinalKMEvaluationResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}DataRequirementCriterion'] = DSS_DataRequirementCriterion_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}DataRequirementCriterion'] = DSS_DataRequirementCriterion_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidTimeZoneOffsetException'] = DSS_InvalidTimeZoneOffsetException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidTimeZoneOffsetException'] = DSS_InvalidTimeZoneOffsetException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResponse'] = DSS_KMEvaluationResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResponse'] = DSS_KMEvaluationResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticRequirement'] = DSS_describeSemanticRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeSemanticRequirement'] = DSS_describeSemanticRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateAtSpecifiedTimeResponse'] = DSS_evaluateAtSpecifiedTimeResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateAtSpecifiedTimeResponse'] = DSS_evaluateAtSpecifiedTimeResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}XSDComputableDefinition'] = DSS_XSDComputableDefinition_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}XSDComputableDefinition'] = DSS_XSDComputableDefinition_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMStatusCriterion'] = DSS_KMStatusCriterion_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMStatusCriterion'] = DSS_KMStatusCriterion_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}DataRequirementBase'] = DSS_DataRequirementBase_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}DataRequirementBase'] = DSS_DataRequirementBase_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMList'] = DSS_KMList_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMList'] = DSS_KMList_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse'] = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse'] = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTimeResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationRequest'] = DSS_KMEvaluationRequest_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationRequest'] = DSS_KMEvaluationRequest_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedLanguageException'] = DSS_UnrecognizedLanguageException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedLanguageException'] = DSS_UnrecognizedLanguageException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}RelatedKMSearchCriterion'] = DSS_RelatedKMSearchCriterion_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}RelatedKMSearchCriterion'] = DSS_RelatedKMSearchCriterion_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMSearchCriteria'] = DSS_KMSearchCriteria_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMSearchCriteria'] = DSS_KMSearchCriteria_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}TraitSetRequirement'] = DSS_TraitSetRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}TraitSetRequirement'] = DSS_TraitSetRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticProfile'] = DSS_SemanticProfile_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticProfile'] = DSS_SemanticProfile_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultSemantics'] = DSS_KMEvaluationResultSemantics_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultSemantics'] = DSS_KMEvaluationResultSemantics_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier'] = DSS_InteractionIdentifier_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}InteractionIdentifier'] = DSS_InteractionIdentifier_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntity'] = DSS_describeScopingEntity_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntity'] = DSS_describeScopingEntity_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDescriptionResponse'] = DSS_getKMDescriptionResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDescriptionResponse'] = DSS_getKMDescriptionResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}DSSException'] = DSS_DSSException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}DSSException'] = DSS_DSSException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationException'] = DSS_EvaluationException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}EvaluationException'] = DSS_EvaluationException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMLocalizedTraitValue'] = DSS_KMLocalizedTraitValue_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMLocalizedTraitValue'] = DSS_KMLocalizedTraitValue_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}InformationModelAlternative'] = DSS_InformationModelAlternative_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}InformationModelAlternative'] = DSS_InformationModelAlternative_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityResponse'] = DSS_describeScopingEntityResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityResponse'] = DSS_describeScopingEntityResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirements'] = DSS_getKMDataRequirements_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirements'] = DSS_getKMDataRequirements_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}DSSRuntimeException'] = DSS_DSSRuntimeException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}DSSRuntimeException'] = DSS_DSSRuntimeException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}findKMs'] = DSS_findKMs_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}findKMs'] = DSS_findKMs_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsForEvaluationAtSpecifiedTime'] = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}getKMDataRequirementsForEvaluationAtSpecifiedTime'] = DSS_getKMDataRequirementsForEvaluationAtSpecifiedTime_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}findKMsResponse'] = DSS_findKMsResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}findKMsResponse'] = DSS_findKMsResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidDriDataFormatException'] = DSS_InvalidDriDataFormatException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}InvalidDriDataFormatException'] = DSS_InvalidDriDataFormatException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}RequiredDataNotProvidedException'] = DSS_RequiredDataNotProvidedException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}RequiredDataNotProvidedException'] = DSS_RequiredDataNotProvidedException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedScopedEntityException'] = DSS_UnrecognizedScopedEntityException_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}UnrecognizedScopedEntityException'] = DSS_UnrecognizedScopedEntityException_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityHierarchyResponse'] = DSS_describeScopingEntityHierarchyResponse_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeScopingEntityHierarchyResponse'] = DSS_describeScopingEntityHierarchyResponse_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}OtherSemanticRequirement'] = DSS_OtherSemanticRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}OtherSemanticRequirement'] = DSS_OtherSemanticRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}describeProfile'] = DSS_describeProfile_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}describeProfile'] = DSS_describeProfile_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateAtSpecifiedTime'] = DSS_evaluateAtSpecifiedTime_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}evaluateAtSpecifiedTime'] = DSS_evaluateAtSpecifiedTime_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMConsumerProvidedQueryParameter'] = DSS_KMConsumerProvidedQueryParameter_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMConsumerProvidedQueryParameter'] = DSS_KMConsumerProvidedQueryParameter_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ScopedDO'] = DSS_ScopedDO_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ScopedDO'] = DSS_ScopedDO_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}InformationModelRequirement'] = DSS_InformationModelRequirement_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}InformationModelRequirement'] = DSS_InformationModelRequirement_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultData'] = DSS_KMEvaluationResultData_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}KMEvaluationResultData'] = DSS_KMEvaluationResultData_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}ProfilesOfType'] = DSS_ProfilesOfType_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}ProfilesOfType'] = DSS_ProfilesOfType_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}TraitCriterion'] = DSS_TraitCriterion_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}TraitCriterion'] = DSS_TraitCriterion_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticSignifier'] = DSS_SemanticSignifier_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}SemanticSignifier'] = DSS_SemanticSignifier_deserialize;
    this.globalElementSerializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeKMEvaluationRequest'] = DSS_IterativeKMEvaluationRequest_serialize;
    this.globalElementDeserializers['{http://www.omg.org/spec/CDSS/201105/dss}IterativeKMEvaluationRequest'] = DSS_IterativeKMEvaluationRequest_deserialize;
}

function DSSWSDL_evaluateAtSpecifiedTime_op_onsuccess(client, responseXml) {
    if (client.user_onsuccess) {
     var responseObject = null;
     var element = responseXml.documentElement;
     this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
     element = this.jsutils.getFirstElementChild(element);
     this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
     while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
       throw 'No env:Body in message.'
      }
     }
     element = this.jsutils.getFirstElementChild(element);
     this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
     this.jsutils.trace('calling DSSWSDL_evaluateAtSpecifiedTimeResponse_deserializeResponse');
     responseObject = DSSWSDL_evaluateAtSpecifiedTimeResponse_deserializeResponse(this.jsutils, element);
     client.user_onsuccess(responseObject);
    }
}

DSSWSDL_Evaluation.prototype.evaluateAtSpecifiedTime_onsuccess = DSSWSDL_evaluateAtSpecifiedTime_op_onsuccess;

function DSSWSDL_evaluateAtSpecifiedTime_op_onerror(client) {
    if (client.user_onerror) {
     var httpStatus;
     var httpStatusText;
     try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
     } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
     }
     client.user_onerror(httpStatus, httpStatusText);
    }
}

DSSWSDL_Evaluation.prototype.evaluateAtSpecifiedTime_onerror = DSSWSDL_evaluateAtSpecifiedTime_op_onerror;

//
// Operation {http://www.omg.org/spec/CDSS/201105/dssWsdl}evaluateAtSpecifiedTime
// Wrapped operation.
//
function DSSWSDL_evaluateAtSpecifiedTime_op(successCallback, errorCallback, interactionId, specifiedTime, evaluationRequest) {
    this.client = new CxfApacheOrgClient(this.jsutils);
    var xml = null;
    var args = new Array(3);
    args[0] = interactionId;
    args[1] = specifiedTime;
    args[2] = evaluationRequest;
    xml = this.evaluateAtSpecifiedTimeRequest_serializeInput(this.jsutils, args);
    this.client.user_onsuccess = successCallback;
    this.client.user_onerror = errorCallback;
    var closureThis = this;
    this.client.onsuccess = function(client, responseXml) { closureThis.evaluateAtSpecifiedTime_onsuccess(client, responseXml); };
    this.client.onerror = function(client) { closureThis.evaluateAtSpecifiedTime_onerror(client); };
    var requestHeaders = [];
    requestHeaders['SOAPAction'] = 'http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:evaluateAtSpecifiedTime';
    this.jsutils.trace('synchronous = ' + this.synchronous);
    this.client.request(this.url, xml, null, this.synchronous, requestHeaders);
}

DSSWSDL_Evaluation.prototype.evaluateAtSpecifiedTime = DSSWSDL_evaluateAtSpecifiedTime_op;

function DSSWSDL_evaluateAtSpecifiedTimeRequest_serializeInput(cxfjsutils, args) {
    var wrapperObj = new DSS_evaluateAtSpecifiedTime();
    wrapperObj.setInteractionId(args[0]);
    wrapperObj.setSpecifiedTime(args[1]);
    wrapperObj.setEvaluationRequest(args[2]);
    var xml;
    xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.omg.org/spec/CDSS/201105/dss' ");
    // block for local variables
    {
     xml = xml + wrapperObj.serialize(cxfjsutils, 'jns0:evaluateAtSpecifiedTime', null);
    }
    xml = xml + cxfjsutils.endSoap11Message();
    return xml;
}

DSSWSDL_Evaluation.prototype.evaluateAtSpecifiedTimeRequest_serializeInput = DSSWSDL_evaluateAtSpecifiedTimeRequest_serializeInput;

function DSSWSDL_evaluateAtSpecifiedTimeResponse_deserializeResponse(cxfjsutils, partElement) {
    var returnObject = DSS_evaluateAtSpecifiedTimeResponse_deserialize (cxfjsutils, partElement);

    return returnObject;
}
function DSSWSDL_evaluate_op_onsuccess(client, responseXml) {
    if (client.user_onsuccess) {
     var responseObject = null;
     var element = responseXml.documentElement;
     this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
     element = this.jsutils.getFirstElementChild(element);
     this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
     while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
       throw 'No env:Body in message.'
      }
     }
     element = this.jsutils.getFirstElementChild(element);
     this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
     this.jsutils.trace('calling DSSWSDL_evaluateResponse_deserializeResponse');
     responseObject = DSSWSDL_evaluateResponse_deserializeResponse(this.jsutils, element);
     client.user_onsuccess(responseObject);
    }
}

DSSWSDL_Evaluation.prototype.evaluate_onsuccess = DSSWSDL_evaluate_op_onsuccess;

function DSSWSDL_evaluate_op_onerror(client) {
    if (client.user_onerror) {
     var httpStatus;
     var httpStatusText;
     try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
     } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
     }
     client.user_onerror(httpStatus, httpStatusText);
    }
}

DSSWSDL_Evaluation.prototype.evaluate_onerror = DSSWSDL_evaluate_op_onerror;

//
// Operation {http://www.omg.org/spec/CDSS/201105/dssWsdl}evaluate
// Wrapped operation.
//
function DSSWSDL_evaluate_op(successCallback, errorCallback, interactionId, evaluationRequest) {
    this.client = new CxfApacheOrgClient(this.jsutils);
    var xml = null;
    var args = new Array(2);
    args[0] = interactionId;
    args[1] = evaluationRequest;
    xml = this.evaluateRequest_serializeInput(this.jsutils, args);
    this.client.user_onsuccess = successCallback;
    this.client.user_onerror = errorCallback;
    var closureThis = this;
    this.client.onsuccess = function(client, responseXml) { closureThis.evaluate_onsuccess(client, responseXml); };
    this.client.onerror = function(client) { closureThis.evaluate_onerror(client); };
    var requestHeaders = [];
    requestHeaders['SOAPAction'] = 'http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:evaluate';
    this.jsutils.trace('synchronous = ' + this.synchronous);
    this.client.request(this.url, xml, null, this.synchronous, requestHeaders);
}

DSSWSDL_Evaluation.prototype.evaluate = DSSWSDL_evaluate_op;

function DSSWSDL_evaluateRequest_serializeInput(cxfjsutils, args) {
    var wrapperObj = new DSS_evaluate();
    wrapperObj.setInteractionId(args[0]);
    wrapperObj.setEvaluationRequest(args[1]);
    var xml;
    xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.omg.org/spec/CDSS/201105/dss' ");
    // block for local variables
    {
     xml = xml + wrapperObj.serialize(cxfjsutils, 'jns0:evaluate', null);
    }
    xml = xml + cxfjsutils.endSoap11Message();
    return xml;
}

DSSWSDL_Evaluation.prototype.evaluateRequest_serializeInput = DSSWSDL_evaluateRequest_serializeInput;

function DSSWSDL_evaluateResponse_deserializeResponse(cxfjsutils, partElement) {
    var returnObject = DSS_evaluateResponse_deserialize (cxfjsutils, partElement);

    return returnObject;
}
function DSSWSDL_evaluateIterativelyAtSpecifiedTime_op_onsuccess(client, responseXml) {
    if (client.user_onsuccess) {
     var responseObject = null;
     var element = responseXml.documentElement;
     this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
     element = this.jsutils.getFirstElementChild(element);
     this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
     while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
       throw 'No env:Body in message.'
      }
     }
     element = this.jsutils.getFirstElementChild(element);
     this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
     this.jsutils.trace('calling DSSWSDL_evaluateIterativelyAtSpecifiedTimeResponse_deserializeResponse');
     responseObject = DSSWSDL_evaluateIterativelyAtSpecifiedTimeResponse_deserializeResponse(this.jsutils, element);
     client.user_onsuccess(responseObject);
    }
}

DSSWSDL_Evaluation.prototype.evaluateIterativelyAtSpecifiedTime_onsuccess = DSSWSDL_evaluateIterativelyAtSpecifiedTime_op_onsuccess;

function DSSWSDL_evaluateIterativelyAtSpecifiedTime_op_onerror(client) {
    if (client.user_onerror) {
     var httpStatus;
     var httpStatusText;
     try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
     } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
     }
     client.user_onerror(httpStatus, httpStatusText);
    }
}

DSSWSDL_Evaluation.prototype.evaluateIterativelyAtSpecifiedTime_onerror = DSSWSDL_evaluateIterativelyAtSpecifiedTime_op_onerror;

//
// Operation {http://www.omg.org/spec/CDSS/201105/dssWsdl}evaluateIterativelyAtSpecifiedTime
// Wrapped operation.
//
function DSSWSDL_evaluateIterativelyAtSpecifiedTime_op(successCallback, errorCallback, interactionId, specifiedTime, iterativeEvaluationRequest) {
    this.client = new CxfApacheOrgClient(this.jsutils);
    var xml = null;
    var args = new Array(3);
    args[0] = interactionId;
    args[1] = specifiedTime;
    args[2] = iterativeEvaluationRequest;
    xml = this.evaluateIterativelyAtSpecifiedTimeRequest_serializeInput(this.jsutils, args);
    this.client.user_onsuccess = successCallback;
    this.client.user_onerror = errorCallback;
    var closureThis = this;
    this.client.onsuccess = function(client, responseXml) { closureThis.evaluateIterativelyAtSpecifiedTime_onsuccess(client, responseXml); };
    this.client.onerror = function(client) { closureThis.evaluateIterativelyAtSpecifiedTime_onerror(client); };
    var requestHeaders = [];
    requestHeaders['SOAPAction'] = 'http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:evaluateIterativelyAtSpecifiedTime';
    this.jsutils.trace('synchronous = ' + this.synchronous);
    this.client.request(this.url, xml, null, this.synchronous, requestHeaders);
}

DSSWSDL_Evaluation.prototype.evaluateIterativelyAtSpecifiedTime = DSSWSDL_evaluateIterativelyAtSpecifiedTime_op;

function DSSWSDL_evaluateIterativelyAtSpecifiedTimeRequest_serializeInput(cxfjsutils, args) {
    var wrapperObj = new DSS_evaluateIterativelyAtSpecifiedTime();
    wrapperObj.setInteractionId(args[0]);
    wrapperObj.setSpecifiedTime(args[1]);
    wrapperObj.setIterativeEvaluationRequest(args[2]);
    var xml;
    xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.omg.org/spec/CDSS/201105/dss' ");
    // block for local variables
    {
     xml = xml + wrapperObj.serialize(cxfjsutils, 'jns0:evaluateIterativelyAtSpecifiedTime', null);
    }
    xml = xml + cxfjsutils.endSoap11Message();
    return xml;
}

DSSWSDL_Evaluation.prototype.evaluateIterativelyAtSpecifiedTimeRequest_serializeInput = DSSWSDL_evaluateIterativelyAtSpecifiedTimeRequest_serializeInput;

function DSSWSDL_evaluateIterativelyAtSpecifiedTimeResponse_deserializeResponse(cxfjsutils, partElement) {
    var returnObject = DSS_evaluateIterativelyAtSpecifiedTimeResponse_deserialize (cxfjsutils, partElement);

    return returnObject;
}
function DSSWSDL_evaluateIteratively_op_onsuccess(client, responseXml) {
    if (client.user_onsuccess) {
     var responseObject = null;
     var element = responseXml.documentElement;
     this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
     element = this.jsutils.getFirstElementChild(element);
     this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
     while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
       throw 'No env:Body in message.'
      }
     }
     element = this.jsutils.getFirstElementChild(element);
     this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
     this.jsutils.trace('calling DSSWSDL_evaluateIterativelyResponse_deserializeResponse');
     responseObject = DSSWSDL_evaluateIterativelyResponse_deserializeResponse(this.jsutils, element);
     client.user_onsuccess(responseObject);
    }
}

DSSWSDL_Evaluation.prototype.evaluateIteratively_onsuccess = DSSWSDL_evaluateIteratively_op_onsuccess;

function DSSWSDL_evaluateIteratively_op_onerror(client) {
    if (client.user_onerror) {
     var httpStatus;
     var httpStatusText;
     try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
     } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
     }
     client.user_onerror(httpStatus, httpStatusText);
    }
}

DSSWSDL_Evaluation.prototype.evaluateIteratively_onerror = DSSWSDL_evaluateIteratively_op_onerror;

//
// Operation {http://www.omg.org/spec/CDSS/201105/dssWsdl}evaluateIteratively
// Wrapped operation.
//
function DSSWSDL_evaluateIteratively_op(successCallback, errorCallback, interactionId, iterativeEvaluationRequest) {
    this.client = new CxfApacheOrgClient(this.jsutils);
    var xml = null;
    var args = new Array(2);
    args[0] = interactionId;
    args[1] = iterativeEvaluationRequest;
    xml = this.evaluateIterativelyRequest_serializeInput(this.jsutils, args);
    this.client.user_onsuccess = successCallback;
    this.client.user_onerror = errorCallback;
    var closureThis = this;
    this.client.onsuccess = function(client, responseXml) { closureThis.evaluateIteratively_onsuccess(client, responseXml); };
    this.client.onerror = function(client) { closureThis.evaluateIteratively_onerror(client); };
    var requestHeaders = [];
    requestHeaders['SOAPAction'] = 'http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:evaluateIteratively';
    this.jsutils.trace('synchronous = ' + this.synchronous);
    this.client.request(this.url, xml, null, this.synchronous, requestHeaders);
}

DSSWSDL_Evaluation.prototype.evaluateIteratively = DSSWSDL_evaluateIteratively_op;

function DSSWSDL_evaluateIterativelyRequest_serializeInput(cxfjsutils, args) {
    var wrapperObj = new DSS_evaluateIteratively();
    wrapperObj.setInteractionId(args[0]);
    wrapperObj.setIterativeEvaluationRequest(args[1]);
    var xml;
    xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.omg.org/spec/CDSS/201105/dss' ");
    // block for local variables
    {
     xml = xml + wrapperObj.serialize(cxfjsutils, 'jns0:evaluateIteratively', null);
    }
    xml = xml + cxfjsutils.endSoap11Message();
    return xml;
}

DSSWSDL_Evaluation.prototype.evaluateIterativelyRequest_serializeInput = DSSWSDL_evaluateIterativelyRequest_serializeInput;

function DSSWSDL_evaluateIterativelyResponse_deserializeResponse(cxfjsutils, partElement) {
    var returnObject = DSS_evaluateIterativelyResponse_deserialize (cxfjsutils, partElement);

    return returnObject;
}
function DSSWSDL_Evaluation_DSSWSDL_evaluate () {
  this.url = 'http://localhost:8081/opencds-decision-support-service/evaluate';
}
DSSWSDL_Evaluation_DSSWSDL_evaluate.prototype = new DSSWSDL_Evaluation;
