
var EvaluationService = new DSSWSDL_Evaluation();

EvaluationService.url = "http://localhost:8080/opencds-decision-support-service/evaluate"

function invokeEvaluationService(){
		
	var interactionId = new DSS_InteractionIdentifier();
	
	var interactionId_scopingEntityId = document.opencdsclientform.interactionId_scopingEntityId.value;
	interactionId.setScopingEntityId(interactionId_scopingEntityId);
		
	var interactionId_interactionId = document.opencdsclientform.interactionId_interactionId.value;
	interactionId.setInteractionId(interactionId_interactionId);
	
	var interactionId_submissionTime = document.opencdsclientform.interactionId_submissionTime.value;
	interactionId.setSubmissionTime(interactionId_submissionTime);
	
   	var specifiedTime = document.opencdsclientform.specifiedTime.value;
   					
   	var evaluationRequest = new DSS_EvaluationRequest();   
   	var evaluationRequest_clientLanguage = document.opencdsclientform.evaluationRequest_clientLanguage.value;
   	evaluationRequest.setClientLanguage(evaluationRequest_clientLanguage);
   	 	
   	var evaluationRequest_clientTimeZoneOffset = document.opencdsclientform.evaluationRequest_clientTimeZoneOffset.value;
   	evaluationRequest.setClientTimeZoneOffset(evaluationRequest_clientTimeZoneOffset);
  
   	var kmEvaluationRequest = new DSS_KMEvaluationRequest();
   			
   	var kmId = new DSS_EntityIdentifier();
   	var scopingEntityId = document.opencdsclientform.kmEvaluationRequest_kmId_scopingEntityId.value;
   	var businessId = document.opencdsclientform.kmEvaluationRequest_kmId_businessId.value;
   	var version = document.opencdsclientform.kmEvaluationRequest_kmId_version.value;
   	
   	kmId.setScopingEntityId(scopingEntityId);
   	kmId.setBusinessId(businessId);
   	kmId.setVersion(version);
   		
    kmEvaluationRequest.setKmId(kmId);	
   	
   	evaluationRequest._kmEvaluationRequest[0] = kmEvaluationRequest;
   	
   	var dataRequirementsItemData = new DSS_DataRequirementItemData();
	var driId = new DSS_ItemIdentifier();
				
	var containingEntityId = new DSS_EntityIdentifier();
	var containingEntityId_scopingEntityId = document.opencdsclientform.containingEntityId_scopingEntityId.value; 
	containingEntityId.setScopingEntityId(containingEntityId_scopingEntityId);
	
	var containingEntityId_businessId = document.opencdsclientform.containingEntityId_businessId.value; 
	containingEntityId.setBusinessId(containingEntityId_businessId);
	
	var containingEntityId_version = document.opencdsclientform.containingEntityId_version.value;
	containingEntityId.setVersion(containingEntityId_version);	
	
	driId.setContainingEntityId(containingEntityId);
	var dataRequirementItemData_driId = document.opencdsclientform.dataRequirementItemData_driId.value;
	driId.setItemId(dataRequirementItemData_driId);
	
	dataRequirementsItemData.setDriId(driId);	
			
	var data = new DSS_SemanticPayload();
	var informationModelSSId = new DSS_EntityIdentifier();
	var informationModelSSId_scopingEntityId = document.opencdsclientform.informationModelSSId_scopingEntityId.value;
	informationModelSSId.setScopingEntityId(informationModelSSId_scopingEntityId);
	
	var informationModelSSId_businessId = document.opencdsclientform.informationModelSSId_businessId.value;
	informationModelSSId.setBusinessId(informationModelSSId_businessId);
	
	var informationModelSSId_version = document.opencdsclientform.informationModelSSId_version.value;
	informationModelSSId.setVersion(informationModelSSId_version);
		
	data.setInformationModelSSId(informationModelSSId);
	var fhir = document.opencdsclientform.outputFhirMsg.value;
	var encodedFhir = window.btoa(fhir);
	data.setBase64EncodedPayload(encodedFhir);
	
	dataRequirementsItemData.setData(data);
			
	evaluationRequest._dataRequirementItemData[0] = dataRequirementsItemData;	
		
	EvaluationService.evaluateAtSpecifiedTime(sucessResponse, errorResponse, interactionId, specifiedTime, evaluationRequest);
}
	

function sucessResponse(response)
{
	
	var base64Payload = response.getEvaluationResponse().getFinalKMEvaluationResponse()[0].getKmEvaluationResultData()[0].getData().getBase64EncodedPayload();
	var decodedPayload = window.atob(base64Payload); 
	// console.log(decodedPayload);
	
	var obj = JSON.parse(decodedPayload);
	var pretty = JSON.stringify(obj, undefined, 4);
	document.getElementById('response').value = pretty;
	// automatically scroll bar down 
	var textarea = document.getElementById('response');
	textarea.scrollTop = textarea.scrollHeight;	
}

function errorResponse(error)
{	
	alert("Error message is " + error);
}

function clearResponseTextArea(){
    document.getElementById("response").value='';              
}